#include "unity_testing.h"
#include <remote-sensor/communication/models.h>
#include <remote-sensor/resource/stream/manager.h>

/* Static Global variables */
static Buffer fake_buffer;
static StreamBuffers fake_buffers;
static Stream* fake_stream;
static Handles fake_handle;

/* Fake function */
Buffer*
fake_buffer_create(uint8_t size)
{
    Buffer* buffer = &fake_buffer;
    buffer->size = size;
    buffer->count = 0;
    return buffer;
}

Buffer*
fake_null_buffer_creation(uint8_t size)
{
    return (Buffer*) NULL;
}

void
fake_destroy(Stream* stream)
{
    return;
}

Handles*
fake_handle_create()
{
    return &fake_handle;
}

Stream*
fake_stream_create(StreamBuffers* buffers, ResettableFlag resettable, Handles* handle)
{
    Stream* stream = &fake_stream;
    stream->buffers = buffers;
    stream->resettable = resettable;
    stream->handles = handle;

    return stream;
}

void
fake_attach(Handles* handle, Stream* stream)
{
    stream->handles = handle;
}

BufferStatus
fake_health_buffer(int fake)
{
    if (fake == 0)
    {
        return BUFFER_EMPTY;
    }
    if (fake == 1)
    {
        return BUFFER_BUSY;
    }
    if (fake == 2)
    {
        return BUFFER_FULL;
    }
    if (fake == 3)
    {
        return BUFFER_ERROR;
    }
}

StreamBuffers*
fake_buffers_create(uint8_t size, SingleFlag single)
{
    StreamBuffers* buffers = &fake_buffers;
    buffers->output = &fake_buffer;
    buffers->output->size = size;
    if (single == DOUBLE_BUFFER)
    {
        buffers->input = &fake_buffer;
    }
    buffers->single_buffer = single;
    return buffers;
}

StreamHealth
fake_reset(Stream* stream)
{
    return STREAM_DESTROYED;
}

StreamHealth
fake_health(Stream* stream)
{
    return STREAM_READY;
}

/* Fake Write Handle */
void
fake_write_handle(StreamBuffers* buffers, Request* request, Parameter* params[], Response* response)
{}

void (*buffer_manager_reset_holder)(Buffer* buffer);

void
fake_buffer_manager_reset(Buffer* buffer)
{}

/* Function holders */
Buffer* (*create_holder)(uint8_t);
Stream* (*destroy_holder)(Stream*);
void (*attach_holder)(Handles*, Stream*);
StreamHealth (*health_holder)(Stream*);
void (*reset_holder)(Buffer*);

/**
 * Will test the proper buffer creation
 */
TEST(StreamManager, create_buffer)
{
    // substitute the create call from CircularBuffer
    create_holder = CircularBuffer.create;
    CircularBuffer.create = &fake_null_buffer_creation;

    uint8_t size = 0x3;
    Buffer* result;
    result = StreamManager_create_buffer(&fake_buffers, size);

    TEST_ASSERT_NULL_MESSAGE(result, "Creating buffer did not returned null");
    TEST_ASSERT_EQUAL(BUFFER_ERROR, fake_buffers.status);

    CircularBuffer.create = &fake_buffer_create;

    result = StreamManager_create_buffer(&fake_buffers, size);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Creating the buffer returned null");
    TEST_ASSERT_EQUAL_MESSAGE(BUFFER_READY, fake_buffers.status, "Buffer status is not READY once created");
    TEST_ASSERT_EQUAL(size, result->size);

    // put back the function
    CircularBuffer.create = create_holder;
}

/**
 * Tests the creation of StreamBuffers
 */
TEST(StreamManager, create_buffers)
{
    uint8_t size = 3;
    StreamBuffers* result;
    create_holder = CircularBuffer.create;
    CircularBuffer.create = &fake_buffer_create;

    result = StreamManager_create_buffers(size, SINGLE_BUFFER);
    TEST_ASSERT_NOT_NULL(result);
    TEST_ASSERT_NOT_NULL_MESSAGE(result->output, "Output buffer is null");
    TEST_ASSERT_EQUAL(SINGLE_BUFFER, result->single_buffer);
    TEST_ASSERT_EQUAL(ISOLATED_BUFFERS, result->isolated);
    free(result);

    result = StreamManager_create_buffers(size, DOUBLE_BUFFER);
    TEST_ASSERT_NOT_NULL(result);
    TEST_ASSERT_NOT_NULL_MESSAGE(result->output, "Output buffer is null");
    TEST_ASSERT_NOT_NULL_MESSAGE(result->input, "Input buffer is null");
    TEST_ASSERT_NOT_EQUAL(SINGLE_BUFFER, result->single_buffer);
    TEST_ASSERT_EQUAL(ISOLATED_BUFFERS, result->isolated);

    result->output = NULL;  // we remove the reference to the dummy buffer
    result->input = NULL;
    free(result);
    CircularBuffer.create = create_holder;  // put back the function
}

/**
 * Tests the create handles properly
 */
TEST(StreamManager, create_handles)
{
    Handles* handle = StreamManager_create_handles();

    TEST_ASSERT_NOT_NULL(handle);

    free(handle);
    handle = NULL;
}

/**
 * Tests the Stream creation
 */
TEST(StreamManager, create)
{
    uint8_t size = 4;
    create_holder = CircularBuffer.create;
    CircularBuffer.create = &fake_buffer_create;

    Stream* out_stream = StreamManager.create(size, IS_RESETTABLE, SINGLE_BUFFER);
    TEST_ASSERT_NOT_NULL(out_stream);
    TEST_ASSERT_NOT_NULL(out_stream->buffers);
    TEST_ASSERT_NOT_NULL(out_stream->buffers->output);
    TEST_ASSERT_NOT_NULL(out_stream->handles);
    TEST_ASSERT_EQUAL(size, out_stream->buffers->output->size);
    TEST_ASSERT_EQUAL(SINGLE_BUFFER, out_stream->buffers->single_buffer);
    TEST_ASSERT_EQUAL(IS_RESETTABLE, out_stream->resettable);
    free(out_stream->handles);
    free(out_stream->buffers);

    out_stream = StreamManager.create(size, IS_RESETTABLE, DOUBLE_BUFFER);
    TEST_ASSERT_NOT_NULL(out_stream);
    TEST_ASSERT_NOT_NULL(out_stream->buffers);
    TEST_ASSERT_NOT_NULL(out_stream->buffers->output);
    TEST_ASSERT_NOT_NULL(out_stream->buffers->input);
    TEST_ASSERT_NOT_NULL(out_stream->handles);
    TEST_ASSERT_EQUAL(size, out_stream->buffers->output->size);
    TEST_ASSERT_EQUAL(size, out_stream->buffers->input->size);
    TEST_ASSERT_EQUAL(IS_RESETTABLE, out_stream->resettable);
    TEST_ASSERT_EQUAL(DOUBLE_BUFFER, out_stream->buffers->single_buffer);

    out_stream->buffers->output = NULL;
    out_stream->buffers->input = NULL;
    free(out_stream->handles);
    free(out_stream->buffers);
    free(out_stream);
    out_stream = NULL;
    CircularBuffer.create = create_holder;  // put back the function
}

/**
 * tests the behavior of destroy in case that there is
 * nothing to destroy
 */

TEST(StreamManager, destroy_handles)
{
    int called = 0;
    Stream* null_stream = NULL;
    destroy_holder = StreamManager.destroy;
    StreamManager.destroy = &StreamManager_destroy_handles;

    if(StreamManager_destroy(null_stream) == STREAM_DESTROYED)
    {
        called = 1;
    }

    TEST_ASSERT_EQUAL(0, called);

    StreamManager.destroy = destroy_holder;
}

TEST(StreamManger, destroy_buffers)
{

    int called = 0;
    Stream* null_stream = NULL;
    destroy_holder = StreamManager.destroy;
    StreamManager.destroy = &StreamManager_destroy_buffers;

    if(StreamManager_destroy(null_stream) == STREAM_DESTROYED)
    {
        called = 1;
    }

    TEST_ASSERT_EQUAL(0, called);

    StreamManager.destroy = destroy_holder;
}

TEST(StreamManager, destroy)
{
    int called = 0;
    Stream* null_stream = NULL;

    if(StreamManager_destroy(null_stream) == STREAM_DESTROYED)
    {
        called = 1;
    }

    TEST_ASSERT_EQUAL(0, called);
}

TEST(StreamManager, check_buffer_health)
{
    StreamBuffers* buffers = NULL;
    Handles handle;
    Stream* stream = fake_stream_create(buffers, IS_RESETTABLE, &handle);
    StreamHealth result = StreamManager.check_buffer_health(stream);

    // buffer is NULL

    TEST_ASSERT_EQUAL(STREAM_ERROR, result);

    //buffer is not NULL but Status is ERROR

    buffers = fake_buffers_create(3, SINGLE_BUFFER);
    stream = fake_stream_create(buffers, IS_RESETTABLE, &handle);
    stream->buffers->status = fake_health_buffer(3);
    result = StreamManager.check_buffer_health(stream);
    TEST_ASSERT_EQUAL(STREAM_ERROR, result);
    //Buffer NULL and status ERROR

    stream->buffers = NULL;
    result = StreamManager.check_buffer_health(stream);
    TEST_ASSERT_EQUAL(STREAM_ERROR, result);
    //Output is NULL:

    buffers = fake_buffers_create(3, SINGLE_BUFFER);
    stream = fake_stream_create(buffers, IS_RESETTABLE, &handle);
    stream->buffers->output = NULL;
    result = StreamManager.check_buffer_health(stream);
    TEST_ASSERT_EQUAL(STREAM_ERROR, result);

    //stream is double buffered but input is NULL

    buffers = fake_buffers_create(3, DOUBLE_BUFFER);
    stream = fake_stream_create(buffers, IS_RESETTABLE, &handle);
    stream->buffers->input = NULL;
    result = StreamManager.check_buffer_health(stream);
    TEST_ASSERT_EQUAL(STREAM_ERROR,result);
    // buffer is full:

    buffers = fake_buffers_create(3, SINGLE_BUFFER);
    stream = fake_stream_create(buffers, IS_RESETTABLE, &handle);
    stream->buffers->status = fake_health_buffer(2);
    result = StreamManager.check_buffer_health(stream);
    TEST_ASSERT_EQUAL(STREAM_BUSY, result);

    //buffer is Busy:

    stream->buffers->status = fake_health_buffer(1);
    result = StreamManager.check_buffer_health(stream);
    TEST_ASSERT_EQUAL(STREAM_BUSY, result);

    //everything is ok:

    stream->buffers->status = fake_health_buffer(0);
    result = StreamManager.check_buffer_health(stream);
    TEST_ASSERT_EQUAL(STREAM_READY, result);

}

TEST(StreamManager, check_health)
{
    Handles* handle = fake_handle_create();
    StreamBuffers* buffers = fake_buffers_create(3, SINGLE_BUFFER);
    Stream* stream = NULL;
    StreamHealth health = StreamManager.check_health(stream);

    //stream is NULL

    TEST_ASSERT_EQUAL(STREAM_ERROR, health);

    //stream  is not NULL but handle is NULL

    stream = fake_stream_create(buffers, IS_RESETTABLE, NULL);
    health = StreamManager.check_health(stream);
    TEST_ASSERT_EQUAL(STREAM_ERROR, health);
}

/**
 * tests in case that streamHelath is STREAM_READY , a write
 * handle will be created and further that this handle isn't
 * empty
 */
TEST(StreamManager, post)
{
    uint8_t size = 3;
    Request request;
    Response response;
    Parameter* parameters[1];

    StreamBuffers* buffers = fake_buffers_create(size, SINGLE_BUFFER);
    buffers->output->data = NULL;

    Handles* handles = fake_handle_create();
    Stream* stream = fake_stream_create(buffers, IS_RESETTABLE, handles);
    buffer_manager_reset_holder = BufferManager.reset;
    BufferManager.reset = fake_buffer_manager_reset;

    PeripheralWriteHandle write_handle = &fake_write_handle;
    handles->write_handle = write_handle;

    health_holder = StreamManager.check_health;
    StreamManager.check_health = &fake_health;

    Response* returned = StreamManager.write(stream, &request, parameters, &response);
    TEST_ASSERT_NOT_NULL(returned);
    TEST_ASSERT_EQUAL_PTR(&response, returned);

    StreamManager.check_health = health_holder;
    BufferManager.reset = buffer_manager_reset_holder;
}

TEST(StreamManager, clone)
{
    uint8_t size = 3;
    Handles* handle = fake_handle_create();
    StreamBuffers* buffers = StreamManager_create_buffers(size, SINGLE_BUFFER);
    Stream* stream = StreamManager.create(size, IS_RESETTABLE, SINGLE_BUFFER);
    Stream* second_stream = NULL;

    //tests if the if condition in StreamManager.clone works correctly

    stream->handles = NULL;
    second_stream = StreamManager.clone(stream, ISOLATED_BUFFERS);
    TEST_ASSERT_NULL(second_stream);

    //test second if condition

    stream->handles = handle;
    second_stream = StreamManager.clone(stream, ISOLATED_BUFFERS);
    TEST_ASSERT_EQUAL(size,second_stream->buffers->output->size);
    TEST_ASSERT_EQUAL(SINGLE_BUFFER, second_stream->buffers->single_buffer);
    TEST_ASSERT_EQUAL(IS_RESETTABLE, second_stream->resettable);
    TEST_ASSERT_EQUAL(handle, second_stream->handles);
    TEST_ASSERT_EQUAL(ISOLATED_BUFFERS, second_stream->buffers->isolated);

    //test equality of streams created by last condition

    second_stream = StreamManager.clone(stream, SHARED_BUFFERS);
    TEST_ASSERT_EQUAL(size, second_stream->buffers->output->size);
    TEST_ASSERT_EQUAL(SINGLE_BUFFER, second_stream->buffers->single_buffer);
    TEST_ASSERT_EQUAL(IS_RESETTABLE, second_stream->resettable);
    TEST_ASSERT_EQUAL(handle, second_stream->handles);
    TEST_ASSERT_EQUAL(SHARED_BUFFERS, second_stream->buffers->isolated);

    free(second_stream);
    second_stream = NULL;
    free(stream);
    stream = NULL;
    free(buffers);
    buffers = NULL;
}

/**
 * tests if StreamManager_reset_buffers works correctly
 */
TEST(StreamManager, reset)
{
    Handles* handle = fake_handle_create();
    uint8_t size = 3;
    StreamBuffers* buffers = fake_buffers_create(size, DOUBLE_BUFFER);
    Stream* stream = fake_stream_create(buffers, IS_RESETTABLE, handle);
    reset_holder = CircularBuffer.reset;
    CircularBuffer.reset = &fake_reset;
    health_holder = StreamManager.check_buffer_health;
    StreamManager.check_buffer_health = &fake_health;

    int called = 0;
    StreamManager_reset_buffers(stream);
    called = 1;
    TEST_ASSERT_EQUAL(1, called);

    CircularBuffer.reset = reset_holder;
    StreamManager.check_buffer_health = health_holder;
}


