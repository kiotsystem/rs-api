#include <unity_testing.h>
#include <remote-sensor/resource/stream/structures/circular_buffer.h>

static uint8_t buffer_size;

/** Fake functions **/
uint8_t*
fake_null_alloc(uint8_t size)
{
    return NULL;
}

uint8_t*
fake_alloc(uint8_t size)
{
    return (uint8_t*) 0x1;
}

/** Helper functions **/
void
initalize_buffer(Buffer* buffer)
{
    buffer->data = malloc(sizeof(uint8_t) * 3);
    buffer->size = 3;
    buffer->count = 0;
    buffer->end = buffer->data + buffer->size - 1;
    buffer->head = buffer->data;
    buffer->tail = buffer->data;
}

/**
 * IMPORTANT: adds data to the buffer's data and increases the counter
 * BUT does not update the pointers
 * @param Buffer* buffer
 */
void
add_data_to(Buffer* buffer)
{
    // add some data to the buffer
    *(buffer->data) = 1;
    *(buffer->data + 1) = 2;
    *(buffer->data + 2) = 3;
    buffer->count = 3;
}

/** Definitions for holding the functions temporary **/
/**
 * Function to represent the Buffer_alloc
 * @param uint8_t
 * @return uint8_t*
 */
uint8_t* (*helper)(uint8_t);

/** Tests **/

TEST(CircularBuffer, create)
{
    buffer_size = 0x2;
    // save the original function pointer
    helper = BufferFactory.create;
    // replace the pointer
    BufferFactory.create = &fake_alloc;

    Buffer* buffer = CircularBuffer.create(buffer_size);

    TEST_ASSERT_NOT_NULL(buffer);
    TEST_ASSERT_EQUAL(buffer_size, buffer->size);
    TEST_ASSERT_EQUAL(fake_alloc(buffer_size), buffer->data);
    TEST_ASSERT_EQUAL(fake_alloc(buffer_size) + buffer_size - 1, buffer->end);
    TEST_ASSERT_EQUAL(0, buffer->count);
    TEST_ASSERT_EQUAL(buffer->head, buffer->tail);

    free(buffer);
    buffer = NULL;
    // putting the original function back, for next tests
    BufferFactory.create = helper;
}

/**
 * This function handles the case of when a buffer failed to allocate
 */
TEST(CircularBuffer, null_create)
{
    buffer_size = 0x2;
    helper = BufferFactory.create;
    BufferFactory.create = &fake_null_alloc;

    Buffer* buffer = CircularBuffer.create(buffer_size);
    TEST_ASSERT_NULL(buffer);

    free(buffer);
    buffer = NULL;
    // putting the original function back, for next tests
    BufferFactory.create = helper;
}

/**
 * Tests the update pointer function
 */
TEST(CircularBuffer, update_head_pointer)
{
    Buffer buffer;
    initalize_buffer(&buffer);

    // increment the head pointer  by 1
    BufferStatus status = CircularBuffer_update_pointer(
            &buffer,
            HEAD,
            CIRCULAR_BUFFER_INCREMENT,
            CIRCULAR_BUFFER_INCREMENT);

    TEST_ASSERT_EQUAL(BUFFER_READY, status);
    TEST_ASSERT_EQUAL(buffer.data + 1, buffer.head);
    TEST_ASSERT_EQUAL(buffer.data, buffer.tail);
    TEST_ASSERT_EQUAL((int) CIRCULAR_BUFFER_INCREMENT, buffer.count);

    status = CircularBuffer_update_pointer(
            &buffer,
            HEAD,
            CIRCULAR_BUFFER_DECREMENT,
            CIRCULAR_BUFFER_DECREMENT);

    TEST_ASSERT_EQUAL(buffer.data, buffer.head);
    TEST_ASSERT_EQUAL(buffer.data, buffer.tail);
    TEST_ASSERT_EQUAL(0, buffer.count);

    free(buffer.data);
    buffer.data = NULL;
}


TEST(CircularBuffer, update_tail_pointer)
{
    Buffer buffer;
    initalize_buffer(&buffer);

    // increment the tail pointer  by 1
    BufferStatus status = CircularBuffer_update_pointer(
            &buffer,
            TAIL,
            CIRCULAR_BUFFER_INCREMENT,
            CIRCULAR_BUFFER_INCREMENT);
    TEST_ASSERT_EQUAL(BUFFER_READY, status);
    TEST_ASSERT_EQUAL(buffer.data + 1, buffer.tail);
    TEST_ASSERT_EQUAL(buffer.data, buffer.head);
    TEST_ASSERT_EQUAL((int) CIRCULAR_BUFFER_INCREMENT, buffer.count);

    status = CircularBuffer_update_pointer(
            &buffer,
            TAIL,
            CIRCULAR_BUFFER_DECREMENT,
            CIRCULAR_BUFFER_DECREMENT);
    
    TEST_ASSERT_EQUAL(BUFFER_READY, status);
    TEST_ASSERT_EQUAL(buffer.data, buffer.tail);
    TEST_ASSERT_EQUAL(buffer.data, buffer.head);
    TEST_ASSERT_EQUAL(0, buffer.count);

    free(buffer.data);
    buffer.data = NULL;
}

TEST(CircularBuffer, mix_update_pointer)
{
    Buffer buffer;
    initalize_buffer(&buffer);

    // increment the tail pointer  by 1
    BufferStatus status = CircularBuffer_update_pointer(
            &buffer,
            TAIL,
            CIRCULAR_BUFFER_INCREMENT,
            CIRCULAR_BUFFER_INCREMENT);

    TEST_ASSERT_EQUAL(BUFFER_READY, status);
    TEST_ASSERT_EQUAL(buffer.data + 1, buffer.tail);
    TEST_ASSERT_EQUAL(buffer.data, buffer.head);
    TEST_ASSERT_EQUAL((int) CIRCULAR_BUFFER_INCREMENT, buffer.count);

    status = CircularBuffer_update_pointer(
            &buffer,
            HEAD,
            CIRCULAR_BUFFER_INCREMENT,
            CIRCULAR_BUFFER_INCREMENT);
    
    TEST_ASSERT_EQUAL(BUFFER_READY, status);
    TEST_ASSERT_EQUAL(buffer.data + 1, buffer.head);
    TEST_ASSERT_NOT_EQUAL(buffer.data, buffer.head);
    TEST_ASSERT_EQUAL(buffer.head, buffer.tail);
    TEST_ASSERT_EQUAL(2, buffer.count);

    status = CircularBuffer_update_pointer(
            &buffer,
            TAIL,
            CIRCULAR_BUFFER_INCREMENT,
            CIRCULAR_BUFFER_INCREMENT);

    TEST_ASSERT_EQUAL(BUFFER_READY, status);
    TEST_ASSERT_EQUAL(buffer.data + 2, buffer.tail);
    TEST_ASSERT_EQUAL(buffer.end, buffer.tail);
    TEST_ASSERT_NOT_EQUAL(buffer.data, buffer.head);
    TEST_ASSERT_EQUAL(3, buffer.count);

    // locate the head same level than tail but decrease one the counter
    status = CircularBuffer_update_pointer(
            &buffer,
            HEAD,
            CIRCULAR_BUFFER_INCREMENT,
            CIRCULAR_BUFFER_DECREMENT);

    TEST_ASSERT_EQUAL(BUFFER_READY, status);
    TEST_ASSERT_EQUAL(buffer.data + 2, buffer.tail);
    TEST_ASSERT_EQUAL(buffer.data + 2, buffer.head);
    TEST_ASSERT_EQUAL(buffer.head, buffer.tail);
    TEST_ASSERT_EQUAL(2, buffer.count);
    
    free(buffer.data);
    buffer.data = NULL;
}

/**
 * Tests the simple implementation of the count
 */
TEST(CircularBuffer, count)
{
    Buffer buffer;
    initalize_buffer(&buffer);

    buffer.count = 2;
    TEST_ASSERT_EQUAL(2, CircularBuffer.count(&buffer));

    free(buffer.data);
    buffer.data = NULL;
}

/**
 * This test reads sequential data from the buffer
 * @see CircularBuffer
 */
TEST(CircularBuffer, get_non_empty)
{
    Buffer buffer;
    initalize_buffer(&buffer);
    add_data_to(&buffer);  // read helper function docs
    // move the head to match the state of the buffer
    buffer.head = buffer.data + 2;

    TEST_ASSERT_NOT_EQUAL(BUFFER_EMPTY, CircularBuffer.get(&buffer));
    TEST_ASSERT_EQUAL(1, CircularBuffer.get(&buffer));

    free(buffer.data);
    buffer.data = NULL;
}

/**
 * Test the behaviour of the CircularBuffer in case of the buffer being empty
 */
TEST(CircularBuffer, empty_get)
{
    Buffer buffer;
    initalize_buffer(&buffer);
    // but not adding any data
    TEST_ASSERT_EQUAL(BUFFER_EMPTY, CircularBuffer.get(&buffer));
    // now increase the counter and check still if is empty (not updating the pointers)
    buffer.count = 2;
    TEST_ASSERT_NOT_EQUAL(BUFFER_EMPTY, CircularBuffer.get(&buffer));

    free(buffer.data);
    buffer.data = NULL;
}

/**
 * Test the pop functionality
 */
TEST(CircularBuffer, pop)
{
    Buffer buffer;
    initalize_buffer(&buffer);
    add_data_to(&buffer);  // refer to the data added!
    buffer.head = buffer.data;  // point to the last element in the buffer

    uint8_t count_holder = buffer.count;

    uint8_t data = CircularBuffer.pop(&buffer);
    TEST_ASSERT_EQUAL(1, data);
    TEST_ASSERT_NOT_EQUAL(buffer.data, buffer.tail);  // prove the pointer moved
    TEST_ASSERT_NOT_EQUAL(buffer.head, buffer.tail);  // prove the are not the same
    TEST_ASSERT_EQUAL(count_holder - 1, buffer.count);  // prove the counter decreased one

    // pop again
    data = CircularBuffer.pop(&buffer);
    TEST_ASSERT_EQUAL(2, data);
    TEST_ASSERT_NOT_EQUAL(buffer.data, buffer.tail);  // prove the pointer moved
    TEST_ASSERT_NOT_EQUAL(buffer.head, buffer.tail);  // prove the are not the same
    TEST_ASSERT_EQUAL(count_holder - 2, buffer.count);  // prove the counter decreased twice
    TEST_ASSERT_EQUAL(buffer.end, buffer.tail);
    // pop again
    data = CircularBuffer.pop(&buffer);
    TEST_ASSERT_EQUAL(3, data);
    TEST_ASSERT_EQUAL(count_holder - 3, buffer.count);  // prove the counter decreased three times
    TEST_ASSERT_EQUAL(buffer.data, buffer.head);
    TEST_ASSERT_EQUAL(buffer.head, buffer.tail);

    // fail to pop because head is pointing to the end element
    // another pop means error
    data = CircularBuffer.pop(&buffer);
    TEST_ASSERT_EQUAL((uint8_t) BUFFER_ERROR, data);
    TEST_ASSERT_EQUAL(buffer.head, buffer.tail);
    // including the twice casting of the BUFFER_ERROR because is -1 and casted to unsigned
    TEST_ASSERT_EQUAL((BufferStatus)(uint8_t) BUFFER_ERROR, (BufferStatus) data);

    // move the head to data "fake push", so we can read all three elements in the buffer
    buffer.head = buffer.data + 1;
    buffer.count = 1;
    data = CircularBuffer.pop(&buffer);
    TEST_ASSERT_EQUAL(1, data);
    TEST_ASSERT_NOT_EQUAL(*(buffer.end), *(buffer.tail));
    TEST_ASSERT_EQUAL(buffer.head, buffer.tail);  // prove the pointer moved
    TEST_ASSERT_EQUAL(*(buffer.head), *(buffer.tail));  // prove the pointer moved
    TEST_ASSERT_EQUAL(count_holder - 3, buffer.count);  // prove the counter decreased twice
    TEST_ASSERT_EQUAL(0, buffer.count);

    free(buffer.data);
    buffer.data = NULL;
}

/**
 * Test the implementation of push
 */
TEST(CircularBuffer, push)
{
    Buffer buffer;
    initalize_buffer(&buffer);

    uint8_t value;

    // push the first data
    value = CircularBuffer.push(&buffer, 0x04);
    TEST_ASSERT_EQUAL(0x04, value);
    TEST_ASSERT_EQUAL(1, buffer.count);
    
    value = CircularBuffer.push(&buffer, 0x99);
    TEST_ASSERT_EQUAL(0x99, value);
    TEST_ASSERT_NOT_EQUAL(buffer.head, buffer.tail);
    TEST_ASSERT_EQUAL(2, buffer.count);
    
    value = CircularBuffer.push(&buffer, 0x48);
    TEST_ASSERT_EQUAL(0x48, value);
    TEST_ASSERT_EQUAL(buffer.head, buffer.tail);
    TEST_ASSERT_EQUAL(3, buffer.count);

    // push that fails to add data because of being full
    value = CircularBuffer.push(&buffer, 0x67);
    TEST_ASSERT_EQUAL((uint8_t) BUFFER_FULL, value);
    TEST_ASSERT_EQUAL(3, buffer.count);
    TEST_ASSERT_EQUAL(buffer.head, buffer.tail);

    value = CircularBuffer.push(&buffer, 0x67);
    TEST_ASSERT_EQUAL((uint8_t) BUFFER_FULL, value);
    TEST_ASSERT_EQUAL(buffer.head, buffer.tail);
    
    free(buffer.data);
    buffer.data = NULL;
}

/**
 * Tests the health check for a Circular Buffer
 */
TEST(CircularBuffer, check_health)
{
    Buffer buffer;
    BufferStatus status;

    buffer.data = NULL;
    buffer.size = (uint8_t) NULL;
    buffer.count = (uint8_t) NULL;

    status = CircularBuffer.check_health(&buffer);
    TEST_ASSERT_EQUAL(BUFFER_UNINITIALIZED, status);

    buffer.data = NULL;
    buffer.size = 2;
    buffer.count = 1;
    status = CircularBuffer.check_health(&buffer);
    TEST_ASSERT_EQUAL(BUFFER_ERROR, status);

    buffer.data =  (uint8_t*)0x01;
    buffer.size = 2;
    buffer.count = 0;
    status = CircularBuffer.check_health(&buffer);
    TEST_ASSERT_EQUAL(BUFFER_EMPTY, status);

    buffer.count = 1;
    buffer.size = 2;
    status = CircularBuffer.check_health(&buffer);
    TEST_ASSERT_EQUAL(BUFFER_READY, status);
}

/**
 * Clean up function tests
 */
TEST(CircularBuffer, clean_up)
{
    Buffer buffer;
    initalize_buffer(&buffer);

    buffer = *(CircularBuffer.clean_up(&buffer));
    TEST_ASSERT_EQUAL(BUFFER_UNINITIALIZED, buffer.size);
    TEST_ASSERT_EQUAL(BUFFER_EMPTY, buffer.count);
    TEST_ASSERT_NULL(buffer.data);

    // test to see if the clean up wont break on null
    CircularBuffer.clean_up((Buffer*) NULL);
}

/**
 * Reset test function
 */
TEST(CircularBuffer, reset)
{
    Buffer buffer;
    initalize_buffer(&buffer);
    add_data_to(&buffer);
    buffer.head = buffer.data + 1;
    buffer.tail = buffer.end;

    CircularBuffer.reset(&buffer);
    TEST_ASSERT_EQUAL(buffer.data, buffer.head);
    TEST_ASSERT_EQUAL(buffer.data, buffer.tail);
    TEST_ASSERT_EQUAL(0, buffer.count);

    // test to check if test does not break
    CircularBuffer.reset((Buffer*) NULL);
}

