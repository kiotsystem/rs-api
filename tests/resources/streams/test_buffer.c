#include <unity_testing.h>
#include <remote-sensor/resource/stream/structures/buffer.h>

static uint8_t size;

/**
 * The Buffer_allocate is low level api call, no function pointer substitution is required here.
 */
TEST(Buffer, buffer_allocate)
{
    size = 0x2;
    uint8_t* data = BufferFactory.create(size);
    TEST_ASSERT_NOT_NULL(data);
    free(data);
}
