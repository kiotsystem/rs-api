#include <unity_testing.h>
#include <remote-sensor/resource/navigator.h>

/* Static/Global variables */
static URIComponent root;
static URIComponent first_child;
static URIComponent second_child;
static URIComponent third_child;
static URIComponent sub_child;

/**
 * Init function for the root component
 */
static void
init_root_uri(URIComponent* comp)
{
    comp->name = ".";
    comp->resources = (Resource*) NULL;
    comp->parent = NULL;
    comp->sibling = (URIComponent*) NULL;
    comp->child = (URIComponent*) NULL;
}

/**
 * This simulates a fake tree with the strcuture as it follows:
 *
 * . (root)
 * |
 * .-- first
 * |   |
 * |   .-- *r_1
 * |
 * .-- second
 * |   |
 * |   .-- sub_child (no resources, optionally)
 * |   |   |
 * |   |   .-- *r_2_1
 * |   |
 * |   .-- *r_2
 * |
 * .-- third
 *     |
 *     .-- *r_3
 */
static void
init_tree(URIComponent* root_comp)
{
    first_child.name = "first";
    second_child.name = "second";
    third_child.name = "third";
    sub_child.name = "sub_child";

    // register components as root's child
    root_comp->child = &first_child;  // ./first_child
    first_child.child = NULL;
    first_child.sibling = &second_child;  // ./second_child
    second_child.sibling = &third_child;  // ./third_child
    third_child.sibling = NULL;
    third_child.child = NULL;
    second_child.child = &sub_child;  // ./second_child/sub_child
    sub_child.sibling = NULL;
}
/**
 * Tests the counting of elements in the tree
 */
TEST(ResourceNavigator, count)
{
    init_root_uri(&root);
    init_tree(&root);
    uint8_t count = 0;
    count = ResourceNavigator.count(&root);

    TEST_ASSERT_NOT_EQUAL(0, count);
    TEST_ASSERT_EQUAL(5, count);
}
