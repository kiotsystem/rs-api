#include "unity_testing.h"
#include <remote-sensor/resource/manager.h>

/* Static/Global variables */
static URIComponent uri_comp;
static URIComponent uri_comp_2;
static URIComponent uri_comp_3;
static URIComponent uri_sibling;

static Resource res_1;
static Resource res_2;
static Resource res_3;

static Resource sibling;

/**
 * Resets the components and resources to all NULL
 */
static void
reset_dependencies()
{
    /* reset the resources */
    uri_comp.resources = NULL;
    uri_comp_2.resources = NULL;
    uri_comp_3.resources = NULL;
    uri_sibling.resources = NULL;

    /* reset the siblings */
    uri_comp.sibling = NULL;
    uri_comp_2.sibling = NULL;
    uri_comp_3.sibling = NULL;
    uri_sibling.sibling = NULL;
    res_1.sibling = NULL;
    res_2.sibling = NULL;
    res_3.sibling = NULL;
    sibling.sibling = NULL;

    /* reset parent */
    uri_comp.parent = NULL;
    uri_comp_2.parent = NULL;
    uri_comp_3.parent = NULL;
    uri_sibling.parent = NULL;

    /* reset children */
    uri_comp.child = NULL;
    uri_comp_2.child = NULL;
    uri_comp_3.child = NULL;
    uri_sibling.child = NULL;

    /* reset the component */
    res_1.component = NULL;
    res_2.component = NULL;
    res_3.component = NULL;
    sibling.component = NULL;
}

/**
 * testcase : NULL Resource registration
 */
TEST(ResourceManager, register_null_resource)
{
    reset_dependencies();

    ResourceManagerState state;
    URIComponent * comp = (URIComponent *) NULL;
    state = ResourceManager.register_resource(&res_1, comp);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_ERROR);
    TEST_ASSERT_NULL(res_1.component);
}

/**
 * testcase : Registration of Resource with no sibling
 */
TEST(ResourceManager, register_resource_with_no_siblings)
{
    reset_dependencies();
    ResourceManagerState state;
    state = ResourceManager.register_resource(&res_2, &uri_comp);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    TEST_ASSERT_EQUAL_PTR(&res_2, uri_comp.resources);
}

/**
 * testcase : Registration of Resource with one or more sibling
 */
TEST(ResourceManager, register_resource_with_siblings)
{
    reset_dependencies();
    ResourceManagerState state;
    sibling.name = "sibling";

    uri_comp.resources = &res_1;
    res_1.sibling = &sibling;

    state = ResourceManager.register_resource(&res_2, &uri_comp);
    
    TEST_ASSERT_EQUAL(RESOURCE_MANAGER_SUCCESS, state);
    TEST_ASSERT_EQUAL_PTR(sibling.sibling, &res_2);
    TEST_ASSERT_NULL(res_2.sibling);
    TEST_ASSERT_EQUAL_PTR(res_2.component, &uri_comp);
}

/**
 * testcase : Registration of URIComponent with no child
 */
TEST(ResourceManager, register_component_with_no_child)
{
    reset_dependencies();
    ResourceManagerState state;

    state = ResourceManager.register_component(&uri_comp, &uri_comp_2);

    TEST_ASSERT_EQUAL_PTR(uri_comp_2.child, &uri_comp);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
}

/**
 *  Registration of URIComponent with one or more child
 *  testcase : Resource already exist
 */
TEST(ResourceManager, register_component_duplicate)
{
    reset_dependencies();
    ResourceManagerState state;

    uri_comp_2.child = &uri_comp_3;
    uri_comp_3.sibling = &uri_sibling;

    uri_comp.name = "comp_1";
    uri_comp_2.name = "comp_2";
    uri_comp_3.name = uri_comp.name;  // set the same name as comp_2

    state = ResourceManager.register_component(&uri_comp, &uri_comp_2);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS);
    TEST_ASSERT_EQUAL_PTR(&uri_comp_3, uri_comp_2.child);
    TEST_ASSERT_EQUAL_PTR(&uri_sibling, uri_comp_3.sibling);
    TEST_ASSERT_NULL(uri_comp.sibling);
    TEST_ASSERT_EQUAL_STRING("comp_1", uri_comp.name);
}

/**
 * testcase : Unregistration of Resource with no resources
 */
TEST(ResourceManager, unregister_null_resource)
{
    reset_dependencies();
    Resource* res = (Resource *) NULL;
    ResourceManagerState state;
    state = ResourceManager.unregister(res);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);

    state = ResourceManager.unregister(&res_2);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);
}

/**
 * testcase : Unregistration of Resource with one or more resources
 */
TEST(ResourceManager, unregister_resource)
{
    reset_dependencies();
    ResourceManagerState state;

    uri_comp.resources = &res_2;
    res_2.sibling = &res_3;
    res_3.sibling = NULL;
    res_2.component = &uri_comp;
    res_3.component = &uri_comp;
    res_2.name = "res_2";
    res_3.name = "res_3";

    state = ResourceManager.unregister(&res_3);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    TEST_ASSERT_NULL(res_2.sibling);
    TEST_ASSERT_NOT_NULL(res_2.component);
    TEST_ASSERT_EQUAL_PTR(&res_2, uri_comp.resources);
    TEST_ASSERT_EQUAL_PTR(&uri_comp, res_2.component);

    state = ResourceManager.unregister(&res_2);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    TEST_ASSERT_NULL(uri_comp.resources);
}

/**
 * Unregistration of NULL URIComponent
 * testcase 1: URIComponent is NULL
 * testcase 2: URIComponent hase no parent
 */
TEST(ResourceManager, unregister_null_component)
{
    URIComponent* comp = (URIComponent *) NULL;
    ResourceManagerState state;
    state = ResourceManager.unregister_component(comp);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);

    // resource is not head
    TEST_ASSERT_EQUAL(uri_comp.parent, NULL);
    state = ResourceManager.unregister_component(&uri_comp);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);
}

/**
 * Unregistration of NOT NULL URIComponent
 * testcase 1: same identifier
 * testcase 2: different identifier
 */
TEST(ResourceManager, unregister_component)
{
    ResourceManagerState state;

    uri_comp.child = &uri_comp_2;
    uri_comp_2.sibling = &uri_comp_3;
    uri_comp_3.sibling = NULL;

    uri_comp_2.parent = &uri_comp;
    uri_comp_3.parent = &uri_comp;

    uri_comp.name = "uri_comp";
    uri_comp_2.name = "uri_comp_2";
    uri_comp_3.name = "uri_comp_3";

    state = ResourceManager.unregister_component(&uri_comp_2);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    TEST_ASSERT_EQUAL_PTR(&uri_comp_3, uri_comp.child);
    TEST_ASSERT_NULL(uri_comp_2.parent);

    state = ResourceManager.unregister_component(&uri_comp_3);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    TEST_ASSERT_NULL(uri_comp.child);
    TEST_ASSERT_NULL(uri_comp_3.parent);
    TEST_ASSERT_NULL(uri_comp_3.sibling);
}
