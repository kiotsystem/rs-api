#include <unity_testing.h>
#include <remote-sensor/resource/manager.h>

/*Global variables*/
static URIComponent fake_root;
static URIComponent uri_comp;
static URIComponent uri_comp_2;
static URIComponent uri_comp_3;
static URIComponent uri_sibling;
static URIComponent* get_root;

static Resource res_1;
static Resource res_2;
static Resource sibling;

static Stream fake_stream;
static Handles fake_handle;

static char* fake_component_path = "/some/path";

char* (*get_path_for_component_holder)(URIComponent*);

/**
 * @return fake Handle
 */
Handles*
faked_handle_create()
{
    return &fake_handle;
}

char*
fake_get_path_for_component(URIComponent* component)
{
    char* result = malloc(strlen("root/child")+1);
    strcpy(result,"root/child");
    return result;
}

/**
 * @return a fake Stream
 */
Stream*
faked_stream_create(StreamBuffers* buffers, ResettableFlag resettable, Handles* handle)
{
    Stream* stream = &fake_stream;
    stream->buffers = buffers;
    stream->resettable = resettable;
    stream->handles = handle;

    return stream;
}

/**
 * @return stream health check status
 */
StreamHealth fake_stream_check_health(Stream* stream) {
    return STREAM_READY;
}

StreamHealth (*stream_health_holder)(Stream*);

/**
 * initializing fake call handle
 */
void
faked_call_handle(StreamBuffers* buffer, Request* request, Parameter* some_array[], Response* response){}

/**
 * initializing fake read handle
 */
uint8_t*
faked_read_handle(StreamBuffers* buffer, Request* request, Parameter* some_array[], Response* response, uint8_t* length)
{
    return (uint8_t*) NULL;
}

/**
 * initializing fake write handle
 */
uint8_t*
faked_write_handle(StreamBuffers* buffer, Request* request, Parameter* some_array[], Response* response, uint8_t* length)
{
    return (uint8_t*) NULL;
}

/**
 * Init function for a resource
 */
void
init_resource(Resource* resource)
{
    fake_root.resources = resource;
    resource->name = "resource";
    resource->data_size = 0;
    resource->failed_writes = 0;
    resource->state = RESOURCE_READY;
    resource->sibling = &res_2;
    res_2.sibling = NULL;
    res_2.component = &fake_root;
    res_2.name = "res_2";
    resource->stream = &fake_stream;
    fake_stream.buffers = NULL;
    fake_stream.handles = &fake_handle;
}

/**
 * Init function for the root component
 */
void
init_root_component(URIComponent* comp)
{
    comp->name = ".";
    comp->resources = (Resource*) NULL;
    comp->parent = NULL;
    comp->sibling = (URIComponent*) NULL;
    comp->child = (URIComponent*) NULL;
}

/**
 * Init function for the  component
 */
void
init_component(URIComponent* uricomp)
{
    uricomp->name = "uricomp";
    uricomp->resources = &res_1;
    uricomp->parent = &uri_comp;
    uricomp->sibling = &uri_sibling;
    uricomp->child = &uri_comp_2;
}

/**
 * @return a fake URIComponent
 */
URIComponent*
get_faked_root()
{
    return &fake_root;
}
URIComponent* (*get_root_holder)();

/**
 * Tests for ResourceManager_create_identifier_component
 */
TEST(ResourceManager, create_identifier_component)
{
    URIComponent* result = &fake_root;
    result = ResourceManager.create_identifier("child");

    TEST_ASSERT_EQUAL_STRING("child", result->name);
    TEST_ASSERT_NULL(result->resources);
    TEST_ASSERT_NULL(result->child);

    free(result);
    result = NULL;
}

/**
 *  test how the Resource Manager get the root component
 */
TEST(ResourceManager, get_root_component)
{
    URIComponent* uricomp;
    init_root_component(&fake_root);
    // hold the original function pointer
    get_root_holder = ResourceManager.get_root_component;
    // replace the function
    ResourceManager.get_root_component = &get_faked_root;
    uricomp = ResourceManager.get_root_component();

    TEST_ASSERT_EQUAL(fake_root.name, uricomp->name);
    TEST_ASSERT_EQUAL_PTR(&fake_root, uricomp);

    ResourceManager.get_root_component = get_root_holder;
}

/**
 * Tests for ResourceManager_create_resource
 */
TEST(ResourceManager, create_Resource)
{
    Resource* resource = ResourceManager.create("parent", IS_RESETTABLE, SINGLE_BUFFER, 0x10);
    Stream stream;
    resource->stream = &stream;

    TEST_ASSERT_EQUAL_STRING("parent", resource->name);
    TEST_ASSERT_NULL(resource->sibling);
    TEST_ASSERT_NULL(resource->component);
    TEST_ASSERT_EQUAL(0, resource->failed_writes);
    TEST_ASSERT_EQUAL(RESOURCE_READY, resource->state);

    free(resource);
    resource = NULL;
}

/**
 * Test how the Resource Manager attach a call handle when resource is NULL
 */
TEST(ResourceManager, attach_null_resource_call_handle){

    //for the case resource NULL
    Resource* resource = NULL;
    PeripheralCallHandle callhandle = &faked_call_handle;
    ResourceManagerState  state = ResourceManager.attach_call_handle(callhandle, resource);
    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);
}

/**
 * Test how the Resource Manager attach a call handle when resource is not NULL
 * and call handle is defined
 */
TEST(ResourceManager, attach_resource_call_handle) {

    init_resource(&res_1);
    stream_health_holder = StreamManager.check_health;
    StreamManager.check_health = &fake_stream_check_health;
    PeripheralCallHandle callhandle = &faked_call_handle;
    ResourceManagerState  state = ResourceManager.attach_call_handle(callhandle, &res_1);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    StreamManager.check_health = stream_health_holder;
}

/**
 * Test how the Resource Manager attach a read handle when resource is NULL
 */
TEST(ResourceManager, attach_null_resource_read_handle){
    //for the case resource NULL
    Resource* resource = NULL;
    PeripheralReadHandle readhandle = &faked_read_handle;
    ResourceManagerState  state = ResourceManager.attach_read_handle(readhandle, resource);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);
}

/**
 * Test how the Resource Manager attach a read handle when resource is not NULL
 * and read handle is defined
 */
TEST(ResourceManager, attach_resource_read_handle){

    init_resource(&res_1);
    stream_health_holder = StreamManager.check_health;
    StreamManager.check_health = &fake_stream_check_health;
    PeripheralReadHandle readhandle = &faked_read_handle;
    ResourceManagerState  state = ResourceManager.attach_read_handle(readhandle, &res_1);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    StreamManager.check_health = stream_health_holder;
}

/**
 * Test how the Resource Manager attach a write handle when resource is NULL
 */
TEST(ResourceManager, attach_null_resource_write_handle){
    //for the case resource NULL
    Resource* resource = NULL;
    PeripheralWriteHandle writehandle = &faked_write_handle;
    ResourceManagerState  state = ResourceManager.attach_write_handle(writehandle, resource);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);
}

/**
 * Test how the Resource Manager attach a write handle when resource is not NULL
 * and write handle is defined
 */
TEST(ResourceManager, attach_resource_write_handle){

    init_resource(&res_1);
    stream_health_holder = StreamManager.check_health;
    StreamManager.check_health = &fake_stream_check_health;
    PeripheralWriteHandle writehandle = &faked_write_handle;
    ResourceManagerState  state = ResourceManager.attach_write_handle(writehandle, &res_1);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    StreamManager.check_health = stream_health_holder;
}

/**
 * Test how the Resource Manager attach all three handles
 * that is, the call, read and write handle when resource is NULL
 */
TEST(ResourceManager, attach_null_resource_handles){

    //for the case resource NULL
    Resource* resource = NULL;
    PeripheralCallHandle callhandle = &faked_call_handle;
    PeripheralReadHandle readhandle = &faked_read_handle;
    PeripheralWriteHandle writehandle = &faked_write_handle;
    ResourceManagerState  state = ResourceManager.attach_handles(resource, callhandle, readhandle, writehandle);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);
}

/**
 * Test how the Resource Manager attach all three defined handles
 * that is, the call, read and write handle when resource is not NULL
 */
TEST(ResourceManager, attach_resource_handles){

    init_resource(&res_1);
    stream_health_holder = StreamManager.check_health;
    StreamManager.check_health = &fake_stream_check_health;
    PeripheralCallHandle callhandle = &faked_call_handle;
    PeripheralReadHandle readhandle = &faked_read_handle;
    PeripheralWriteHandle writehandle = &faked_write_handle;
    ResourceManagerState  state = ResourceManager.attach_handles(&res_1, callhandle, readhandle, writehandle);

    TEST_ASSERT_EQUAL(state, RESOURCE_MANAGER_SUCCESS);
    StreamManager.check_health = stream_health_holder;
}

/**
 * Deletion of resource, if resource is NULL
 */
TEST(ResourceManager, delete_null_resource)
{
    Resource* res = NULL;
    ResourceManagerState result = ResourceManager.delete(res);
    TEST_ASSERT_EQUAL(result, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);
}

/**
 * Deletion of uricomponent whose child is not  NULL
 */
TEST(ResourceManager, delete_component_with_child)
{
    //URIComponent uri_comp;
    init_component(&uri_comp);
    ResourceManagerState result = ResourceManager.delete_component(&uri_comp);
    TEST_ASSERT_EQUAL(result, RESOURCE_MANAGER_RESOURCE_NOT_FOUND);
}

/**
 * Tests ResourceManager_concat
 */
TEST(ResourceManager, string_concat)
{
    char* result;

    // test for NULL String
    result = string_concat(NULL, "something");
    TEST_ASSERT_NULL(result);

    // test for valid Strings
    result =  string_concat("./", "something");
    TEST_ASSERT_EQUAL_STRING("./something", result);
    free(result);
    result = NULL;
}

/**
 * Tests ResourceManager_get_path_for_component
 */
TEST(ResourceManager, get_path_for_component)
{
    URIComponent fake_root;
    URIComponent fake_child;
    URIComponent fake_grandchild;
    char* result;
    fake_root.parent = (URIComponent*) NULL;
    fake_child.parent = &fake_root;
    fake_grandchild.parent = &fake_child;

    // test for NULL component
    result = ResourceManager.get_path_for_component(NULL);
    TEST_ASSERT_NULL(result);

    // base case where everything is named
    fake_root.name = "root";
    fake_child.name = "child";
    fake_grandchild.name = "grandchild";
    result = ResourceManager.get_path_for_component(&fake_grandchild);
    TEST_ASSERT_EQUAL_STRING("root/child/grandchild", result);
    free(result);
    result = NULL;

    // test to hide the root
    fake_root.name = ".";
    result = ResourceManager.get_path_for_component(&fake_grandchild);
    TEST_ASSERT_EQUAL_STRING("/child/grandchild", result);
    free(result);
    result = NULL;

    //test for child's name is NULL
    fake_child.name = NULL;
    result = ResourceManager.get_path_for_component(&fake_grandchild);
    TEST_ASSERT_EQUAL_STRING("/grandchild", result);
    free(result);
    result = NULL;

    // test for root is NULL
    fake_child.name = "child";
    fake_root.name = NULL;
    result = ResourceManager.get_path_for_component(&fake_grandchild);
    TEST_ASSERT_EQUAL_STRING("/child/grandchild", result);
    free(result);
    result = NULL;

    //test if given component is NULL
    fake_grandchild.name = NULL;
    result = ResourceManager.get_path_for_component(&fake_grandchild);
    TEST_ASSERT_NULL(result);
}


/**
 * Tests ResourceManager_get_path_for_resource
 */
TEST(ResourceManager, get_path_for_resource)
{
    get_path_for_component_holder = ResourceManager.get_path_for_component;
    ResourceManager.get_path_for_component = &fake_get_path_for_component;
    URIComponent fake_root;
    URIComponent fake_child;
    Resource fake_resource;
    fake_child.resources = &fake_resource;
    fake_resource.name = "GET";
    fake_resource.component = &fake_child;
    char* result;
    fake_root.parent = (URIComponent*) NULL;
    fake_child.parent = &fake_root;


    // test for NULL component
    result = ResourceManager.get_path_for_resource(NULL);
    TEST_ASSERT_NULL(result);

    // base case
    fake_root.name = "root";
    fake_child.name = "child";
    fake_resource.sibling = NULL;
    result = ResourceManager.get_path_for_resource(&fake_resource);
    TEST_ASSERT_EQUAL_STRING("root/child", result);
    free(result);
    result = NULL;

    Resource fake_resource_sibling;
    fake_resource_sibling.name = "fake_me";
    fake_resource.sibling = &fake_resource_sibling;
    result = ResourceManager.get_path_for_resource(&fake_resource);
    TEST_ASSERT_EQUAL_STRING("root/child/?action=GET", result);
    free(result);
    result = NULL;

    ResourceManager.get_path_for_component = get_path_for_component_holder;

}
