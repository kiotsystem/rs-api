#include <unity_testing.h>
#include <remote-sensor/resource/locator.h>

/* Static/Global variables */
static URIComponent root;
static URIComponent first_child;
static URIComponent second_child;
static URIComponent third_child;
static URIComponent sub_child;

static Resource r_1;
static Resource r_2;
static Resource r_3;
static Resource r_2_1;

/* Helper functions */

/**
 * Init function for the root component
 */
void
init_root_uri_component(URIComponent* comp)
{
    comp->name = ".";
    comp->resources = (Resource*) NULL;
    comp->parent = NULL;
    comp->sibling = (URIComponent*) NULL;
    comp->child = (URIComponent*) NULL;
}

/**
 * Sibling linkage
 * r_1 -> r_2 -> r_2_1 -> r_3
 */
void
init_component_with_resources(URIComponent* component)
{
    component->resources = &r_1;
    r_1.sibling = &r_2;
    r_1.name = "temp";
    r_2.sibling = &r_2_1;
    r_2.name = "amount";
    r_2_1.sibling = &r_3;
    r_2_1.name = "random";
    r_3.name = "reset";
    r_3.sibling = NULL;
}

void
reset_resources()
{
    r_1.name = "";
    r_1.sibling = NULL;
    r_2.name = "";
    r_2.sibling = NULL;
    r_2_1.name = "";
    r_2_1.sibling = NULL;
    r_3.name = "";
    r_3.sibling = NULL;
}

/**
 * To use here in the tests
 */
typedef enum TestOptions
{
    NO_SUB_RESOURCES,
    WITH_SUB_RESOURCES
} TestOptions;

/**
 * This simulates a fake tree with the strcuture as it follows:
 *
 * . (root)
 * |
 * .-- first
 * |   |
 * |   .-- *r_1
 * |
 * .-- second
 * |   |
 * |   .-- sub_child (no resources, optionally)
 * |   |   |
 * |   |   .-- *r_2_1
 * |   |
 * |   .-- *r_2
 * |
 * .-- third
 *     |
 *     .-- *r_3
 */
void
init_fake_tree(URIComponent* root_comp, TestOptions sub_resources_option)
{
    first_child.name = "first";
    second_child.name = "second";
    third_child.name = "third";
    sub_child.name = "sub_child";

    // register components as root's child
    root_comp->child = &first_child;  // ./first_child
    first_child.sibling = &second_child;  // ./second_child
    second_child.sibling = &third_child;  // ./third_child
    third_child.sibling = NULL;
    second_child.child = &sub_child;  // ./second_child/sub_child
    sub_child.sibling = NULL;

    r_1.component = &first_child;
    r_1.name = "r_1";
    r_1.sibling = (Resource *) NULL;
    first_child.resources = &r_1;

    r_2.component = &second_child;
    r_2.name = "r_2";
    r_2.sibling = (Resource *) NULL;
    second_child.resources = &r_2;

    r_3.component = &third_child;
    r_3.name = "r_3";
    third_child.resources = &r_3;

    if (sub_resources_option == WITH_SUB_RESOURCES)
    {
        r_2_1.name = "r_2_1";
        r_2_1.sibling = (Resource *) NULL;
        sub_child.resources = &r_2_1;
        r_2_1.component = &sub_child;
    }
}

/**
 * This test case aims to search for the root component
 * This can be acheived by searching for the following routes:
 * + "."
 * + "./"
 * + "/"
 * + ""
 */
TEST(ResourceLocator, search_for_root)
{
    init_root_uri_component(&root);
    init_fake_tree(&root, NO_SUB_RESOURCES);
    char* url;
    URIComponent* result;
    url = "";
    TEST_ASSERT_NOT_EQUAL(url, (char*) NULL);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(root.name, ".", "Root should be named '.'");

    url = ".";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Search should not be null for '.'");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(root.name, result->name, "Names do not match");
    TEST_ASSERT_EQUAL_PTR(&root, result);

    url = "";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Search should not be null for '.'");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(root.name, result->name, "Names do not match");
    TEST_ASSERT_EQUAL_PTR(&root, result);

    url = "/";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Result should not be null for '/'");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(root.name, result->name, "Names do not match");
    TEST_ASSERT_EQUAL_PTR(&root, result);

    url = "./";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Search should not be null for '.'");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(root.name, result->name, "Names do not match");
    TEST_ASSERT_EQUAL_PTR(&root, result);
}

TEST(ResourceLocator, get_last_sibling_component)
{
    init_root_uri_component(&root);
    init_fake_tree(&root, NO_SUB_RESOURCES);
    URIComponent* end = (URIComponent*) NULL;
    ResourceManagerState state;

    state = ResourceLocator.get_last_sibling_component(&first_child, "third", &end);
    TEST_ASSERT_NOT_NULL(end);
    TEST_ASSERT_EQUAL_MESSAGE(RESOURCE_MANAGER_SUCCESS, state, "Resource State was not success");
    TEST_ASSERT_EQUAL_PTR(&third_child, end);
    TEST_ASSERT_EQUAL_STRING("third", end->name);

    end = (URIComponent*) NULL;  // reset the end pointer

    state = ResourceLocator.get_last_sibling_component(&second_child, "third", &end);
    TEST_ASSERT_NOT_NULL(end);
    TEST_ASSERT_EQUAL_MESSAGE(RESOURCE_MANAGER_SUCCESS, state, "Resource State was not success");
    TEST_ASSERT_EQUAL_PTR(&third_child, end);
    TEST_ASSERT_EQUAL_STRING("third", end->name);

    end = (URIComponent*) NULL;  // reset the end pointer

    state = ResourceLocator.get_last_sibling_component(&sub_child, "sub_child", &end);
    TEST_ASSERT_NOT_NULL(end);
    TEST_ASSERT_EQUAL_MESSAGE(RESOURCE_MANAGER_SUCCESS, state, "Resource State was not success");
    TEST_ASSERT_EQUAL_PTR(&sub_child, end);
    TEST_ASSERT_EQUAL_STRING("sub_child", end->name);
}

/**
 * Test the search component sibling
 */
TEST(ResourceLocator, search_for_component_sibling)
{
    init_root_uri_component(&root);
    init_fake_tree(&root, NO_SUB_RESOURCES);
    char* name;
    URIComponent* result;

    name = "third";
    result = ResourceLocator.search_component_sibling(name, (URIComponent*) &first_child);
    TEST_ASSERT_NOT_NULL(result);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(third_child.name, result->name, "The result of searching the first's sibling is not the expected value");
}

/**
 * Tests the result in case that a null value was passed.
 */
TEST(ResourceLocator, null_search_component)
{
    init_root_uri_component(&root);
    init_fake_tree(&root, NO_SUB_RESOURCES);
    char* url = (char *) NULL;
    URIComponent* result;

    result = ResourceLocator.search_component(url, &root);

    TEST_ASSERT_NULL_MESSAGE(result, "Result was not null");
    TEST_ASSERT_NOT_EQUAL_MESSAGE(&root, result, "The root is not equal");
}

/**
 * Tests that an invalid URL does not crash teh application
 */
TEST(ResourceLocator, invalid_search)
{
    init_root_uri_component(&root);
    init_fake_tree(&root, NO_SUB_RESOURCES);
    char* url;
    URIComponent *result;

    url = "sadfadsa";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NULL_MESSAGE(result, "Found something that does not exist");

    url = "sadfadsa/adafd/";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NULL_MESSAGE(result, "Found something that does not exist");

    url = "/sadfadsa/adafd/";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NULL_MESSAGE(result, "Found something that does not exist");
}

/**
 * This test case uses the ResourceLocator's search sibling functionality.
 * Not worth to substitude, besides is search_sibling is tested before this test case gets executed.
 */
TEST(ResourceLocator, search_component)
{
    init_root_uri_component(&root);
    init_fake_tree(&root, NO_SUB_RESOURCES);
    char* url;
    URIComponent *result;

    url = "/first";
    URIComponent *comp = root.child;
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL(root.child);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Result should not be null");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(comp->name, result->name, "Resources are not the same");

    url = "/first/";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Result should not be null");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(comp->name, result->name, "Resources are not the same");

    url = "/second";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Searching for second should not be null");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(second_child.name, result->name, "Result should be the second component");

    url = "/second/";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Searching for second should not be null");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(second_child.name, result->name, "Result should be the second component");

    url = "/third";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Searching for third should not be null");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(third_child.name, result->name, "Result should be the third component");

    url = "/third/";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Searching for third should not be null");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(third_child.name, result->name, "Result should be the third component");

    url = "/second/sub_child";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Searching for the second's sub_child should not be null");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(sub_child.name, result->name, "Result should be the seconds's sub_child component");

    url = "/second/sub_child/";
    result = ResourceLocator.search_component(url, &root);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "Searching for the second's sub_child should not be null");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(sub_child.name, result->name, "Result should be the seconds's sub_child component");
}

/**
 * Tests the search in URIComponent
 */
TEST(ResourceLocator, search_in)
{
    URIComponent uri;
    reset_resources();
    init_component_with_resources(&uri);
    Resource* actual_resource;

    actual_resource = ResourceLocator.search_in(&uri, (uint8_t*) "temp");
    TEST_ASSERT_EQUAL_STRING(r_1.name, actual_resource->name);
    TEST_ASSERT_EQUAL_PTR(&r_1, actual_resource);

    actual_resource = ResourceLocator.search_in(&uri, (uint8_t*) "amount");
    TEST_ASSERT_EQUAL_PTR(&r_2, actual_resource);
    TEST_ASSERT_EQUAL_STRING("amount", actual_resource->name);

    actual_resource = ResourceLocator.search_in(&uri, (uint8_t*) "blabla");
    TEST_ASSERT_NULL(actual_resource);
}

