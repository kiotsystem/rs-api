#include <unity_testing.h>
#include <remote-sensor/communication/marshalers/response.h>

/**
 * Creates an ACK response with the following:
 * 0x45 transaction ID
 * data length 3 (0x3)
 * data: all 0xff
 */
uint8_t*
ack_response(uint8_t* response_data)
{
    response_data[0] = ACK;  // response type
    response_data[1] = 0x45;  // transaction id
    response_data[2] = 0x03;  // data length

    for (uint8_t i = 0x3; i < 0x6; i++)
    {
        response_data[i] = 0xff;
    }

    return response_data;
}

/**
 * Creates an ACK response with the following:
 * 0x45 transaction ID
 * data length 3 (0x3)
 * data: all 0xff
 */
uint8_t*
not_found_response(uint8_t* response_data)
{
    response_data[0] = NOT_FOUND;  // response type
    response_data[1] = 0x45;  // transaction id
    response_data[2] = 0x00;  // data length

    return response_data;
}

/**
 * Tests the demarshalling of the ResponseMarshaler
 */
TEST(ResponseMarshaler, demarshal)
{
    Response response;
    response.type = ACK;
    response.transaction_id = 0x45;
    response.word_size = 0x01;
    response.content_type = UNSIGNED_TYPE;
    response.data_length = 3;
    uint8_t data[3] = {0xff, 0xff, 0xff};
    size_t data_length = 0;
    response.data = data;

    uint8_t* response_data = ResponseMarshaler.demarshal(&response, &data_length);

    TEST_ASSERT_EQUAL(8, data_length);
    TEST_ASSERT_EQUAL_HEX(ACK, *response_data);
    TEST_ASSERT_EQUAL_HEX(0x45, *(response_data + RESPONSE_TRANSACTION_OFFSET));
    TEST_ASSERT_EQUAL_HEX(UNSIGNED_TYPE, *(response_data + RESPONSE_CONTENT_TYPE_OFFSET));
    TEST_ASSERT_EQUAL_HEX(0x3, *(response_data + RESPONSE_DATA_LENGTH_OFFSET));

    free(response_data);
    response_data = NULL;

    response.type = NOT_FOUND;
    response.content_type = SIGNED_TYPE;
    response.data_length = 0x0;
    response_data = ResponseMarshaler.demarshal(&response, &data_length);
    TEST_ASSERT_EQUAL(5, data_length);
    TEST_ASSERT_EQUAL_HEX(NOT_FOUND, *response_data);
    TEST_ASSERT_EQUAL_HEX(0x45, *(response_data + RESPONSE_TRANSACTION_OFFSET));
    TEST_ASSERT_EQUAL_HEX(SIGNED_TYPE, *(response_data + RESPONSE_CONTENT_TYPE_OFFSET));
    TEST_ASSERT_EQUAL_HEX(0x0, *(response_data + RESPONSE_DATA_LENGTH_OFFSET));

    free(response_data);
    response_data = NULL;
}
