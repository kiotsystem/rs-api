#include <unity_testing.h>
#include <remote-sensor/communication/marshalers/request.h>

/**
 * Creates a GET request with the following:
 * GET type
 * 0x45 transaction ID
 * uri length 18 (0x12)
 * uri: all 0xff
 */
uint8_t*
get_request(uint8_t* request_data)
{
    request_data[0] = 0x01;  // GET request
    request_data[1] = 0x45;  // transaction id
    request_data[2] = 0x01;  // connection_type ONE_TIME
    request_data[3] = 0x12;  // uri length

    for (uint8_t i = 0x4; i < 0x16; i++)
    {
        request_data[i] = 0xff;
    }

    return request_data;
}

/**
 * Fakes a POST Request
 */
uint8_t*
post_request(uint8_t* request_data)
{
    request_data[0] = 0x02;  // POST request
    request_data[1] = 0x45;  // transaction id
    request_data[2] = 0x01;  // connection_type ONE_TIME
    request_data[3] = 0x10;  // uri length

    for (uint8_t i = 0x4; i < 0x14; i++)
    {
        request_data[i] = 0xAA;
    }

    return request_data;
}

/**
 * @param uint8_t* requests_data
 * @return uint8_t*
 */
uint8_t*
registration_request(uint8_t* request_data)
{
    request_data[0] = REGISTRATION;
    request_data[1] = 0x45;
    request_data[2] = ONE_TIME;
    request_data[3] = 0;

    return request_data;
}

/**
 * Test the marshalling of a request, based on sequential data
 */
TEST(RequestMarshaler, marshal)
{
    uint8_t request_data[22];
    uint8_t* data = get_request(request_data);
    Request* result_request = RequestMarshaler.marshal(data);

    TEST_ASSERT_EQUAL_HEX(0x1, *data);
    TEST_ASSERT_NOT_NULL(result_request);
    TEST_ASSERT_EQUAL(GET, result_request->type);
    TEST_ASSERT_EQUAL_HEX(0x45, result_request->transaction_id);
    TEST_ASSERT_EQUAL_HEX(0x01, result_request->connection_type);
    TEST_ASSERT_EQUAL_HEX(0x12, result_request->uri_length);

    uint8_t* uri_reference = result_request->uri;
    for (uint8_t i = 0; i < 0x12; i++)
    {
        TEST_ASSERT_EQUAL_HEX(0xFF, *(uri_reference + i));
    }

    free(result_request->uri);
    free(result_request);
    result_request = NULL;

    data = post_request(request_data);
    result_request = RequestMarshaler.marshal(data);

    TEST_ASSERT_EQUAL_HEX(0x02, *data);
    TEST_ASSERT_NOT_NULL(result_request);
    TEST_ASSERT_EQUAL(POST, result_request->type);
    TEST_ASSERT_EQUAL_HEX(0x45, result_request->transaction_id);
    TEST_ASSERT_EQUAL_HEX(0x01, result_request->connection_type);
    TEST_ASSERT_EQUAL_HEX(0x10, result_request->uri_length);

    free(result_request->uri);
    free(result_request);
    result_request = NULL;

    data = registration_request(request_data);
    result_request = RequestMarshaler.marshal(data);

    TEST_ASSERT_EQUAL_HEX(REGISTRATION, *data);
    TEST_ASSERT_NOT_NULL(result_request);
    TEST_ASSERT_EQUAL(REGISTRATION, result_request->type);
    TEST_ASSERT_EQUAL_HEX(0x45, result_request->transaction_id);
    TEST_ASSERT_EQUAL_HEX(ONE_TIME, result_request->connection_type);
    TEST_ASSERT_EQUAL_HEX(0x0, result_request->uri_length);
    TEST_ASSERT_NULL(result_request->uri);
    free(result_request);
    result_request = NULL;
}

/**
 * Tests the demarshal of the Requestmarshaler
 */
TEST(RequestMarshaler, demarshal)
{
    Request request;
    request.connection_type = ONE_TIME;
    request.transaction_id = 0x45;
    request.type = GET;
    request.uri_length = 2;
    request.uri = (URI) "ab";
    size_t length = 0;

    uint8_t* request_data = RequestMarshaler.demarshal(&request, &length);
    TEST_ASSERT_NOT_NULL(request_data);
    TEST_ASSERT_EQUAL(6, length);
    TEST_ASSERT_EQUAL_HEX(0x1, *request_data);
    TEST_ASSERT_EQUAL_HEX(0x45, *(request_data + REQUEST_TRANSACTION_OFFSET));
    TEST_ASSERT_EQUAL_HEX(ONE_TIME, *(request_data + REQUEST_CONNECTION_OFFSET));

    free(request_data);
    request_data = NULL;
}
