#include <unity_testing.h>
#include <remote-sensor/communication/response/manager.h>

static Response dummy_response;

static Response*
dummy_create_response()
{
    return &dummy_response;
}

Response* (*create_response_holder)();

/**
 * Tests the creation of an error response
 */
TEST(ResponseManager, create_error)
{
    Response* error_response = NULL;
    TransactionID trans = 0x01;
    create_response_holder = ResponseManager.create;

    error_response = ResponseManager.create_error(trans);

    TEST_ASSERT_EQUAL(ERROR_FOUND, error_response->type);
    TEST_ASSERT_EQUAL(trans, error_response->transaction_id);
    TEST_ASSERT_EQUAL(UNSIGNED_TYPE, error_response->content_type);
    TEST_ASSERT_EQUAL(1, error_response->word_size);
    TEST_ASSERT_EQUAL(0, error_response->data_length);
    TEST_ASSERT_NULL(error_response->data);

    ResponseManager.create = create_response_holder;
}
