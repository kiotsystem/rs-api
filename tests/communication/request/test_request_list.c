#include <unity_testing.h>
#include <remote-sensor/communication/request/list.h>

static RequestIterator iterator;

static void
init_iterator()
{
    iterator.head = NULL;
    iterator.end = NULL;
    iterator.current = NULL;
    iterator.size = 3;
    iterator.count = 0;
}

static RequestIterator* (*get_iterator_holder)();

RequestIterator* fake_get_iterator()
{
    return &iterator;
}

static Request fake_request;
static RequestElement* first_element;
static RequestElement* second_element;
static RequestElement* third_element;

static void set_fake_list()
{
    fake_request.uri = (URI) "/switch/";
    fake_request.transaction_id = 0xAA;
    iterator.count = 3;
    iterator.size = 4;

    RequestElement* element = malloc(sizeof(RequestElement));
    element->request = &fake_request;
    element->frequency = 1;
    element->next = NULL;
    first_element = element;
    iterator.head = element;

    element = malloc(sizeof(RequestElement));
    element->request = &fake_request;
    element->frequency = 2;
    element->next = NULL;
    second_element = element;
    first_element->next = second_element;

    element = malloc(sizeof(RequestElement));
    element->request = &fake_request;
    element->frequency = 3;
    element->next = NULL;
    third_element = element;
    second_element->next = third_element;
    iterator.end = third_element;
}

static void fake_reset_iterator()
{
    iterator.current = iterator.head;
}

/**
 * Tests the init function
 */
TEST(RequestList, init)
{
    init_iterator();
    get_iterator_holder = RequestList.get_iterator;
    RequestList.get_iterator = fake_get_iterator;
    size_t list_size = 4;

    RequestList.init(list_size);

    TEST_ASSERT_EQUAL(0, iterator.count);
    TEST_ASSERT_EQUAL(list_size, iterator.size);
    TEST_ASSERT_NOT_NULL(&iterator);
    TEST_ASSERT_NULL(iterator.head);
    TEST_ASSERT_NULL(iterator.current);
    TEST_ASSERT_NULL(iterator.end);

    RequestList.get_iterator = get_iterator_holder;
}

/**
 * Tests the case when adding a request
 */
TEST(RequestList, add)
{
    /* init */
    Request request_to_add;
    request_to_add.uri = "/switch/";
    uint8_t frequency = 0x01;

    init_iterator();
    get_iterator_holder = RequestList.get_iterator;
    RequestList.get_iterator = fake_get_iterator;

    /* test */
    RequestList.add(&request_to_add, frequency);
    RequestElement* element_added = iterator.head;

    TEST_ASSERT_EQUAL(1, iterator.count);
    TEST_ASSERT_NOT_NULL(iterator.head);
    TEST_ASSERT_NOT_NULL(iterator.end);
    TEST_ASSERT_NULL(iterator.current);
    TEST_ASSERT_EQUAL_PTR(iterator.head, iterator.end);
    TEST_ASSERT_EQUAL_PTR(element_added->request, &request_to_add);
    TEST_ASSERT_EQUAL_HEX(0x01, element_added->frequency);
    TEST_ASSERT_EQUAL_STRING("/switch/", element_added->request->uri);

    Request request_to_add_2;
    request_to_add_2.uri = (URI) "/temp/";
    frequency = 5;

    RequestList.add(&request_to_add_2, frequency);

    TEST_ASSERT_EQUAL(2, iterator.count);
    TEST_ASSERT_NOT_NULL(iterator.head);
    TEST_ASSERT_EQUAL_PTR(element_added, iterator.head);  // proof it is the same head
    TEST_ASSERT_NOT_NULL(iterator.end);
    TEST_ASSERT_NOT_EQUAL(iterator.end, iterator.head);
    TEST_ASSERT_EQUAL_PTR(&request_to_add_2, (iterator.end)->request);
    TEST_ASSERT_EQUAL(frequency, (iterator.end)->frequency);

    /* teardown */
    free(iterator.head);
    iterator.head = NULL;
    free(iterator.end);
    iterator.end = NULL;

    RequestList.get_iterator = get_iterator_holder;
}

/**
 * Test the case when removing the head
 */
TEST(RequestList, remove)
{
    /* init */
    init_iterator();
    set_fake_list();
    get_iterator_holder = RequestList.get_iterator;
    RequestList.get_iterator = fake_get_iterator;

    /* test */
    Request* request = RequestList.remove();
    TEST_ASSERT_NOT_NULL(request);
    TEST_ASSERT_NULL(iterator.current);
    TEST_ASSERT_EQUAL_PTR(&fake_request, request);
    TEST_ASSERT_EQUAL(2, iterator.count);
    TEST_ASSERT_EQUAL(4, iterator.size);
    TEST_ASSERT_NOT_NULL(iterator.head);
    TEST_ASSERT_NOT_NULL(iterator.end);
    TEST_ASSERT_EQUAL_PTR(second_element, iterator.head);

    request = RequestList.remove();
    TEST_ASSERT_NOT_NULL(request);
    TEST_ASSERT_NULL(iterator.current);
    TEST_ASSERT_EQUAL_PTR(&fake_request, request);
    TEST_ASSERT_EQUAL(1, iterator.count);
    TEST_ASSERT_EQUAL(4, iterator.size);
    TEST_ASSERT_NOT_NULL(iterator.head);
    TEST_ASSERT_NOT_NULL(iterator.end);
    TEST_ASSERT_EQUAL_PTR(third_element, iterator.head);
    TEST_ASSERT_EQUAL_PTR(third_element, iterator.end);
    TEST_ASSERT_EQUAL_PTR(iterator.end, iterator.head);

    request = RequestList.remove();
    TEST_ASSERT_NOT_NULL(request);
    TEST_ASSERT_EQUAL_PTR(&fake_request, request);
    TEST_ASSERT_NULL(iterator.head);
    TEST_ASSERT_NULL(iterator.current);
    TEST_ASSERT_NULL(iterator.end);
    TEST_ASSERT_EQUAL(0, iterator.count);
    TEST_ASSERT_EQUAL(4, iterator.size);

    request = RequestList.remove();
    TEST_ASSERT_NULL(request);
    TEST_ASSERT_NULL(iterator.head);
    TEST_ASSERT_NULL(iterator.current);
    TEST_ASSERT_NULL(iterator.end);
    TEST_ASSERT_EQUAL(0, iterator.count);
    TEST_ASSERT_EQUAL(4, iterator.size);

    /* teardown */
    RequestList.get_iterator = get_iterator_holder;
}

/**
 * Tests the search by transaction Id
 */
TEST(RequestList, get_by_transaction)
{
    /* init */
    init_iterator();
    set_fake_list();
    get_iterator_holder = RequestList.get_iterator;
    RequestList.get_iterator = fake_get_iterator;

    Request second_fake_request;
    second_fake_request.transaction_id = 0x34;
    Request third_fake_request;
    third_fake_request.transaction_id = 0xFF;
    second_element->request = &second_fake_request;
    third_element->request = &third_fake_request;

    /* tests */
    RequestElement* element = RequestList.get_by_transaction(0xFF);

    TEST_ASSERT_NOT_NULL(element);
    TEST_ASSERT_EQUAL_PTR(&third_fake_request, element->request);
    TEST_ASSERT_EQUAL(0xFF, element->request->transaction_id);
    TEST_ASSERT_EQUAL_PTR(third_element, element);
    TEST_ASSERT_EQUAL_PTR(first_element, iterator.head);
    TEST_ASSERT_EQUAL_PTR(second_element, (iterator.head)->next);
    TEST_ASSERT_EQUAL_PTR(third_element, iterator.end);

    element = RequestList.get_by_transaction(0x34);

    TEST_ASSERT_NOT_NULL(element);
    TEST_ASSERT_EQUAL_PTR(&second_fake_request, element->request);
    TEST_ASSERT_EQUAL(0x34, element->request->transaction_id);
    TEST_ASSERT_EQUAL_PTR(second_element, element);
    TEST_ASSERT_EQUAL_PTR(first_element, iterator.head);
    TEST_ASSERT_EQUAL_PTR(second_element, (iterator.head)->next);
    TEST_ASSERT_EQUAL_PTR(third_element, iterator.end);

    element = RequestList.get_by_transaction(0xDD);
    TEST_ASSERT_NULL(element);

    /* Teardown */
    RequestList.get_iterator = get_iterator_holder;
    free(first_element);
    first_element = NULL;
    free(second_element);
    second_element = NULL;
    free(third_element);
    third_element = NULL;
}

/**
 * Removes an element based from a transaction ID
 */
TEST(RequestList, remove_by_transaction)
{
    /* init */
    init_iterator();
    set_fake_list();
    get_iterator_holder = RequestList.get_iterator;
    RequestList.get_iterator = fake_get_iterator;

    Request second_fake_request;
    second_fake_request.transaction_id = 0x34;
    Request third_fake_request;
    third_fake_request.transaction_id = 0xFF;
    second_element->request = &second_fake_request;
    third_element->request = &third_fake_request;
    Request* result = NULL;

    /* Tests */
    result = RequestList.remove_by_transaction(0xBB);
    TEST_ASSERT_NULL(result);
    TEST_ASSERT_EQUAL_PTR(first_element, iterator.head);
    TEST_ASSERT_EQUAL_PTR(second_element, (iterator.head)->next);
    TEST_ASSERT_EQUAL_PTR(third_element, second_element->next);
    TEST_ASSERT_EQUAL_PTR(third_element, iterator.end);
    TEST_ASSERT_EQUAL(3, iterator.count);

    result = RequestList.remove_by_transaction(0xAA);  // remove the first_element
    TEST_ASSERT_NOT_NULL(result);
    TEST_ASSERT_EQUAL_PTR(result, &fake_request);
    TEST_ASSERT_EQUAL_PTR(second_element, iterator.head);
    TEST_ASSERT_EQUAL_PTR(third_element, iterator.end);
    TEST_ASSERT_NULL((iterator.end)->next);
    TEST_ASSERT_EQUAL(2, iterator.count);

    result = RequestList.remove_by_transaction(0xFF);  // remove the last element
    TEST_ASSERT_NOT_NULL(result);
    TEST_ASSERT_EQUAL_PTR(&third_fake_request, result);
    TEST_ASSERT_NOT_NULL(iterator.end);
    TEST_ASSERT_NOT_NULL(iterator.head);
    TEST_ASSERT_EQUAL_PTR(second_element, iterator.head);
    TEST_ASSERT_EQUAL_PTR(second_element, iterator.end);
    TEST_ASSERT_EQUAL(1, iterator.count);

    result = RequestList.remove_by_transaction(0x34);  // remove the last element of the list
    TEST_ASSERT_NOT_NULL(result);
    TEST_ASSERT_EQUAL_PTR(&second_fake_request, result);
    TEST_ASSERT_NULL(iterator.head);
    TEST_ASSERT_NULL(iterator.end);
    TEST_ASSERT_EQUAL(0, iterator.count);

    result = RequestList.remove_by_transaction(0xAA);  // remove something it does not exist
    TEST_ASSERT_NULL(result);
    TEST_ASSERT_NULL(iterator.head);
    TEST_ASSERT_NULL(iterator.end);
    TEST_ASSERT_EQUAL(0, iterator.count);
    TEST_ASSERT_EQUAL(4, iterator.size);

    /* Teardown */
    RequestList.get_iterator = get_iterator_holder;
}

/**
 * Tests the resetting of the iterator
 */
TEST(RequestList, reset_iterator)
{
    init_iterator();
    set_fake_list();
    get_iterator_holder = RequestList.get_iterator;
    RequestList.get_iterator = fake_get_iterator;

    TEST_ASSERT_NULL(iterator.current);

    RequestList.reset_iterator();

    TEST_ASSERT_NOT_NULL(iterator.current);
    TEST_ASSERT_EQUAL_PTR(first_element, iterator.current);

    RequestList.get_iterator = get_iterator_holder;
    free(first_element);
    first_element = NULL;
    free(second_element);
    second_element = NULL;
    free(third_element);
    third_element = NULL;
}

/**
 * Tests the iterate functionality of the List
 */
TEST(RequestList, iterate)
{
    /* init */
    init_iterator();
    set_fake_list();
    get_iterator_holder = RequestList.get_iterator;
    RequestList.get_iterator = fake_get_iterator;

    Request second_fake_request;
    second_fake_request.transaction_id = 0x34;
    Request third_fake_request;
    third_fake_request.transaction_id = 0xFF;
    second_element->request = &second_fake_request;
    third_element->request = &third_fake_request;

    /* Tests */
    RequestElement* element = RequestList.iterate();
    TEST_ASSERT_NULL(element);  // we haven't reset the iterator

    fake_reset_iterator();
    TEST_ASSERT_NOT_NULL(iterator.current);

    element = RequestList.iterate();
    TEST_ASSERT_NOT_NULL(element);
    TEST_ASSERT_EQUAL_PTR(first_element, element);
    TEST_ASSERT_EQUAL(3, iterator.count);

    element = RequestList.iterate();
    TEST_ASSERT_NOT_NULL(element);
    TEST_ASSERT_EQUAL_PTR(second_element, element);
    TEST_ASSERT_EQUAL(3, iterator.count);

    element = RequestList.iterate();
    TEST_ASSERT_NOT_NULL(element);
    TEST_ASSERT_EQUAL_PTR(third_element, element);
    TEST_ASSERT_EQUAL(3, iterator.count);

    element = RequestList.iterate();
    TEST_ASSERT_NULL(element);

    element = RequestList.iterate();
    TEST_ASSERT_NULL(element);

    /* Teardown */
    RequestList.get_iterator = get_iterator_holder;
    free(first_element);
    first_element = NULL;
    free(second_element);
    second_element = NULL;
    free(third_element);
    third_element = NULL;
}

