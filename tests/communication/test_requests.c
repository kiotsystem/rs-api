#include <unity_testing.h>
#include <remote-sensor/communication/request/manager.h>

/**
 * Tests the get uri is not destructive and actually extracts the uri
 * without the query
 */
TEST(RequestManager, get_path)
{
    Request request;
    request.uri = (URI) "/some/uri/?param=1&another=2";
    char* uri = RequestManager.get_path(&request);

    TEST_ASSERT_EQUAL_STRING("/some/uri/", uri);
    TEST_ASSERT_EQUAL_STRING("/some/uri/?param=1&another=2", request.uri);
    free(uri);

    request.uri = (URI) "/some/uri/";
    uri = RequestManager.get_path(&request);
    TEST_ASSERT_EQUAL_STRING("/some/uri/", uri);
    free(uri);
}

/**
 * Tests the search algorithm that searches for the Parameter that has the desired key.
 */
TEST(RequestManager, search_by_key)
{
    Parameter* params[4];
    Parameter p1;
    p1.key = (uint8_t*) "help";
    Parameter p2;
    p2.key = (uint8_t*) "action";
    Parameter p3;
    p3.key = (uint8_t*) "else";
    params[0] = &p1;
    params[1] = &p2;
    params[2] = &p3;
    params[3] = NULL;

    // search for parameter help
    Parameter* result = RequestManager.search_by_key("help", params);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "The search should not have returned null");
    TEST_ASSERT_EQUAL_STRING(p1.key, result->key);
    TEST_ASSERT_EQUAL_PTR(&p1, result);

    // search for parameter else
    result = RequestManager.search_by_key("else", params);
    TEST_ASSERT_NOT_NULL_MESSAGE(result, "The search should not have returned null");
    TEST_ASSERT_EQUAL_PTR(&p3, result);
    TEST_ASSERT_EQUAL_STRING(p3.key, result->key);

    // search for something that is not found
    result = RequestManager.search_by_key("lol", params);
    TEST_ASSERT_NULL(result);
}

/**
 * Tests if the action was passed as a query parameter
 */
TEST(RequestManager, get_action)
{
    Parameter* params[4];
    Parameter p1;
    p1.key = (uint8_t*) "help";
    Parameter p2;
    p2.key = (uint8_t*) "action";
    Parameter p3;
    p3.key = (uint8_t*) "else";
    params[0] = &p1;
    params[1] = &p2;
    params[2] = &p3;
    params[3] = NULL;

    Parameter* result = RequestManager.get_action(params);
    TEST_ASSERT_EQUAL_STRING((uint8_t*) "action", result->key);
    TEST_ASSERT_EQUAL_PTR(&p2, result);

    p1.key = (uint8_t*) "action";
    p2.key = (uint8_t*) "else";
    p3.key = (uint8_t*) "help";

    result = RequestManager.get_action(params);
    TEST_ASSERT_EQUAL_STRING((uint8_t*) "action", result->key);
    TEST_ASSERT_EQUAL_PTR(&p1, result);

    p1.key = (uint8_t*) "haha";
    p2.key = (uint8_t*) "haha";
    p3.key = (uint8_t*) "haha";
    result = RequestManager.get_action(params);
    TEST_ASSERT_NULL(result);
}

/**
 * Tests get frequency
 */
TEST(RequestManager, get_frequency)
{
    Parameter* params[4];
    Parameter p1;
    p1.key = (uint8_t*) "help";
    Parameter p2;
    p2.key = (uint8_t*) "freq";
    p2.value = (uint8_t*) "42";
    params[0] = &p1;
    params[1] = &p2;
    params[2] = NULL;

    uint8_t result = RequestManager.get_frequency(params);
    TEST_ASSERT_EQUAL(42, result);

    p2.key = (uint8_t*) "nullish";
    result = RequestManager.get_frequency(params);
    TEST_ASSERT_EQUAL(1, result);

}



/**
 * This test case tests that the extracting of parameters from one request can be done by iterating over it
 */
TEST(RequestManager, get_query_parameter)
{
    Request request;
    request.uri = (URI) "/some/uri?query=1&param=2";
    Parameter* parameter;
    parameter = RequestManager.get_query_parameter(&request);

    TEST_ASSERT_EQUAL_STRING("query", parameter->key);
    TEST_ASSERT_EQUAL_STRING("1", parameter->value);
    free(parameter);

    parameter = RequestManager.get_query_parameter(&request);
    TEST_ASSERT_EQUAL_STRING("param", parameter->key);
    TEST_ASSERT_EQUAL_STRING("2", parameter->value);
    free(parameter);

    TEST_ASSERT_EQUAL_STRING("/some/uri?query=1&param=2", request.uri);
}

/**
 * Tests the counting of parameters
 */
TEST(RequestManager, count_parameters)
{
    Request request;
    request.uri = (URI) "/some/path";
    uint8_t count;
    
    count = RequestManager.count_parameters(&request);
    TEST_ASSERT_EQUAL(0, count);

    request.uri = (URI) "/some/path?temp=2";
    count = RequestManager.count_parameters(&request);
    TEST_ASSERT_EQUAL(1, count);

    request.uri = (URI) "/some/?temp=2&hello=2";
    count = RequestManager.count_parameters(&request);
    TEST_ASSERT_EQUAL(2, count);
}

/**
 * Since the RequestManager saves a state then is required an integration test
 * among different Requests
 */
TEST(RequestManager, get_query_parameter_random)
{
    Request request_1;
    Request request_2;
    request_1.uri = (URI) "/some/uri/?string=1&stuff=3";
    request_2.uri = (URI) "/Some/another/?another=3";
    Parameter* parameter;

    parameter = RequestManager.get_query_parameter(&request_1);
    TEST_ASSERT_EQUAL_STRING("string", parameter->key);
    TEST_ASSERT_EQUAL_STRING("1", parameter->value);
    RequestManager.destroy_parameter(parameter);

    parameter = RequestManager.get_query_parameter(&request_2);
    TEST_ASSERT_EQUAL_STRING("another", parameter->key);
    TEST_ASSERT_EQUAL_STRING("3", parameter->value);
    RequestManager.destroy_parameter(parameter);

    parameter = RequestManager.get_query_parameter(&request_2);
    TEST_ASSERT_NULL(parameter->key);
    TEST_ASSERT_NULL(parameter->value);
    RequestManager.destroy_parameter(parameter);

    parameter = RequestManager.get_query_parameter(&request_1);
    TEST_ASSERT_EQUAL_STRING("string", parameter->key);
    TEST_ASSERT_EQUAL_STRING("1", parameter->value);
    RequestManager.destroy_parameter(parameter);

    parameter = RequestManager.get_query_parameter(NULL);
    TEST_ASSERT_NULL(parameter);
}

/**
 * Integration test for the get_query_parameters function
 */
TEST(RequestManager, get_query_parameters)
{
    Request request;
    request.uri  = (URI) "/some_uri/?a=3&b=5";
    uint8_t params_size = 4;
    Parameter* parameters[params_size];
    uint8_t parameter_count = 0;

    parameter_count = RequestManager.get_query_parameters(&request, parameters, params_size);


    TEST_ASSERT_EQUAL(2, parameter_count);
    TEST_ASSERT_NOT_NULL(parameters[0]);
    TEST_ASSERT_EQUAL_STRING("a", parameters[0]->key);
    TEST_ASSERT_EQUAL_STRING("3", parameters[0]->value);

    TEST_ASSERT_NOT_NULL(parameters[1]);
    TEST_ASSERT_EQUAL_STRING("b", parameters[1]->key);
    TEST_ASSERT_EQUAL_STRING("5", parameters[1]->value);

    TEST_ASSERT_NULL(parameters[2]);

    RequestManager.reset(parameters, parameter_count);

    TEST_ASSERT_NULL(parameters[0]);
    TEST_ASSERT_NULL(parameters[1]);
    TEST_ASSERT_NULL(parameters[2]);
}
