/**
 * @file CommunicationMiddlewareAPI.h
 *
 * This file is required only for unit testing, in order to avoid compiler issues.
 * This shall never be included in production environment!
 *
 * \brief DUMMY BEHAVIOUR!
 */
#ifndef DUMMY_COMMUNICATION_MIDDLEWARE_H_
#define DUMMY_COMMUNICATION_MIDDLEWARE_H_

#include <stdint.h>
#include <stdbool.h>

/**
 * CommunicationMiddlewareAPI_init Call definition
 */
void
CommunicationMiddlewareAPI_init(void);

/**
 * \struct CommunicationMiddlewareAPI
 * If this file was included mind that the documentation stays truth to the date 22.07.2017, however this file is a dummy file and not all
 * functions are implemented!
 *
 * Include at your own will...
 */
struct CommunicationMiddlewareAPI
{
	/**
	 *
	 * This functions are not used in
	 * all implementations currently
	 * and might get deprecated.
	 */
	void (*init) (void);

	/**
	 *
	 * This functions are not used in
	 * all implementations currently
	 * and might get deprecated.
	 */
	int8_t (*receiveFrame) (void);

	/**
	 * Send messages to other network devices.
	 * @param[in] address the array where the destination address is stored
	 * @param[in] payload the array containing the payload that shall be sent
	 * @param[in] payload_size specifies the number of bytes in the payload array that shall be sent
	 * @param[in] option the options that can be specified here are currently depending on the used protocol, in most cases using no options is fine. Enter 0 for no options.
	 */
	int8_t (*sendFrameTo16BitAddress) (const uint8_t *address,
			const uint8_t *payload, uint8_t payload_size, uint8_t option);
	int8_t (*sendFrameTo64BitAddress) (const uint8_t *address,
			const uint8_t *payload, uint8_t payload_size, uint8_t option);

	/**
	 * Tells you if a new frame has been received.
	 * @return true if a new valid frame has been received since the last call, false otherwise
	 * Use this function to check if a new frame has been received.
	 * It returns true the first time it is called after a frame
	 * with a correct checksum was received. Every following call
	 * to the function without receiving a new message in the
	 * mean time will yield false.
	 */
	bool (*receivedNewFrame) (void);

	/**
	 *
	 * @return the payload size of the current received message
	 */
	uint8_t (*getReceivedPayloadSize) (void);

	/**
	 * Copies the current messages payload to the specified destination.
	 * Currently the internal buffer for the received message might
	 * get overwritten during the execution of this command.
	 * However this might/will change in the future.
	 * @param[out] payload_destination
	 */
	void (*getReceivedPayload) (uint8_t *payload_destination);

	/**
	 * Copies the address to the specified destination.
	 * @param[out] array_to_hold_16bit_address pointer to the array where the address shall be stored
	 */
	int8_t (*get16BitSourceAddressOfReceivedMessage) (uint8_t *array_to_hold_16bit_address);
	int8_t (*get64BitSourceAddressOfReceivedMessage) (uint8_t *array_to_hold_64bit_address);

	/**
	 * This is an interrupt handler used by
	 * the code controlling the hardware module.
	 * Do not call it explicitly somewhere else.
	 */
	void (*interruptHandler) (void);
};

struct CommunicationMiddlewareAPI CommunicationMiddlewareAPI;

#endif /* DUMMY_COMMUNICATION_MIDDLEWARE_H_ */
