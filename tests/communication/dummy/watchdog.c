#include "watchdog.h"

/**
 * Initialises the Watchdog
 * This implementation only allows 120ms and above implementations, in the parameter it means above 2 (max. avaiable in 328p and 2650 MCU's is 7 -> 2 seconds)
 * @see WDTO_120MS
 * @see WDTO_250MS
 * @see WDTO_500MS
 * @see WDTO_1S
 * @see WDTO_2S
 * @param uint8_t
 */
void
CommunicationControllerRecovery_init(uint8_t interval)
{
}
/**
 * Taps the Dog
 */
void
CommunicationControllerRecovery_ok()
{
}

/**
 * Cancels the watchdog
 */
void
CommunicationControllerRecovery_cancel()
{
}

struct CommunicationControllerRecovery CtrlRecovery = {
    .init = CommunicationControllerRecovery_init,
    .ok = CommunicationControllerRecovery_ok,
    .cancel = CommunicationControllerRecovery_cancel
};

