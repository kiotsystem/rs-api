#ifndef COMMUNICATION_DUMMY_CONTROLLER_WATCHDOG_H_
#define COMMUNICATION_DUMMY_CONTROLLER_WATCHDOG_H_

#include <inttypes.h>

/**
 * \typedef TimeInterval
 */
typedef enum
TimeInterval {
    T120MS = 3,
    T250MS = 4,
    T500MS = 5,
    T1S = 6,
    T2S = 7
} TimeInterval;
/**

* \struct CommunicationControllerRecovery
 * DUMMY: This is a wrapper for the Watchdog, if dummy present this API is for testing purposes only
 */
struct CommunicationControllerRecovery
{
    /**
     * Initialises the Watchdog
     * This implementation only allows 120ms and above implementations, in the parameter it means above 2 (max. avaiable in 328p and 2650 MCU's is 7 -> 2 seconds)
     * @see WDTO_120MS
     * @see WDTO_250MS
     * @see WDTO_500MS
     * @see WDTO_1S
     * @see WDTO_2S
     * @param uint8_t
     */
    void (*init)(TimeInterval);
    /**
     * Taps the Dog
     */
    void (*ok)();
    /**
     * Cancels the watchdog
     */
    void (*cancel)();
};

extern struct CommunicationControllerRecovery CtrlRecovery;

#endif /* COMMUNICATION_DUMMY_CONTROLLER_WATCHDOG_H_ */
