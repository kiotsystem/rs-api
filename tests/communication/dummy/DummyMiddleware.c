/**
 * Mind that this are dummy functions!!!
 */
#include "CommunicationMiddlewareAPI.h"

/**
Just define it
*/
void
CommunicationMiddlewareAPI_init(void)
{}

/**
 * Return some data to fullfil the API
 */
bool
DummyCommMiddleAPI_receive_new_frame()
{
    return true;
}

int8_t
DummyCommMiddleAPI_send_frame_16_bit_address(const uint8_t* address, const uint8_t* payload, uint8_t payload_size, uint8_t option)
{
    return 0x00;
}

int8_t
DummyCommMiddleAPI_send_frame_64_bit_address(const uint8_t* address, const uint8_t* payload, uint8_t payload_size, uint8_t option)
{
    return 0x00;
}

uint8_t
DummyCommMiddleAPI_get_payload_size()
{
    return 0x1;
}

void
DummyCommMiddleAPI_get_payload(uint8_t* dest)
{
    *dest = 0xAA;
}

/**
 * Initialized the Dummy API
 */
struct CommunicationMiddlewareAPI CommunicationMiddlewareAPI = {
    .receivedNewFrame = DummyCommMiddleAPI_receive_new_frame,
    .getReceivedPayloadSize = DummyCommMiddleAPI_get_payload_size,
    .getReceivedPayload = DummyCommMiddleAPI_get_payload,
    .sendFrameTo16BitAddress = DummyCommMiddleAPI_send_frame_16_bit_address,
    .sendFrameTo64BitAddress = DummyCommMiddleAPI_send_frame_64_bit_address
};

