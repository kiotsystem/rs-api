/**
 * Logger implementation with USART Controller
 *
 * usart_logger.c
 *
 */

#include <stdlib.h>
#include <string.h>
#include "usart_controller.h"
#include "logger.h"


/***** Helper functions ******/

LoggerLevel LOG_LEVEL = DEBUG;

/**
 * This function just returns the logging code so the message can be interpreted
 * as being level dependent
 * "This is a debug message" -> "DEB:This is a debug message"
 */
char*
Logger_get_log_code(LoggerLevel level)
{
    char* prefix;
    // log level codes
    switch (level)
    {
        case DEBUG:
            prefix = "DEB:";
            break;
        case INFO:
            prefix = "INF:";
            break;
        case WARNING:
            prefix = "WAR:";
            break;
        case CRITICAL:
            prefix = "CRI:";
            break;
        case ERROR:
            prefix = "ERR:";
            break;
        case OUT:
            prefix = "OUT:";
            break;
        default:
            prefix = "DEB:";
            break;
    }
    
    return prefix;
};

/* End helper functions */

/**
 * Initializes the logger with the desired configuration.
 * @param uint16_t baud rate, if applies, give it the baud rate to use in the usart
 * @param int flushes at initialization
 * @param LoggerLevel The level of the logger (DEBUG, INFO, WARNING, …).
 */
void
Logger_init(uint16_t baud_rate, int clear, LoggerLevel level)
{
    USART.init(baud_rate, clear);
    LOG_LEVEL = level;
};

/**
 * This function is the aggregation of all calls, will call the USART implementation
 * to output.
 * @param char* The message to print out.
 * @param LoggerLevel The level of the logger (DEBUG, INFO, WARNING, …).
 */
void
Logger_log(char* message, LoggerLevel level)
{
    if (level >= LOG_LEVEL)
    {
        char* logger_code = Logger_get_log_code(level);
        USART.write(logger_code);
        USART.write(message);
        USART.newLine();
    } // else do nothing
};

/**
 * @param int32_t number
 * @param LoggerLevel The level of the logger (DEBUG, INFO, WARNING, …).
 */
void
Logger_log_numbers(int32_t number, LoggerLevel level)
{
    char* logger_code = Logger_get_log_code(level);
    USART.write(logger_code);
    USART.writeNumbers(number);
    USART.newLine();
}

/**
 * Prints the message as a warning
 * @param char* The message to print out.
 */
void
Logger_warn(char* message)
{
    Logger_log(message, WARNING);
};

/**
 * Logs the message as a debug message
 * @param char* The message to print out.
 */
void
Logger_debug(char* message)
{
    Logger_log(message, DEBUG);
};

/**
 * Prints the message as info
 * @param char* The message to print out.
 */
void
Logger_info(char* message)
{
    Logger_log(message, INFO);
};

/**
 * Logs the message as a normal output, this is important to note that
 * this has the highest priority of all Levels, this adds no other characters
 * @param char* The message to print out.
 */
void
Logger_out(char* message)
{
    Logger_log(message, OUT);
};

/**
 * Prints the message as an error
 * @param char* The message to print out.
 */
void
Logger_error(char* message)
{
    Logger_log(message, ERROR);
};

/**
 * Prints the message as critical
 * @param char* The message to print out.
 */
void
Logger_critical(char* message)
{
    Logger_log(message, CRITICAL);
};

/**
 * Expose the Logger with the assigned functions
 */
struct Logger Logger = 
{
    .init = Logger_init,
    .log = Logger_log,
    .debug = Logger_debug,
    .info = Logger_info,
    .warn = Logger_warn,
    .critical = Logger_critical,
    .error = Logger_error,
    .out = Logger_out,
    .numbers = Logger_log_numbers
};

