/*
 * usart_controller.h
 *
 */

#ifndef COMMUNICATIONS_USART_H_
#define COMMUNICATIONS_USART_H_

#include <avr/io.h>
#include <stdio.h>
#include <inttypes.h>

/* USART API */
struct USART
{
    /**
     * Flushes the current USART buffer
     */
	void (*flush)();
    /**
     * This function initializes the USART in the board
     * @param uint16_t baudRate
     * @param int clear
     */
	void (*init)(uint16_t baudRate, int clear);
	/**
	 * Gets one character from the bus.
	 * @return uint8_t The byte to return
	 */
	uint8_t (*get)(void);
	/**
	 * Reads a sentence.
	 * @param char*
	 * @return uint16_t
	 */
	uint16_t (*read)(char *s);
	/**
	 *
	 * @param int32_t number
	 */
	void (*out)(char character);
	/**
	 * Writes a series of numbers (as characters).
	 * @param int32_t number
	 */
	void (*writeNumbers)(int32_t n);
	/**
	 * Writes a series of characters, depends on append.
	 * @param char* someString
	 */
	void (*write)(char* s);
	/**
	 * Writes a fresh new line.
	 */
	void (*newLine)();
};
extern struct USART USART;  // Expose the structure

#endif /* COMMUNICATIONS_USART_H_ */
