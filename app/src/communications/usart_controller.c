#include "usart_controller.h"

#define NEW_LINE 0xA
#define CR 0xD

/**
 * Flushes the buffer.
 */
void
USART_flush()
{
	while(UCSR0A & (1 << RXC0))
    {
		unsigned char dummy = UDR0;
	}
};

/**
 * Initializes the USART as a 8N1 Channel Format.
 * @param baud rate
 * @param int clear
 */
void
USART_init(uint16_t baudRate, int clear)
{

	uint16_t final_baud_val;

	//calculate BAUD Rate
	final_baud_val = F_CPU / (baudRate * 16L) - 1;
	UBRR0H = (uint8_t) (final_baud_val >> 8);
	UBRR0L = (uint8_t) final_baud_val;

	//Enable receiving and sending
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
	//use 8N1 (bit/n-parity/1stop-bit) USART format
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);

	if (clear == 1) USART_flush();

};

/**
 * Appends a character in the bus.
 * @param char character
 */
void
USART_append(char character)
{

	//wait until USART is ready to send
	while (!(UCSR0A & (1 << UDRE0)));
	//set UDR0 to the value to send
	UDR0 = character;

};

/**
 * Writes a fresh new line.
 */
void
USART_newLine()
{
	USART.out(NEW_LINE);
};

/**
 * Gets one character from the bus.
 * @return uint8_t The byte to return
 */
uint8_t
USART_get()
{
	//wait until byte available on bus
	while (!(UCSR0A & (1 << RXC0)));
	//return that byte
	return UDR0;
};

/**
 * Reads a sentence.
 * @param char*
 * @return uint16_t
 */
uint16_t
USART_read(char* someString)
{
	//Read bytes until newline occurs
	uint16_t count = 0;
	char charReceived;
	do
    {
		charReceived = USART.get();
		*someString = charReceived;
		someString++;
		count++;
	} while (charReceived != 0x0A);
	//return number of read bytes (including newline)
	return count;
};

/**
 * Writes a series of numbers (as characters).
 * @param int32_t number
 */
void
USART_writeNumbers(int32_t number)
{
	char buf[20];
	sprintf(buf, "0x%lx", number);
	USART.write(buf);
};

/**
 * Writes a series of characters, depends on append.
 * @param char* someString
 */
void
USART_write(char* someString)
{
	while (*someString)
    {
		USART.out(*someString);
		someString++;
	}
};

/* definition of our structure */
struct USART USART =
{
		.flush = USART_flush,
		.init = USART_init,
		.out = USART_append,
		.newLine = USART_newLine,
		.read = USART_read,
		.get = USART_get,
		.writeNumbers = USART_writeNumbers,
		.write = USART_write
};

