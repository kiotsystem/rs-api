/*
 * logger.h
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <inttypes.h>

/**
 * LoggerLevel enumeration defines the hierarchy of the amount of information to log
 * It assigns a numeric value to them in an ascending order.
 */
typedef enum
LoggerLevel
{
    DEBUG = 0,
    INFO = 1,
    WARNING = 2,
    CRITICAL = 3,
    ERROR = 4, // only show ERRORs
    OUT = 5 // using only OUT means the only messages explicitly called with the OUT parameter
}
LoggerLevel;

/**
 *  Logger API
 */
struct Logger
{
    /**
     * Initializes the Logger with the desired configuration.
     * @param uint16_t baud rate, if applies, give it the baud rate to use in the usart
     * @param int flush, flushes at initialization
     * @param LoggerLevel level to log messages
     */
    void (*init)(uint16_t baud_rate, int flush, LoggerLevel level);
    /**
     * This is the base logger call which will send data to the targeted output
     * @param char* message
     * @param LoggerLevel level
     */
    void (*log)(char* message, LoggerLevel level);
    /**
     * Shortcut method that will call the Logger.log function but with a predefined LoggerLevel
     * @param char* message
     */
    void (*debug)(char* message);
    /** 
     * Shortcut method that will call the Logger.log function but with a predefined LoggerLevel
     * @param char* message
     */
    void (*info)(char* message);
    /**
     * Shortcut method that will call the Logger.log function but with a predefined LoggerLevel
     * @param char* message
     */
    void (*warn)(char* message);
    /**
     * Shortcut method that will call the Logger.log function but with a predefined LoggerLevel
     * @param char* message
     */
    void (*critical)(char* message);
    /**
     * Shortcut method that will call the Logger.log function but with a predefined LoggerLevel
     * @param char* message
     */
    void (*error)(char* message);
    /**
     * Shortcut method that will call the Logger.log function but with a predefined LoggerLevel
     * @param char* message
     */
    void (*out)(char* message);
    /**
     * This call is meant to be used to log numbers only, so the number will be printed in hex format
     * @param int32_t number
     * @param LoggerLevel level
     */
    void (*numbers)(int32_t number, LoggerLevel level);
};
extern struct Logger Logger; // expose the Logger structure
#endif /* LOGGER_H_ */
