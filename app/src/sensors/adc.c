/*
 * ADC_custom.c
 *
 *  Created on: 28 Jun 2017
 */
#include <stdint.h>
#include "adc.h"


/**
 * Initiialize the ADC
 */
void
ADC_init ()
{
	// AREF = AVcc
	ADMUX = (1<<REFS0);
	// ADC Enable and prescaler of 128
	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
}

/**
 * Read value of the ADC from the given pin
 * @param uint8_t pin (analog pin)
 * @return uint16_t value read from the pin
 */
uint16_t
ADC_read(uint8_t pin)
{

	ADMUX	&=	0xf0;
	ADMUX	|=	pin;

    // start conversion
	ADCSRA |= _BV(ADSC);
    while ((ADCSRA & _BV(ADSC)));
    ADCSRA |= (1<<ADSC);

    return ADC;
}

