/*
 * ACD_custom.h
 *
 *  Created on: 28 Jun 2017
 */
#ifndef LIB_VENDOR_ADC_CUSTOM_H_
#define LIB_VENDOR_ADC_CUSTOM_H_

#include <avr/io.h>
#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/delay.h>

/**
 * Initiialize the ADC
 */
void
ADC_init();

/**
 * Read value of the ADC from the given pin
 * @param uint8_t pin (analog pin)
 * @return uint16_t value read from the pin
 */
uint16_t
ADC_read(uint8_t);

#endif /* LIB_VENDOR_ADC_CUSTOM_H_ */
