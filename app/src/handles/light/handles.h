/**
 * @file light_handles.h
 *
 */
#ifndef APP_SRC_HANDLES_LIGHT_LIGHT_HANDLES_H_
#define APP_SRC_HANDLES_LIGHT_LIGHT_HANDLES_H_

#include "../../app/src/sensors/adc.h"
#include <remote-sensor/api.h>

/**
 * Light GET handler
 * @see PeripheralReadHandle
 * @param StreamBuffers* Buffer for the values read.
 * @param Request*
 * @param Parameter*[] The parameters of the URI
 * @param Response*
 */
void
Light_get_handler(StreamBuffers* buffers, Request* request, Parameter* parameters[], Response* response);

#endif /* APP_SRC_HANDLES_LIGHT_LIGHT_HANDLES_H_ */
