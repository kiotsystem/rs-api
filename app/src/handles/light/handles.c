/**
 * light_handles.c
 *
 */

#include "handles.h"

/**
 * Light GET handler
 * @see PeripheralReadHandle
 * @param StreamBuffers* Buffer for the values read.
 * @param Request*
 * @param Parameter*[] The parameters of the URI
 * @param Response*
 */
void
Light_get_handler(StreamBuffers* buffers, Request* request, Parameter* parameters[], Response* response)
{
    Parameter* reading_parameter = RequestManager.search_by_key("readings", parameters);
    uint8_t readings = reading_parameter == NULL ? 1 : (uint8_t) atoi((char*) reading_parameter->value);

    uint8_t readings_left = readings;
    uint8_t values[2];

    while (readings_left > 0 && BufferManager.buffer_can_fit(2, buffers->output))
    {
        uint16_t reading = ADC_read(1);
        values[0] = reading & 0xFF;
        values[1] = (reading >> 8);

        BufferManager.push(values[0], buffers->output);
        BufferManager.push(values[1], buffers->output);

        readings_left--;
    }

    response->word_size = 0x2;  // 2 Bytes word size
    response->content_type = UNSIGNED_TYPE;
}
