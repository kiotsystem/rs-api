/**
 * @file handles_index.h
 *
 *This file shall contain all the handles in one file
 */
#ifndef HANDLES_INDEX_H_
#define HANDLES_INDEX_H_

#include "dipswitch/handles.h"
#include "temperature/handles.h"
#include "light/handles.h"

#endif /* HANDLES_INDEX_H_ */
