/*
 * temperature_handles.h
 *
 */
#ifndef APP_SRC_HANDLES_TEMPERATURE_TEMPERATURE_HANDLES_H_
#define APP_SRC_HANDLES_TEMPERATURE_TEMPERATURE_HANDLES_H_

#include "../../app/src/sensors/adc.h"
#include <remote-sensor/api.h>

/**
 * Temperature GET handler
 * @see PeripheralReadHandle
 * @param StreamBuffers* Buffer for the values read.
 * @param Request*
 * @param Parameter*[] The parameters of the URI
 * @param Response*
 */
void
Temperature_get_handler(StreamBuffers* buffers, Request* request, Parameter* parameters[], Response* response);

#endif  /* APP_SRC_HANDLES_TEMPERATURE_TEMPERATURE_HANDLES_H_ */
