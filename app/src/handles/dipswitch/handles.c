/**
 * dipswitch_handles.c
 */
#include "handles.h"

/**
 *  Handle for the dipswitch
 *  @see PeripheralReadHandle
 *  @param StreamBuffers* Buffer for the values read.
 *  @param Request*
 *  @param Parameter*[] The parameters of the URI
 *  @param Response*
 */
void
DipSwitch_get_handler(StreamBuffers* buffers, Request* request, Parameter* parameters[], Response* response)
{
    Parameter* reading_parameter = RequestManager.search_by_key("readings", parameters);
    uint8_t readings = reading_parameter == NULL ? 1 : (uint8_t) atoi((char*) reading_parameter->value);

    uint8_t readings_left = readings;

    while (readings_left > 0 && BufferManager.get_availability(buffers->output))
    {
        uint8_t reading = DipSwitch_getValue();

        BufferManager.push(reading, buffers->output);

        readings_left--;
    }

    response->word_size = 0x1;  // 1 Byte
    response->content_type = UNSIGNED_TYPE;
}
