/**
 * @file dipswitch_handles.h
 *
 */
#ifndef APP_SRC_HANDLES_DIPSWITCH_DIPSWITCH_HANDLES_H_
#define APP_SRC_HANDLES_DIPSWITCH_DIPSWITCH_HANDLES_H_

#include <remote-sensor/api.h>
#include <vendor/Dipswitch.h>

/**
 *  Handle for the dipswitch
 *  @see PeripheralReadHandle
 *  @param StreamBuffers* Buffer for the values read.
 *  @param Request*
 *  @param Parameter*[] The parameters of the URI
 *  @param Response*
 */
void
DipSwitch_get_handler(StreamBuffers* buffers, Request* request, Parameter* parameters[], Response* response);

#endif  /* APP_SRC_HANDLES_DIPSWITCH_DIPSWITCH_HANDLES_H_ */
