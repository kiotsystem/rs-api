/**
 * main.c
 *
 *  Created on: 19 May 2017
 */

/* Definitions */
#define BAUD_RATE 9600
/* library imports */
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdint.h>

/* imports */
#include <remote-sensor/api.h>
#include <vendor/IOPinController.h>
#include <vendor/Dipswitch.h>
#include "src/sensors/adc.h"
#include "src/handles/index.h"

/* Variables */
Port port_D;
/* Function declarations */
void
system_init();

int
main(void)
{
    /** Initializes the system */
    system_init();

    uint8_t address[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    size_t address_size = 8;  // bytes
    const uint8_t NUMBER_OF_ATTEMPTS = 0x80;

    /* makes the Protocol a stateful one */
    RegistrationController.register_to(address, address_size, NUMBER_OF_ATTEMPTS);

    while (1)
    {
        CommunicationController.listen(address, address_size);
    }
}



void system_init()
{
    // initializes the ADC reader
    ADC_init();
    // create empty port
    port_D = IOPinController_createEmptyPort();
    // set data direction register
    IOPinController_setDataDirectionRegister(port_D, &DDRD);
    // set data register
    IOPinController_setDataRegister(port_D, &PORTD);
    // set inputs pin register
    IOPinController_setInputPinsRegister(port_D, &PIND);
    /* DIP Switch */
    // init dipswitch
    DipSwitch_init(port_D, 3, 4, 5);

    // init resource manager
    uint8_t buffer_size = 0x06;
    uint8_t params_size = 0x05;
    size_t stream_request_list_size = 5;
    RequestAcceptType accept_requests = ACCEPT_STREAM;

    CommunicationController.init(accept_requests, stream_request_list_size, params_size, buffer_size);

    /* Switch Resource registration */
    // create a resource action
    Resource* read_switch = ResourceManager.create("read", IS_RESETTABLE,
            SINGLE_BUFFER, RESOURCE_MANAGER_DEFAULT);
    PeripheralReadHandle read_handle = &DipSwitch_get_handler;
    ResourceManager.attach_read_handle(read_handle, read_switch);
    PeripheralCallHandle call_handle = &DipSwitch_get_handler;
    ResourceManager.attach_call_handle(call_handle, read_switch);
    Resource* read_clone = ResourceManager.clone(read_switch, SHARED_BUFFERS);

    // register a resource under the uri: '/switch/' without any resource (this URI wont be registered)
    URIComponent* switch_uri = ResourceManager.create_identifier("switch");

    // register a resource under switch: '/switch/dip_switch', but this will be registered as there's resources attached
    URIComponent* dip_switch = ResourceManager.create_identifier("dip_switch");
    ResourceManager.register_resource(read_switch, dip_switch);
    ResourceManager.register_component(dip_switch, switch_uri);

    // register a resource under switch: '/switch/cloned_switch'
    URIComponent* fake_switch = ResourceManager.create_identifier("cloned_switch");
    ResourceManager.register_resource(read_clone, fake_switch);
    ResourceManager.register_component(fake_switch, switch_uri);
    
    // register the URIComponent under root
    ResourceManager.register_component(switch_uri, ResourceManager.get_root_component());

    /* Temperature resource registration */
    Resource* temperature_resource = ResourceManager.create("read", IS_RESETTABLE,
            SINGLE_BUFFER, RESOURCE_MANAGER_DEFAULT);
    PeripheralReadHandle temp_read_handle = &Temperature_get_handler;
    ResourceManager.attach_read_handle(temp_read_handle, temperature_resource);
    PeripheralCallHandle temp_call_handle = &Temperature_get_handler;
    ResourceManager.attach_call_handle(temp_call_handle, temperature_resource);

    // register the temperature resource under the uri: '/temperature/' with default action 'read'
    URIComponent* temperature_uri = ResourceManager.create_identifier("temperature");
    ResourceManager.register_resource(temperature_resource, temperature_uri);

    ResourceManager.register_component(temperature_uri, ResourceManager.get_root_component());

    /* Light resource registration */
    Resource* light_resource = ResourceManager.create("read", IS_RESETTABLE,
            SINGLE_BUFFER, RESOURCE_MANAGER_DEFAULT);
    PeripheralReadHandle light_read_handle = &Light_get_handler;
    ResourceManager.attach_read_handle(light_read_handle, light_resource);
    PeripheralCallHandle light_call_handle = &Light_get_handler;
    ResourceManager.attach_call_handle(light_call_handle, light_resource);

    // register the light resource under the uri: '/light/' with default action 'read'
    URIComponent* light_uri = ResourceManager.create_identifier("light");
    ResourceManager.register_resource(light_resource, light_uri);

    ResourceManager.register_component(light_uri, ResourceManager.get_root_component());
}
