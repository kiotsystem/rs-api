#ifndef REMOTE_SENSOR_API_H_
#define REMOTE_SENSOR_API_H_
/**
 * @file api.h
 * Remote Sensor API header.
 *
 * As the Remote Sensor project is Stateless by design, the registration protocol is not required, but offered.
 * In order to have the registration protocol included define the REMOTE_SENSOR_REGISTRATION Symbol (macro) and the API will included automatically.
 *
 */
#ifdef REMOTE_SENSOR_REGISTRATION
#include <remote-sensor/communication/registration.h>
#endif
#include <remote-sensor/communication/controller.h>

#endif /* REMOTE_SENSOR_API_H_ */
