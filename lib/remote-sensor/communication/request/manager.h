/**
 * @file request_manager
 * This has all the requests api
 */
#ifndef REQUEST_REQUEST_MANAGER_H_
#define REQUEST_REQUEST_MANAGER_H_

#include <stdlib.h>
#include <string.h>

#include <remote-sensor/communication/models.h>
#include "list.h"

char* QUERY;
URI URI_POINTER;
uint8_t TRANSACTION_ID;

/**
 * \typedef RequestDeepDestroy
 * Determines if RequestManager.destroy will keep or free the URI memory.
 * @see RequestManager.destroy
 */
typedef enum RequestDeepDestroy
{
    DESTROY_URI,
    KEEP_URI
} RequestDeepDestroy;

/**
 * \typedef RequestAcceptType
 * Determines if the Node can or not accept Streams
 * @see RequestList
 */
typedef enum RequestAcceptType
{
    ACCEPT_ONE_TIME,
    ACCEPT_STREAM
} RequestAcceptType;

/**
 * \struct RequestManager
 * The RequestManager API is used to be able to extract exact information from the request, independent from the Request Type.
 */
struct RequestManager
{
    /**
     * Initializes the RequestManager depending on the acceptance type.
     * By hierarchy, accepting `ONE_TIME` will only accept one time and will not initialize the request list for the Streams,
     * If stream types are accepted then it will initialize the list with the maximum number of Request.
     * @see RequestList.init
     * @param RequestAcceptType
     * @param size_t size of the list
     */
    void (*init)(RequestAcceptType, size_t);
    /**
     * Creates an empty request, if NULL is passed then the URI is set to NULL, if a URI is passed (pointer to array of chars) the request is only linked to the URI
     * but no duplication in memory is made, the request instance returned itself is malloced, therefore it has to be destroyed.
     * @see RequestManager.destroy
     * @param URI or NULL
     * @param uin8t_t the URI size (not counting the "\0" character)
     * @return Request*
     */
    Request* (*create)(URI, uint8_t);
    /**
     * This functions calls malloc to instantiate the request to be returned that will be used for a Registration request,
     * this request has no URI attached (NULL), therefore no malloc was called for the URI.
     * @return Request*
     */
    Request* (*create_registration)();
    /**
     * Returns the current transaction id, without any effects on the transaction id counter.
     * @return uint8_t
     */
    uint8_t (*get_current_transaction)();
    /**
     * Calls the get_current_transaction, then increments accordingly the transaction id counter for the next request.
     * @return uint8_t
     */
    uint8_t (*get_transaction)();
    /**
     * Assigns the next valid transaction id to the passed Request, then returns it.
     * @return Request*
     */
    Request* (*assign_transaction)(Request*);
    /**
     * Returns a path from the Request, separating it from the query, and duplicates it.
     *\brief Allocates memory for the copy
     * @param Request*
     * @return char* the path string
     */
    char* (*get_path)(Request*);
    /**
     * Returns a Parameter from the Request URI
     * Call this multiple times for looping over multiple query parameters, it saves an URI pointer internally, once the Request reference changes then the
     * looping restarts from beginning, pass NULL to restart it.
     * \brief will allocate memory for the parameter use RequestManager.destroy to free it.
     * @param Request* the Request to get the parameter from or NULL
     * @return Parameter*
     * @see RequestManager.destroy
     */
    Parameter* (*get_query_parameter)(Request*);
    /**
     * Extracts the query parameters from an URI (passed in the request) and assigns the parameters in the Parameter array. Assigns a NULL pointer to the last Parameter in the array to facilitate iteration.
     * Returns the number of parameters found in the Request's URI.
     * @see RequestManager.count_parameters
     * @param Request*
     * @param Parameter*[] An array of all Parameters found + 1 (automatically assigned) NULL at the end.
     * @param uint8_t array size number of query parameters passed
     * @return uint8_t parameter count
     */
    uint8_t (*get_query_parameters)(Request*, volatile Parameter*[], uint8_t);
    /**
     * Searches in a list of parameters the parameter that has "action" as key, if not found then returns NULL.
     * @param Parameter*[] a parameter list
     * @return Parameter* || NULL a parameter with key "action"
     */
    Parameter* (*get_action)(Parameter*[]);
    /**
     * Searches the given list of parameters for the frequency ("freq") keyword and returns the value of the frequency parameter, if it was not present then returns 0x01, which is the default value.
     * The value is converted from ASCII to a number by the c function atoi
     * @param Parameter[] a parameter list
     * @return uint8_t The frequency found or 0x01 (default).
     */
    uint8_t (*get_frequency)(Parameter*[]);
    /**
     * Searches the passed list of parameters and searches for the Parameter that matches the key passed.
     * Will return a pointer to the first matched Parameter.
     * This function does not call malloc, it instead searches for actual parameters in the list.
     * @param char* a string with the  key to be searched
     * @param Parameter*[] a list of parameters
     * @return Parameter* || NULL
     */
    Parameter* (*search_by_key)(char*, Parameter*[]);
    /**
     * Returns the number of Parameters found in the Request's uri.
     * @param Request*
     * @return uint8_t the number of parameters found
     */
    uint8_t (*count_parameters)(Request*);
    /**
     * Cleans the Parameter passed, frees the key and value but not the Parameter instance.
     * @param Parameter*
     */
    void (*clean)(Parameter*);
    /**
     * Resets the Parameter pointer array passed by freeing the allocated memory and setting NULL to all freed elements, therefore the outcome of this call will be the list of Parameter* having only NULL.
     * Do not pass a Parameter* array that contains Parameters in stack memory!
     * @param Parameter*[]
     * @param uint8_t array_size
     */
    void (*reset)(volatile Parameter*[], uint8_t);
    /**
     * Destroys a Request created by the RequestManager, frees only the memory of the request instance itself.
     * @param Request* The request to destroy.
     * @param RequestDeepDestroy DESTROY_URI or KEEP_URI which will determine if the uri is kept or freed
     * @see RequestDeepDestroy
     */
    void (*destroy)(Request*, RequestDeepDestroy);
    /**
     * Destroys and frees the memory from the Parameter, this will call free on
     * the attributes and then the struct itself.
     * @param Parameter
     */
    void (*destroy_parameter)(Parameter*);
};

extern struct RequestManager RequestManager;

#endif /* REQUEST_REQUEST_MANAGER_H_ */
