#include "manager.h"

/* Global variables */
char* QUERY;
uint8_t TRANSACTION_ID = 0;
URI URI_POINTER;
/* Static variables */
static uint8_t COUNTER = 0;

/**
 * Initializes the RequestManager depending on the acceptance type.
 * By hierarchy, accepting `ONE_TIME` will only accept one time and will not initialize the request list for the Streams,
 * If stream types are accepted then it will initialize the list with the maximum number of Request.
 * @see RequestList.init
 * @param RequestAcceptType
 * @param size_t size of the list
 */
void
RequestManager_init(RequestAcceptType acceptance_type, size_t list_size)
{
    if (acceptance_type == ACCEPT_STREAM)
    {
        RequestList.init(list_size);
    }
}

/**
 * Returns the current transaction id, without incrementing it
 * @return uin8t_t
 */
uint8_t
RequestManager_get_current_transaction()
{
    return TRANSACTION_ID;
}

/**
 * Returns the next available transaction id, this affects the transaction id counter.
 * @return uint8_t ID
 */   
uint8_t
RequestManager_get_transaction()
{
    uint8_t id = RequestManager.get_current_transaction();

    if (TRANSACTION_ID == 0xFF)
    {
        TRANSACTION_ID = 0x00;
    } else {
        TRANSACTION_ID++;
    }

    return id;
}

/**
 * Assigns the transaction to the request and then returns the passed request
 * @param Request*
 * @return Request*
 */
Request*
RequestManager_assign_transaction(Request* request)
{
    request->transaction_id = RequestManager.get_transaction();
    return request;
}

/**
 * Creates an empty request, if NULL is passed then the URI is set to NULL, if a URI is passed (pointer to array of chars) the request is only linked to the URI
 * but no duplication in memory is made, the request instance returned itself is malloced, therefore it has to be destroyed.
 * @see RequestManager.destroy
 * @param URI
 * @param uin8t_t the URI size (not counting the "\0" character)
 * @return Request*
 */
Request*
RequestManager_create(URI uri, uint8_t uri_size)
{
    Request* request = malloc(sizeof(Request));
    request = RequestManager.assign_transaction(request);
    request->uri = uri;
    request->uri_length = uri_size;
    return request;
}

/**
 * This functions calls malloc to instantiate the request to be returned,
 * this request has no URI attached (NULL), therefore no malloc was called for the URI.
 * @return Request*
 * @see RequestManager.create
 * @see RequestManager.create_registration
 */
Request*
RequestManager_create_registration()
{
    Request* request = RequestManager.create(NULL, 0x0);

    request->type = REGISTRATION;
    request->connection_type = ONE_TIME;

    return request;
}

/**
 * Returns a path from the Request.
 * Duplicates the path from the request, this is helpful when the URI contains some query, e.g. the GET parameters.
 *\brief Allocates memory for the copy
 * @param Request*
 * @return char*
 * @see RequestManager
 */
char*
RequestManager_get_path(Request* request)
{
    if (request == NULL || request->uri == NULL)
    {
        return NULL;
    }

    char* uri = strdup((char*) request->uri);
    if (uri == NULL)
    {
        return NULL;  // failed to allocate memory
    }
    uri = strtok(uri, "?");
    return uri;
}

/**
 * Loops over the list of parameters until the NULL pointer is reached
 * returns the parameter that matches the key
 * @param char* key
 * @param Parameter*[]
 * @return Parameter*
 */
Parameter*
RequestManager_loop_for_key(char* key, Parameter* parameters[])
{
    uint8_t count = 0;
    Parameter* current = parameters[count];

    while(current != NULL && strcmp((char*) current->key, key) != 0)
    {
        count++;
        current = parameters[count];
    }

    return current;
}

/**
 * Searches for the action in a list of parameters and returns the one that has action as key.
 * @param Parameter[] a parameter list
 * @return Parameter* a parameter with key "action"
 */
Parameter*
RequestManager_get_action(Parameter* parameters[])
{
    Parameter* result = RequestManager_loop_for_key("action", parameters);
    return result;
}

/**
 * Searches the given list of parameters for the frequency (freq) keyword and returns the value of the frequency parameter, if it was not present then returns 0x01, which is the default value.
 * The value is converted from ASCII to a number by the c function atoi
 * @param Parameter[] a parameter list
 * @return uint8_t The frequency found or 0x01 (default).
 */
uint8_t
RequestManager_get_frequency(Parameter* parameters[])
{
    Parameter* result = RequestManager_loop_for_key("freq", parameters);

    if (result == NULL)
    {
    	return 0x01;
    }

    return (uint8_t) atoi((char*) result->value);
}

/**
 * Searches the passed list of parameters and searches for the Parameter that matches the key passed.
 * Will return a pointer to the first matched Parameter.
 * @param char* key  a string with the  key to be searched
 * @param Parameter*[] a list of parameters
 * @return Parameter* || NULL
 */
Parameter*
RequestManager_search_by_key(char* key, Parameter* parameters[])
{
    Parameter* result = RequestManager_loop_for_key(key, parameters);
    return result;
}

/**
 * Returns the number of Parameters found in the Request's uri
 * @see RequestManager
 * @param Request
 * @return uint8_t number of parameters
 */
uint8_t
RequestManager_count_parameters(Request* request)
{
    uint8_t count = 0;
    uint8_t* path = request->uri;

    if (strstr((char*) path, "?") != NULL)
    {
        for(count; path[count]; path[count] == '&' ? count++ : *path++);
        count = count + 1;  // count the first parameter
    }

    return count;
}

/**
 * Returns the first occurrence of a query parameter.
 * Call this multiple times for looping over the query parameters, once the Request reference changes then the
 * looping restarts from beginning, pass NULL to restart it.
 * \brief Will allocate memory for parameter use RequestManager.destroy to free it.
 * @param Request*
 * @return Parameter*
 * @see RequestManager.destroy
 * @see RequestManager
 */
Parameter*
RequestManager_get_query_parameter(Request *request)
{
    if (request == NULL)  // reset the active URL being used to tokenize
    {
        free(QUERY);
        URI_POINTER = NULL;
        QUERY = NULL;
        COUNTER = 0;

        return NULL;
    }

    Parameter* parameter = malloc(sizeof(Parameter));

    if (QUERY == NULL || URI_POINTER != request->uri)
    {
        // reset the url if the QUERY is NULL or the original request url pointer is different (the request changed)
        URI_POINTER = request->uri;
        COUNTER = 0;
        size_t size = strlen(request->uri);
        char* holder = malloc(size + 1);
        strcpy(holder, request->uri);

        QUERY = holder;
        QUERY = strtok(QUERY, "?");
        QUERY = strtok(NULL, "?");  // get the query
        char* temp = strdup(QUERY);
        QUERY = temp;
        free(holder);
    }

    uint8_t count = 0;
    char* copy = strdup(QUERY);
    char* raw_parameter = copy;
    raw_parameter = strtok(raw_parameter, "&");

    while(count < COUNTER && raw_parameter != NULL)
    {
        raw_parameter = strtok(NULL, "&");
        count++;
    }
    COUNTER++;

    if (raw_parameter == NULL)
    {
        free(copy);
        parameter->key = NULL;
        parameter->value = NULL;
        return parameter;
    }

    parameter->key = (int8_t*) strtok(raw_parameter, "=");
    parameter->value = (int8_t*) strtok(NULL, "=");

    parameter->key = (int8_t*) strdup(parameter->key);
    parameter->value = (int8_t*) strdup(parameter->value);

    free(copy);

    return parameter;
}

/**
 * Extracts the query parameters from a URI (passed in the request) and assigns the parameters in the Parameter array. Assigns a NULL pointer to the last Parameter in the array.
 * Returns the number of parameters found in the Request's URI.
 * @see RequestManager.get_query_parameters
 * @param Request*
 * @param Parameter*[]
 * @param uint8_t array_size
 * @return the number of parameters in the request
 */
uint8_t
RequestManager_get_query_parameters(Request* request, volatile Parameter* parameters[], uint8_t array_size)
{
    uint8_t parameter_count = RequestManager.count_parameters(request);

    if (parameter_count > 0)
    {
        uint8_t count;
        uint8_t array_limit = array_size - 1;
        RequestManager.get_query_parameter(NULL);  // reset the state of the function
        for (count = 0; count < parameter_count && count < array_limit; count++)
        {
            parameters[count] = RequestManager.get_query_parameter(request);
        }
        parameters[count] = NULL;  // assign NULL as the terminating parameter
    }
    
    return parameter_count;
}

/**
 * Resets the Parameter pointer array passed by freeing the allocated memory and setting NULL to all freed elements, therefore the outcome of this call will be the list of Parameter* having only NULL.
 * Do not pass a Parameter* array that contains Parameters in stack memory!
 * @param Parameter* parameters[]
 * @param uint8_t array_size
 */
void
RequestManager_reset(volatile Parameter* parameters[], uint8_t array_size)
{
    for (uint8_t count = 0; count < array_size; count++)
    {
        RequestManager.destroy_parameter(parameters[count]);
        parameters[count] = NULL;
    }
}

/**
 * Cleans the Parameter to be reused later
 * @param Parameter* parameter
 */
void
RequestManager_clean(Parameter* parameter)
{
    if (parameter != NULL)
    {
        free(parameter->key);
        parameter->key = NULL;
        free(parameter->value);
        parameter->value = NULL;
    }
}

/**
 * Destroys the Request according to the create function.
 * @param Request* The request to destroy.
 * @param RequestDeepDestroy DESTROY_URI or KEEP_URI  which will determine if the uri is kept or freed
 */
void
RequestManager_destroy(Request* request, RequestDeepDestroy flag)
{
    if (request == NULL)
    {
        return;
    }

    if (flag == DESTROY_URI && request->uri != NULL)
    {
        free(request->uri);
        request->uri = NULL;
    }

    free(request);
    request = NULL;
}

/**
 * Destroys and frees the memory from the Parameter, this will call free on
 * the attributes and then the struct itself.
 * @param Parameter*
 */
void
RequestManager_destroy_parameter(Parameter* parameter)
{
    if (parameter == NULL)
    {
        return;
    }

    if (parameter->key != NULL)
    {
        free(parameter->key);
    }

    if (parameter->value != NULL)
    {
        free(parameter->value);
    }

    free(parameter);
    parameter = NULL;
}

/* RequestManager API */
struct RequestManager RequestManager = {
    .init = RequestManager_init,
    .create = RequestManager_create,
    .create_registration = RequestManager_create_registration,
    .get_current_transaction = RequestManager_get_current_transaction,
    .get_transaction = RequestManager_get_transaction,
    .assign_transaction = RequestManager_assign_transaction,
    .get_path = RequestManager_get_path,
    .get_action = RequestManager_get_action,
	.get_frequency = RequestManager_get_frequency,
    .get_query_parameter = RequestManager_get_query_parameter,
    .get_query_parameters = RequestManager_get_query_parameters,
    .search_by_key = RequestManager_search_by_key,
    .count_parameters = RequestManager_count_parameters,
    .clean = RequestManager_clean,
    .reset = RequestManager_reset,
    .destroy = RequestManager_destroy,
    .destroy_parameter = RequestManager_destroy_parameter
};
