#ifndef REQUEST_LIST_H_
#define REQUEST_LIST_H_

#include <stdlib.h>
#include <remote-sensor/communication/models/list.h>

/**
 * \struct RequestList
 * Exposes the RequestList API to handle Streams.
 * This API works with a list by adding an removing elements as well as iterating through them.
 */
struct RequestList
{
    /**
     * Initializes the RequestList with the maximum amount of elements.
     * @param size_t the maximum number of streams allowed
     */
    void (*init)(size_t);
    /**
     * Returns the used RequestIterator instance that's being used by the RequestList.
     * @return RequestIterator*
     */
    RequestIterator* (*get_iterator)();
    /**
     * Destroys the iterator by freeing up all memory,
     * this will not free the Request's Memory
     */
    void (*destroy)();
    /**
     * Adds the Request to the List relating it to the frequency passed.
     * Returns the reference to the Request added, else will return NULL.
     * @param Request* The request to be added
     * @param uint8_t the desired frequency
     * @return Request* or NULL if not successful
     */
    Request* (*add)(Request*, uint8_t);
    /**
     * Returns the next Request in the List, this will remove the RequestElement from the list and free its memory, will return NULL when no elements are present anymore in the list.
     * If this function is called, it will reset the current element of the iterator to the head.
     * @return Request*
     */
    Request* (*remove)();
    /**
     * Returns the next RequestElement in the queue, this unlinks the element from the list
     * @return RequestElement*
     */
    RequestElement* (*remove_element)();
    /**
     * Returns the Request from the List searching it by the Transaction ID, returns NULL if the element does not exist in the list.
     * This calls free for the element removed.
     * @param TransactionID
     * @return Request*
     */
    Request* (*remove_by_transaction)(TransactionID);
    /**
     * Searches the List for the Request that has the passed TransactionID, if none found will return NULL.
     * @param TransactionID
     * @return RequestElement* || NULL
     */
    RequestElement* (*get_by_transaction)(TransactionID);
    /**
     * Iterates over the list from the head to the end, once surpassing end it will return NULL.
     * @return RequestElement*
     */
    RequestElement* (*iterate)();
    /**
     * Resets the iterator to point back to the head.
     */
    void (*reset_iterator)();
};

extern struct RequestList RequestList;

#endif /* REQUEST_LIST_H_ */
