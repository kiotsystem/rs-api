#include "list.h"

RequestIterator REQUEST_ITERATOR = {
    .count = 0,
    .size = 0,
    .head = NULL,
    .end = NULL,
    .current = NULL,
};

/**
 * Initializes the RequestList with the maximum amount of elements.
 * @param size_t the maximum number of streams allowed
 */
void
RequestList_init(size_t list_size)
{
    RequestIterator* request_iterator = RequestList.get_iterator();

    if (request_iterator == NULL)
    {
        request_iterator = malloc(sizeof(RequestIterator));
    }

    request_iterator->head = NULL;
    request_iterator->current = NULL;
    request_iterator->end = NULL;

    request_iterator->size = list_size;
    request_iterator->count = 0;
}

/**
 * Returns the used RequestIterator instance that's being used by the RequestList.
 * @return RequestIterator*
 */
RequestIterator*
RequestList_get_iterator()
{
    return &REQUEST_ITERATOR;
}

/**
 * Adds the Request to the List relating it to the frequency passed.
 * @param Request* The request to be added
 * @param uint8_t the desired frequency
 * @return Request*  or NULL if not successful
 */
Request*
RequestList_add(Request* request, uint8_t frequency)
{
    RequestIterator* request_iterator = RequestList.get_iterator();

    if (request_iterator->count >= request_iterator->size)
    {
        return NULL;
    }
    
    RequestElement* element = malloc(sizeof(RequestElement));
    element->request = request;
    element->frequency = frequency;
    element->counter = 0;
    element->next = NULL;

    request_iterator->count = request_iterator->count + 1;
    
    if (request_iterator->head == NULL)
    {
        request_iterator->head = element;
    }

    if (request_iterator->end == NULL)
    {
        request_iterator->end = element;
        return request;
    }

    RequestElement* last_element = request_iterator->end;
    last_element->next = element;
    request_iterator->end = element;

    return request;
}

/**
 * Returns the next Request in the List, this will remove the RequestElement from the list and free its memory, will return NULL when no elements are present anymore in the list.
 * If this function is called, it will reset the current element of the iterator to the head.
 * @return Request*
 */
Request*
RequestList_remove()
{
    Request* request = NULL;
    RequestIterator* request_iterator = RequestList.get_iterator();
    request_iterator->current = NULL;

    if (request_iterator->count > 0)
    {
        RequestElement* element = request_iterator->head;
        request_iterator->head = element->next;
        request_iterator->count = request_iterator->count - 1;

        request = element->request;

        if (request_iterator->head == NULL)
        {
            request_iterator->end = NULL;
        }

        free(element);
        element = NULL;
    }

    return request;
}

/**
 * Returns the next RequestElement in the queue, this unlinks the element from the list
 * @return RequestElement*
 */
RequestElement*
RequestList_remove_element()
{
    RequestElement* element = NULL;
    RequestIterator* request_iterator = RequestList.get_iterator();
    request_iterator->current = NULL;

    if (request_iterator->count > 0)
    {
        element = request_iterator->head;
        request_iterator->head = element->next;
        request_iterator->count = request_iterator->count - 1;
    }

    if (request_iterator->head == NULL)
    {
        request_iterator->end = NULL;
    }

    return element;
}

/**
 * Searches the List for the Request that has the passed TransactionID, if none found will return NULL.
 * @param TransactionID
 * @return RequestElement* || NULL
 */
RequestElement*
RequestList_get_by_transaction(TransactionID transaction_id)
{
    RequestElement* element = NULL;
    RequestIterator* request_iterator = RequestList.get_iterator();
    request_iterator->current = NULL;

    if (request_iterator->count > 0)
    {
        RequestElement* current = request_iterator->head;

        while (current != NULL && current->request->transaction_id != transaction_id)
        {
            current = current->next;
        }

        element = current;
    }

    return element;
}

/**
 * Returns the Request from the List searching it by the Transaction ID, returns NULL if the element does not exist in the list.
 * This calls free for the element removed.
 * @param TransactionID
 * @return Request*
 */
Request*
RequestList_remove_by_transaction(TransactionID transaction)
{
    RequestIterator* request_iterator = RequestList.get_iterator();
    if (request_iterator->count <= 0)
    {
        return NULL;
    }

    request_iterator->current = NULL;
    RequestElement* anchor = request_iterator->head;
    RequestElement* matched_element = request_iterator->head;

    while (matched_element != NULL && matched_element->request->transaction_id != transaction)
    {
        anchor = matched_element;
        matched_element = matched_element->next;
    }

    Request* request_to_return = NULL;

    if (matched_element == NULL)
    {
        // no matched element
        return request_to_return;
    }

    request_to_return = matched_element->request;
    
    if (matched_element == request_iterator->head)
    {
        request_iterator->head = matched_element->next;
    } else {
        // the result was somewhere else
        anchor->next = matched_element->next;
    }

    if (matched_element == request_iterator->end)
    {
        // update the end pointer if the one removed is being the end
        request_iterator->end = anchor;

        if (anchor == matched_element)
        {
            request_iterator->end = NULL;
        }
    }

    request_iterator->count = request_iterator->count - 1;
    free(matched_element);
    matched_element = NULL;
    anchor = NULL;

    return request_to_return;
}

/**
 * Iterates over the list from the head to the end, once surpassing end it will return NULL.
 * @return RequestElement*
 */
RequestElement*
RequestList_iterate()
{
    RequestIterator* request_iterator = RequestList.get_iterator();

    if (request_iterator->current == NULL)
    {
        return NULL;
    }

    RequestElement* element = request_iterator->current;
    request_iterator->current = request_iterator->current->next;

    return element;
}

/**
 * Resets the iterator to point back to head.
 */
void
RequestList_reset_iterator()
{
    RequestIterator* request_iterator = RequestList.get_iterator();
    request_iterator->current = request_iterator->head;
}

/**
 * Destroys the iterator by freeing up all memory,
 * this will not free the Request's Memory
 */
void
RequestList_destroy()
{
    RequestIterator* request_iterator = RequestList.get_iterator();
    free(request_iterator);
    request_iterator = NULL;
}

/* Initialize the API */
struct RequestList RequestList = {
    .init = RequestList_init,
    .get_iterator = RequestList_get_iterator,
    .destroy = RequestList_destroy,
    .add = RequestList_add,
    .remove = RequestList_remove,
    .remove_element = RequestList_remove_element,
    .remove_by_transaction = RequestList_remove_by_transaction,
    .get_by_transaction = RequestList_get_by_transaction,
    .iterate = RequestList_iterate,
    .reset_iterator = RequestList_reset_iterator
};
