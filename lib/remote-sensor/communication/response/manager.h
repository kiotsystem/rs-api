/**
 * @file ResponseManager
 */
#ifndef RESPONSE_RESPONSE_MANAGER_H_
#define RESPONSE_RESPONSE_MANAGER_H_

#include <stdlib.h>
#include <remote-sensor/communication/models.h>

/**
 * \typedef ResponseDeepDestroy
 * This enum is used in the destroy call of the api to determine if the data variable is freed or not
 */
typedef enum ResponseDeepDestroy {
    RESPONSE_SHALLOW_DESTROY,
    RESPONSE_DEEP_DESTROY
} ResponseDeepDestroy;

/**
 * \struct ResponseManager
 * Response Manager API
 * This API comprises the tools to create and destroy responses to be used in the communication between devices
 * @see Response
 */
struct ResponseManager
{
    /**
     * Creates a Response and initialises it with default values which should be changed accordingly
     * \brief Calls malloc. Free it using the destroy function provided in this API.
     *@see ResponseManager.destroy
     * @return Response*
     *
     */
    Response* (*create)();
    /**
     * Allocates a Request that represents an Error Response with the given Transaction Id,
     * @see ResponseManager.create
     * @see ResponseManager.destroy
     * @param TransactionID
     * @return Response*
     */
    Response* (*create_error)(TransactionID);
    /**
     * Frees the passed data
     * @see ResponseManager
     * @param uint8_t*
     */
    void (*free_data)(uint8_t*);
    /**
     * Destroys the passed Response by clearing all the required memory.
     * It will take the ResponseDeepDestroy in consideration and free the data variable if RESPONSE_DEEP_DESTROY is passed.
     * @param Response*
     * @param ResponseDeepDestroy RESPONSE_SHALLOW_DESTROY or RESPONSE_DEEP_DESTROY
     */
    void (*destroy)(Response*, ResponseDeepDestroy);
};

extern struct ResponseManager ResponseManager;
#endif /* RESPONSE_RESPONSE_MANAGER_H_ */
