#include "manager.h"

/**
 * Creates a Response and initialises it with default values which should be changed accordingly
 * Calls malloc. Free it using the destroy function provided in this API.
 * @see ResponseManager.destroy
 * @return Response*
 */
Response*
ResponseManager_create()
{
    Response* response = malloc(sizeof(Response));

    response->type = ACK;
    response->transaction_id = 0x00;
    response->content_type = UNSIGNED_TYPE;
    response->word_size = 0x01;
    response->data = NULL;
    response->data_length = 0x00;

    return response;
}

/**
 * Allocates a Response in memory which represents an error with the given Transaction Id
 * @see ResponseManager.create
 * @see ResponseManager.destroy
 * @param TransactionID
 * @return Response*
 */
Response*
ResponseManager_create_error(TransactionID transaction)
{
    Response* response = ResponseManager.create();
    response->type = ERROR_FOUND;
    response->transaction_id = transaction;
    response->content_type = UNSIGNED_TYPE;
    response->word_size = 1;
    response->data_length = 0;
    response->data = NULL;

    return response;
}

/**
 * Frees the passed data
 * @param uint8_t* data
 */
void
ResponseManager_free_data(uint8_t* data)
{
    free(data);
    data = NULL;
}

/**
 * Destroys a Response by clearing all the required memory.
 * It will take the ResponseDeepDestroy in consideration and free the data variable if RESPONSE_DEEP_DESTROY is passed.
 * @param Response* response
 * @param ResponseDeepDestroy RESPONSE_SHALLOW_DESTROY or RESPONSE_DEEP_DESTROY
 */
void
ResponseManager_destroy(Response* response, ResponseDeepDestroy destroy_depth)
{
    if (response == NULL)
    {
        return;
    }

    if (response->data != NULL && destroy_depth == RESPONSE_DEEP_DESTROY)
    {
        free(response->data);
        response->data = NULL;
    }

    free(response);
    response = NULL;
}

/* Initialization */
struct ResponseManager ResponseManager = {
    .create = ResponseManager_create,
    .create_error = ResponseManager_create_error,
    .destroy = ResponseManager_destroy,
    .free_data = ResponseManager_free_data
};
