/**
* @file Communication Models
* Contains every single model used in the package
* There are diagrams and example of possible message to get a better understanding of this Request/Response model
* http://confluence.es.uni-due.de:8090/pages/viewpage.action?pageId=15271438
*/

#ifndef COMMUNICATION_MODELS_H_
#define COMMUNICATION_MODELS_H_

#include <inttypes.h>

#include "models/context/request.h"
#include "models/context/response.h"

/**
 * \typedef TransactionID
 * If you sent a Request and you get an Response you need to know from which Request the Response is Acknowledging.
 * If you send a Request with an TransactionID from 0x01 you get an Response with the TransactionID 0x01.
 */
typedef uint8_t TransactionID;

/**
 * \typedef Length
 * Define the Length of the Pointer Data in uint8_t Bytes.
 */
typedef uint8_t Length;

/**
 * \typedef Path
 * represents a Path string in the form root/parent/.../desired_component
 * It is used to locate an URIComponent
 * @see URIComponent
 */
typedef uint8_t* Path;

/**
 * \typedef Request
 * A request is the first step to establish a connection between two devices
 * Both Nodes and Gateway can create Requests.
 * First its converted to byte[] then send by the CommunicationMiddelware to the desired Node.
 * You can marshal and demarshal a request.
 * @see ConnectionType
 * @see RequestType
 */
typedef struct Request
{
    RequestType type;/**< determines the type of the request: GET/POST */
    TransactionID transaction_id;/**<The id of the transaction*/
    ConnectionType connection_type;/**< The desired type of Connection: STREAM||ONE_TIME*/
    Length uri_length;/**< the size of the uri in uint8_t bytes*/
    URI uri;/**< the requested URI*/
} Request;

/**
 * \typedef Response
 * A response is created as an answer to a request.
 * Node and Gateway can create Responses.
 * First its converted to byte[] then sent by the CommunicationMiddelware to the excepted Node.
 * You can marshall and demarshall a Response.
 * @see ContentType
 * @see ResponseType
 * @see WordSize
 */
typedef struct Response
{
    ResponseType type;/**< the type of the response. Acknowledging it or sending an error*/
    TransactionID transaction_id;/**<The id of the transaction*/
    ContentType content_type;/**<the encoding of the data. If it is unsigned signed etc*/
    WordSize word_size;/**<Number of uint8_t bytes for 1 data*/
    Length data_length;/**< Data length as multiples of the word size*/
    Data data;/**< the data itself*/
} Response;

#endif /* COMMUNICATION_MODELS_H_ */
