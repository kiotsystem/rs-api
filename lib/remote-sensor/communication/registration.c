#include "registration.h"

/**
 * The ResourceManager.get_path_for calls malloc therefore freeing the memory of the URI is advised.
 * @see ResourceManager.get_path_for_component
 * @param URIComponent* component
 * @return Request*
 */
Request*
RegistrationController_get_registration_request(URIComponent* component)
{
    Request* registration_request = RequestManager.create_registration();
    registration_request->uri = (URI) ResourceManager.get_path_for_component(component);
    Length uri_length = strlen((char*) registration_request->uri);
    registration_request->uri_length = uri_length;

    return registration_request;
}

/**
 * The ResourceManager.get_path_for calls malloc therefore freeing the memory of the URI is advised
 * @see ResourceManager.get_path_for_component
 * @param URIComponent* component
 * @return Request*
 */
Request*
RegistrationController_get_registration_request_for_resource(Resource* resource)
{
    Request* registration_request = RequestManager.create_registration();
    registration_request->uri = (URI) ResourceManager.get_path_for_resource(resource);

    Length uri_length = strlen((char*) registration_request->uri);
    registration_request->uri_length = uri_length;

    return registration_request;
}

/**
 * Recursively registers the URIComponent in the gateway.
 * Once the request is sent it waits for an ACK response.
 * @param URIComponent* component
 * @param uint8_t address[]
 * @param size_t address_size
 * @return uint8_t Returns the number of registered components.
 */
uint8_t
RegistrationController_register_component_to(URIComponent* component, uint8_t address[], size_t address_size,
        uint8_t attempts_before_fail)
{
    if (component == NULL)
    {
        return 0;
    } else {
        uint8_t counts = 0x0;
        Response* response = NULL;

        while (counts == 0x0)
        {
            Request* request = RegistrationController.get_registration_request(component);
            CommunicationController.send_request(request, address, address_size);
            response = CommunicationController.receive_response(attempts_before_fail);

            if(response != NULL)
            {
                if (request->transaction_id == response->transaction_id)
                {
                    counts = 0x1;
                }
                ResponseManager.destroy(response, RESPONSE_DEEP_DESTROY);
            }
            RequestManager.destroy(request, DESTROY_URI);
        }

        counts += RegistrationController.register_component_to(component->sibling, address, address_size, attempts_before_fail);
        counts += RegistrationController.register_component_to(component->child, address, address_size, attempts_before_fail);

        return counts;
    }
}

/**
 * Registers all resources for a given component
 * @param URIComponent* component
 * @param uint8_t[] address
 * @param size_t size of the address in bytes
 * @param uint8_t number of attempts before failing
 * @return uint8_t count of registrations
 */
uint8_t
RegistrationController_register_resources_from_component(URIComponent* component, bool last_component, uint8_t address[], size_t address_size, uint8_t attempts_before_fail)
{
    Resource* current = component->resources;
    Resource* end = ResourceNavigator.get_last_resource_from(component);
    uint8_t registration_count = 0;
    CtrlRecovery.ok();

    while (current != NULL)
    {
        Request* request = RegistrationController.get_registration_request_for_resource(current);
        
        if (last_component && current == end)
        {
            request->type = END_REGISTRATION;
        }
        
        CommunicationController.send_request(request, address, address_size);
        Response* response = CommunicationController.receive_response(attempts_before_fail);

        if (response->transaction_id == request->transaction_id && response->type == ACK)
        {
            registration_count++;
            current = current->sibling;
            CtrlRecovery.ok();
        }

        ResponseManager.destroy(response, RESPONSE_DEEP_DESTROY);
        RequestManager.destroy(request, DESTROY_URI);
    }

    return registration_count;
}

/**
 * Iteratively registers the Resource in the gateway. Uses Morris Traversal.
 * Once the request is sent it waits for an ACK response.
 * Reference: http://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion-and-without-stack/
 *
 * @param uint8_t the address
 * @param size_t the size of the address in bytes
 * @param uint8_t attempts before fail
 * @return uint8_t count of registrations
 */
uint8_t
RegistrationController_register_resources(uint8_t address[], size_t address_size,
        uint8_t attempts_before_fail)
{
    URIComponent* traverser = ResourceManager.get_root_component();

    if (traverser == NULL || traverser->child == NULL)
    {
        return 0;
    }

    traverser = traverser->child;
    uint8_t count = 0;
    URIComponent* predecessor_node;
    URIComponent* end_node = ResourceNavigator.get_last_component(traverser);

    while (traverser != NULL)
    {
        if (traverser->child == NULL)
        {
            bool last_node = (end_node == traverser);
            count +=  RegistrationController.register_resources_from_component(traverser, last_node, address, address_size, attempts_before_fail);
            traverser = traverser->sibling;
        } else {
            predecessor_node = traverser->child;

            while (predecessor_node->sibling != NULL && predecessor_node->sibling != traverser)
            {
                predecessor_node = predecessor_node->sibling;
            }

            if (predecessor_node->sibling == NULL)
            {
                predecessor_node->sibling = traverser;
                traverser = traverser->child;
            } else {
                predecessor_node->sibling = NULL;
                bool last_node = (end_node == traverser);
                count += RegistrationController.register_resources_from_component(traverser, last_node, address, address_size, attempts_before_fail);
                traverser = traverser->sibling;
            }
        }
    }

    return count;
}

/**
 * Registers the same level components from the passed component, sibling components.
 * @param URIComponent*
 * @param uint8_t address[]
 * @param size_t address_size
 * @param uint8_t attempts_before_fail
 * @return uint8_t returns the number of components returned.
 */
uint8_t
RegistrationController_register_component_level_to(URIComponent* component, uint8_t address[], size_t address_size,
        uint8_t attempts_before_fail)
{
    URIComponent* current = component;
    uint8_t registration_count = 0;
    Response* response = NULL;
    Request* request = RegistrationController.get_registration_request(current);

    while (current != NULL)
    {
        CommunicationController.send_request(request, address, address_size);
        response = CommunicationController.receive_response(attempts_before_fail);

        if(response != NULL)
        {
            if (response->transaction_id == request->transaction_id)
            {
                registration_count++;
                current = current->sibling;
            }
            ResponseManager.destroy(response, RESPONSE_DEEP_DESTROY);
        }
    }
    RequestManager.destroy(request, DESTROY_URI);

    return registration_count;
}

/**
 * Sends a Registration Request per resource to the passed address, waits for the ACK response, and returns a proper state, depending on the Response received.
 * If requests are received during this communication they will be dropped, only the relevant packages will be taken in consideration.
 * This process uses a Watchdog to ensure the registration.
 * @param uint8_t[] address to register to
 * @param size_t size of the address in bytes
 * @param uint8_t number of attempts before returning or 0 to retry infinitely
 * @return uint8_t the number of resources registered
 * @see CtrlRecovery
 */
uint8_t
RegistrationController_register_to(uint8_t address[], size_t address_size, uint8_t attempts_before_fail)
{
	CtrlRecovery.cancel();
	CtrlRecovery.init(T2S);
    uint8_t registration_count = 0;
    registration_count = RegistrationController.register_resources(address, address_size, attempts_before_fail);
    CtrlRecovery.ok();

    return registration_count;
}

/* Registration initialization */
struct RegistrationController RegistrationController = {
    .register_to = RegistrationController_register_to,
    .register_component_to = RegistrationController_register_component_to,
    .register_component_level_to = RegistrationController_register_component_level_to,
    .register_resources_from_component = RegistrationController_register_resources_from_component,
    .register_resources = RegistrationController_register_resources,
    .get_registration_request = RegistrationController_get_registration_request,
    .get_registration_request_for_resource = RegistrationController_get_registration_request_for_resource
};

