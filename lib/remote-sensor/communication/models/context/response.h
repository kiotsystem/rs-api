/**
 * @file Response Model
 */
#ifndef COMMUNICATION_MODELS_RESPONSE_H_
#define COMMUNICATION_MODELS_RESPONSE_H_

#include <inttypes.h>

/**
 * \typedef Data
 * This is the data received from the handle and is sent to the gateway as an Response.
 * @see Buffer
 */
typedef uint8_t* Data;

/**
 * \typedef WordSize.
 * The word size that it is included in the response, the unit of this size represents uint8_t bytes.
 */
typedef uint8_t WordSize;

/**
 * \typedef ResponseType
 * Defines the different possible response types from a request.
 */
typedef enum
ResponseType
{
    ACK = 0x05,/**<The Request was successful.*/
    NOT_FOUND = 0x06,/**<The Resource was not found. URI is wrong.*/
    SEND = 0x07,/**<For the response from the stream*/
    RESOURCE_DELETED = 0x08,/**<if a resource deletion was successful*/
    ERROR_FOUND = 0x09/**<the request resulted in an error*/
} ResponseType;

/**
 * \typedef ContentType
 * Values set in the Response header regarding the content type of the response.
 */
typedef enum
ContentType
{
    UNSIGNED_TYPE = 0x00,
    SIGNED_TYPE = 0x01,
    CHAR_TYPE = 0x02,
    FLOAT_TYPE = 0x03
} ContentType;

#endif /* COMMUNICATION_MODELS_RESPONSE_H_ */
