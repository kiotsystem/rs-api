/**
 * @file Request Model
 */
#ifndef COMMUNICATION_MODELS_REQUEST_H_
#define COMMUNICATION_MODELS_REQUEST_H_

#include <inttypes.h>

/**
 * \typedef URI
 * URI type definition represented by ASCII characters, worth mentioning that this must end in an '\0' character representing the end of the string, which then will aid for the calculation of the length of the URI.
 * When an income request having a URI is received, it will only take the length of the URI that the request says it contains.
 * The URI has the form path?query
 * @see Path
 * @see Parameter
 */
typedef uint8_t* URI; //Pointer to an Array which contains the URI

/**
 * \typedef RequestType
 * Defines the different kinds of request that can be made.
 */
typedef enum
RequestType
{
    GET = 0x01,/**<Retrieves the information corresponding to the resource in request URI, information is requested by the gateway.*/
    POST = 0x02,/**<Requests processing of information in the response, the resource manager should create the data but not send it.*/
    DELETE = 0x03,/**<Requests to delete the resource identified by URI.*/
    REGISTRATION = 0x04,/**<To register the resource at the gateway. First thing to do from the node.*/
    END_REGISTRATION = 0x00/**<Signals the gateway that all resources have been registered for this node*/
} RequestType;

/**
 * \typedef ConnectionType
 * There are two different kind of requests. First is to get/set data once. Known as ONE_TIME
 * The other is to get/set many data all the time. You start to get/set data at the time when the node get STREAM and the Node stops
 * when it gets another request with CLOSE.
 */
typedef enum
ConnectionType
{
    ONE_TIME = 0x01,
    STREAM = 0x02,
    CLOSE = 0x03,
} ConnectionType;

/**
 * \typedef Parameter
 * Represents in a key-value structure the parameters from the query of the URL passed in a Request. The query has the form in the URI key1=value1&key2=value2
 */
typedef struct
Parameter
{
    int8_t* key;
    int8_t* value;
} Parameter;

#endif /* COMMUNICATION_MODELS_REQUEST_H_ */
