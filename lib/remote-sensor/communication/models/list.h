/**
 * @file List Models
 */
#ifndef COMMUNICATION_MODELS_LIST_H_
#define COMMUNICATION_MODELS_LIST_H_

#include <inttypes.h>

#include <remote-sensor/communication/models.h>

/**
 * \typedef RequestElement
 * RequestElement represents a FIFO Linked List of requests.
 */
typedef struct
RequestElement
{
    Request* request; /**< a pointer to a saved request*/
    uint8_t frequency; /**<value which the counter will compare to*/
    uint8_t counter; /**< Individual up counter that will be updated by the timer*/
    struct RequestElement* next; /**< Next element in the link */
} RequestElement;

/**
 * \typedef RequestIterator
 * This structure is used to traverse through the RequestElement linked list
 */
typedef struct
RequestIterator
{
    RequestElement* head; /**< First element of the list*/
    RequestElement* end; /**< Last element of the list*/
    RequestElement* current; /**< Current RequestElement pointing at */
    size_t size; /**< Maximum Size of the list*/
    uint8_t count;/**< Current amount of elements in the list*/
} RequestIterator;

#endif /* COMMUNICATION_MODELS_LIST_H_ */
