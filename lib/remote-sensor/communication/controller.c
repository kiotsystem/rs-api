#include "controller.h"

/* Global variables */
const uint8_t DEFAULT_MAX_PARAMS = 6;
uint8_t* LISTENING_ADDRESS = NULL;
size_t LISTENING_ADDRESS_SIZE = 0;
RequestAcceptType REQUEST_ACCEPTANCE_LEVEL = ACCEPT_ONE_TIME;

/**
 * @link http://www.engblaze.com/microcontroller-tutorial-avr-and-arduino-timer-interrupts/
 */
void CommunicationController_set_interrupt()
{
#ifndef LOCAL_TESTS
    cli();

    // initialize Timer 1
    TCCR1A = 0x00;
    TCCR1B = 0x00;
    // set compare match register to desired timer count:
    OCR1A = 0x3D08;
    // turn on CTC mode:
    TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler:
    TCCR1B |= (1 << CS10);
    TCCR1B |= (1 << CS12);
    // enable timer compare interrupt:
    TIMSK1 |= (1 << OCIE1A);

    sei();
#endif
}

/**
 * This initializes the communication controller by initializing the ResourceManager,RequestManager, Watchdog timer and Middleware.
 * \brief calls ResourceManager.set_defaults so the same conditions apply.
 *
 * @param RequestAcceptType determines if streams are to be accepted or not
 * @param size_t request stream list size the maximum allowed number of concurrent streams
 * @param uint8_t params list size the maximum allowed number of parameters for one request
 * @param uint8_t buffer size the default buffer size for streams
 * @see ResourceManager.set_defaults
 * @see RequestManager.init
 */
void
CommunicationController_init(RequestAcceptType acceptance_type, size_t list_size, uint8_t max_params, uint8_t buffer_size)
{
    MAX_PARAMS = (max_params == 0) ? DEFAULT_MAX_PARAMS : max_params;
    REQUEST_ACCEPTANCE_LEVEL = acceptance_type;

    ResourceManager.set_defaults(buffer_size, NULL);  // set the default root
    RequestManager.init(acceptance_type, list_size);
    CtrlRecovery.init(T1S);
    // initialize the CommunicationMiddelware
    CommunicationMiddlewareAPI_init();

    if (acceptance_type == ACCEPT_STREAM)
    {
        CommunicationController_set_interrupt();
    }
}

/**
 * Cleans up after the request and handles the POS action after the request.
 * @param Parameter* parameters[]
 * @param uint8_t param_count
 * @param Request* request
 * @param Path path
 */
void
CommunicationController_pos_request(volatile Parameter* parameters[], uint8_t param_count, Request* request, Path path)
{
    // free the parameters
    RequestManager.reset(parameters, param_count);
    // free the Path
    if(path != NULL)
    {
        free(path);
        path = NULL;
    }

    if (request->connection_type == ONE_TIME)
    {
        // free the request
        RequestManager.destroy(request, DESTROY_URI);
        request = NULL;
    }
}

/**
 * This call extracts the resource being requested in the request, calls the `ResourceManager` for the requesting `URIComponent`, and depending on the `Request`'s parameters, invokes the `Resource`'s respective handle.
 * Important to note that the available handles are only the GET and POST, translating to `read_handle` and `write_handle` from the Handles.
 * When a DELETE method is being issued, this will unregister the given `Resource` from the URIComponent;
 * If a request type is STREAM, then it registers it into the RequestList and returns the proper response, ERROR_FOUND in case it couldn't add it to the List.
 * If a Request with connection type STREAM is being handled this will handle the registration of the Request for streaming data,
 * as well when a connection type CLOSE comes this will delete the streaming Request.
 * @see CommunicationControllerHandles
 * @see ResourceManager
 * @see Resource
 * @see Handles
 * @param Request* request
 * @return Response*
 */
Response*
CommunicationController_handle_request(Request* request)
{
    Response* response = NULL;

    if (request == NULL)
    {
        return response;
    }

    Path path = (Path) RequestManager.get_path(request);
    URIComponent* component = ResourceManager.search((char*) path);

    volatile Parameter* parameters[MAX_PARAMS];
    // automatically inserts the NULL pointer to the array
    uint8_t parameter_count = RequestManager.get_query_parameters(request, parameters, MAX_PARAMS);

    switch (request->connection_type)
    {
        case ONE_TIME:
            response = CommunicationControllerHandles.action(component, request, parameters);
            break;
        case STREAM:
            response = CommunicationControllerHandles.stream(component, request, parameters);
            break;
        case CLOSE:
            response = CommunicationControllerHandles.close(component, request, parameters);
            break;
    }

    // Handle the freeing of memory on allocated elements
    CommunicationController_pos_request(parameters, parameter_count, request, path);

    return response;
}

/**
 *This call handles a Request, creates a Response from the resulting data and returns it.
 * This function is meant to be called only when dealing with STREAM connections, as it only handles the call to the Stream and not the registration or ONE_TIME connection type requests.
 * @see CommunicationControllerHandles
 * @see RequestList
 * @see ResourceManager
 * @see Resource
 * @see Handles
 * @param Request* request
 * @return Response*
 */
Response*
CommunicationController_handle_call(Request* request)
{
    Response* response = NULL;

    if (request == NULL)
    {
        return response;
    }

    Path path = (Path) RequestManager.get_path(request);
    URIComponent* component = ResourceManager.search((char*) path);

    volatile Parameter* parameters[MAX_PARAMS];
    // automatically inserts the NULL pointer to the array
    uint8_t parameter_count = RequestManager.get_query_parameters(request, parameters, MAX_PARAMS);

    response = CommunicationControllerHandles.call(component, request, parameters);

    // Handle the freeing of memory on allocated elements
    CommunicationController_pos_request(parameters, parameter_count, request, path);

    return response;
}

/**
 * Handles an incoming request from the network.
 * This call in contrast to `handle_request` gets a buffer of `uint8_t`, Marshals it into a request then calls the `handle_request`.
 * When a valid response is returned by the `handle_request` then this function calls the Response marshaler to demarshal it, which results in a buffer of `uint8_t`.
 * @see CommunicationController
 * @see ResponseMarshaler.demarshal
 * @see MarshalerManager
 * @param uint8_t*  serial request data received from the network which will be processed
 * @param size_t* response_size the size of the uint8_t buffer allocated for the response
 * @return uint8_t* A demarshalled response ready to be sent through the network
 */
uint8_t*
CommunicationController_handle(uint8_t* data, size_t* response_size)
{
    CtrlRecovery.ok();

    Request* request = RequestMarshaler.marshal(data);
    // generate a response based on the Request
    Response* response = CommunicationController.handle_request(request);
    // demarshal the response
    uint8_t* raw_response = ResponseMarshaler.demarshal(response, response_size);
    // destroy the response object
    ResponseManager.destroy(response, RESPONSE_SHALLOW_DESTROY);

    CtrlRecovery.ok();

    return raw_response;
}

/**
 * This call enters in an infinite loop in which is listening to incoming requests, processing them and sending a Response.
 * This call is recommended to be executed after, and only after `register`.
 * @param uint8_t address
 * @param size_t size of address
 */
void
CommunicationController_listen(uint8_t address[], size_t address_size)
{
    LISTENING_ADDRESS = address;
    LISTENING_ADDRESS_SIZE = address_size;
    while(1)
    {
        CtrlRecovery.ok();
        if(CommunicationMiddlewareAPI.receivedNewFrame())
        {
            // got frame
            uint8_t request_size = 0;
            request_size = CommunicationMiddlewareAPI.getReceivedPayloadSize();
            uint8_t request_data[request_size];
            CommunicationMiddlewareAPI.getReceivedPayload(request_data);
            if (request_size > 0)
            {
                uint8_t* response_data = NULL;
                size_t response_size = 0;
                response_data = CommunicationController.handle(request_data, &response_size);

                if (response_data != NULL)
                {
                    CommunicationController.send(response_data, response_size, address, address_size);
                    free(response_data);
                } else {
                    // manually get the transaction
                    TransactionID transaction = request_data[RESPONSE_TRANSACTION_OFFSET];
                    Response* error_response = ResponseManager.create_error(transaction);
                    CommunicationController.send_response(error_response, address, address_size);
                    ResponseManager.destroy(error_response, RESPONSE_DEEP_DESTROY);
                }
            }
        }
    }
}

/**
 * This will send, using the CommunicationMiddlewareAPI, the request.
 * @param Request* request to send
 * @param uint8_t[] address
 * @param size_t address size in bytes
 */
void
CommunicationController_send_request(Request* request, uint8_t address[], size_t address_size)
{
    size_t request_data_size = 0;
    uint8_t* request_data = RequestMarshaler.demarshal(request, &request_data_size);
    CommunicationController.send(request_data, request_data_size, address, address_size);
    free(request_data);
}

/**
  * This will send, using the CommunicationMiddlewareAPI, the response.
 * @param Response*
 * @param uint8_t address[]
 * @param size_t address_size in bytes
 */
void
CommunicationController_send_response(Response* response, uint8_t address[], size_t address_size)
{
    size_t response_data_size = 0;
    uint8_t* response_data = ResponseMarshaler.demarshal(response, &response_data_size);
    CommunicationController.send(response_data, response_data_size, address, address_size);
    free(response_data);
    response_data = NULL;
}

/**
 * Enters in a blocking loop waiting for a frame to arrive, when it arrives it returns the Response.
 * If a request is received instead it is dropped.
 * @param uint8_t attempts before returning NULL
 * @return Response*
 */
Response*
CommunicationController_receive_response(uint8_t attempts_before_fail)
{
    Response* response = NULL;
    uint8_t attempts = 0;
    int loop_forever = (attempts_before_fail == 0);

    while (attempts < attempts_before_fail || loop_forever)
    {
        // block until response arrives
        if (CommunicationMiddlewareAPI.receivedNewFrame())
        {
            uint8_t response_size = 0;
            response_size = CommunicationMiddlewareAPI.getReceivedPayloadSize();
            uint8_t response_data[response_size];
            CommunicationMiddlewareAPI.getReceivedPayload(response_data);
            response = ResponseMarshaler.marshal(response_data);
            (response == NULL) ? attempts++ : attempts;

            return response;
        }
    }

    return NULL;
}

/**
 * Sends data.
 * @param uint8_t* data to send
 * @param uint8_t data size
 * @param uint8_t[] address
 * @param size_t address size in bytes
 */
void
CommunicationController_send(uint8_t* data, uint8_t data_size, uint8_t address[], size_t address_size)
{
    if (address_size == 8)  // address_size is in bytes
    {
        CommunicationMiddlewareAPI.sendFrameTo64BitAddress(address, data, data_size, 0);
    } else if (address_size == 2) {
        CommunicationMiddlewareAPI.sendFrameTo16BitAddress(address, data, data_size, 0);
    }
}

/**
 * This call handles the interrupt by iterating over the registered STREAM requests. And sending data through the network if the target frequency is reached.
 * This should operate in parallel with the CommunicationController.listen function
 * @see RequestList
 */
void
CommunicationController_handle_interrupt()
{
    RequestList.reset_iterator();
    RequestElement* element = NULL;
    element = RequestList.iterate();

    while (element != NULL)
    {
        element->counter = element->counter + 0x01;

        if (element->frequency == element->counter)
        {
            CtrlRecovery.ok();
            element->counter = 0x00;
            Response* response = CommunicationController.handle_call(element->request);
            CommunicationController.send_response(response, LISTENING_ADDRESS, LISTENING_ADDRESS_SIZE);
            ResponseManager.destroy(response, RESPONSE_SHALLOW_DESTROY);

            CtrlRecovery.ok();
        }

        element = RequestList.iterate();
    }
}

/* Initialization */
struct
CommunicationController CommunicationController = {
    .init = CommunicationController_init,
    .handle_request = CommunicationController_handle_request,
    .handle_interrupt = CommunicationController_handle_interrupt,
    .handle_call = CommunicationController_handle_call,
    .handle = CommunicationController_handle,
    .send_request = CommunicationController_send_request,
    .send_response = CommunicationController_send_response,
    .send = CommunicationController_send,
    .receive_response = CommunicationController_receive_response,
    .listen = CommunicationController_listen
};

#ifndef LOCAL_TESTS
ISR(TIMER1_COMPA_vect)
{
    CommunicationController.handle_interrupt();
}
#endif
