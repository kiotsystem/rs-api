/**
 * @file Registration
 */
#ifndef COMMUNICATION_CONTROLLER_REGISTRATION_H_
#define COMMUNICATION_CONTROLLER_REGISTRATION_H_

#include <stdbool.h>

#include "controller.h"

/**
 * \struct RegistrationController
 * Handles the Registration Protocol by registering all the resources to the gateway
 */
struct RegistrationController
{
    /**
     * Sends a Registration Request per resource to the passed address, waits for the ACK response, and returns a proper state, depending on the Response received.
     * If requests are received during this communication they will be dropped, only the relevant packages will be taken in consideration.
     * This process uses a Watchdog to ensure the registration.
     * @param uint8_t[] address to register to
     * @param size_t size of the address in bytes
     * @param uint8_t number of attempts before returning or 0 to retry infinitely
     * @return uint8_t the number of resources registered
     * @see CtrlRecovery
     */
    uint8_t (*register_to)(uint8_t[], size_t, uint8_t);
    /**
     * Recursively register the components to the Gateway.
     * @param URIComponent* component from which to start the registration from (generally the root)
     * @param uint8_t[] address
     * @param size_t address_size in bytes
     * @param uint8_t number of attempts
     * @return uint8_t Returns the number of registered components.
     */
    uint8_t (*register_component_to)(URIComponent*, uint8_t[], size_t, uint8_t);
    /**
     * Registers the component's siblings in the gateway.
     * @param URIComponent* component to register recursively
     * @param uint8_t[] address
     * @param size_t address_size in bytes
     * @param uint8_t number of attempts
     * @return uint8_t returns the number of components returned.
     */
    uint8_t (*register_component_level_to)(URIComponent*, uint8_t[], size_t, uint8_t);
    /**
     * Registers all resources for a given component, if the boolean flag is passed as true the last resource registered request will be of type END_REGISTRATION.
     * @see RequestType
     * @param URIComponent* component the component to register all resources from
     * @param bool flag that represents that the passed component is the last component to be registered
     * @param uint8_t[] address
     * @param size_t size of the address in bytes
     * @param uint8_t number of attempts before failing
     * @return uint8_t count of registrations
     */
    uint8_t (*register_resources_from_component)(URIComponent*, bool, uint8_t[], size_t , uint8_t);
    /**
     * Iteratively registers the Resource in the gateway. Uses Morris Traversal.
     * Once the request is sent it waits for an ACK response.
     * Reference: http://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion-and-without-stack/
     *
     * @param uint8_t the address
     * @param size_t the size of the address in bytes
     * @param uint8_t attempts before fail
     * @return uint8_t count of registrations
     */
    uint8_t (*register_resources)(uint8_t[], size_t, uint8_t);
    /**
     * Creates a registration Request based on the component's URI.
     * @param URIComponent*
     * @return Request*
     */
    Request* (*get_registration_request)(URIComponent*);
    /**
     * Creates a registration Request based on the resource's URI
     * @param URIComponent*
     * @return Request*
     */
    Request* (*get_registration_request_for_resource)(Resource*);
};

extern struct RegistrationController RegistrationController;

#endif /* COMMUNICATION_CONTROLLER_REGISTRATION_H_ */
