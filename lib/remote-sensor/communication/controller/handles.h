/**
 * @file handles.h
 * Contains the handles for the request that the CommunicationController depends on
 */

#ifndef COMMUNICATION_CONTROLLER_HANDLES_H_
#define COMMUNICATION_CONTROLLER_HANDLES_H_

#include <stdlib.h>
#include <inttypes.h>

#include <remote-sensor/resource/manager.h>
#include <remote-sensor/communication/models.h>
#include <remote-sensor/communication/marshalers.h>
#include <remote-sensor/communication/request/manager.h>
#include <remote-sensor/communication/response/manager.h>

/**
 * \struct CommunicationControllerHandles API
 * This structure handles the requests given by the communication controller it is not supposed to be used without it
 * @see CommunicationController
 */
struct CommunicationControllerHandles
{
    /**
     * Calls the respective Handle from the Resource to generate data. The resource will be located by the action parameter or the first resource of the URIComponent if it is not defined.
     * This is used for a one time communication, not for a stream.
     * @see ResponseManager.create
     * @see Parameter
     * @param URIComponent* The Component from which the action is determined
     * @param Request*  The passed request which will determine the operation realized(e.g. GET/POST). It will be passed to the handles as well.
     * @param Parameter*[] List of parameters which will be passed to the handles
     * @return Response* The resultant response from the Request
     */
    Response* (*action)(URIComponent*, Request*, Parameter*[]);
    /**
     * Calls the respective Handle from the Resource to generate data.
     * This is used for a stream (sending data in regular periods), not for an one time communication.
     * @see Parameter
     * @param URIComponent* The Component from which the action is determined
     * @param Request* The passed request which will determine the operation realized(e.g. GET/POST).  It will be passed to the handles as well.
     * @param Parameter* List of parameters which will be passed to the handles
     * @return Response* The resultant response from the Request
     */
    Response* (*call)(URIComponent*, Request*, Parameter*[]);
    /**
     * Handles the Request of a Stream.
     * @see RequestList
     * @see ResponseManager.destroy
     * @see Parameter
     * @param URIComponent* The Component determined by the Request
     * @param Request* The passed request . It will be passed to the handles.
     * @param Parameter*[] List of parameters which will be passed to the handles
     * @return Response* The resultant response from the Request
     */
    Response* (*stream)(URIComponent*, Request*, Parameter*[]);
    /**
     * Closes the Stream determined by the request Transaction Id.
     * @see Parameter
     * @see ResponseManager.destroy
     * @param URIComponent* The Component determined by the Request
     * @param Request* The passed request .
     * @param Parameter*[] List of parameters which will be passed to the handles
     * @return Response* The resultant response from the Request
     */
    Response* (*close)(URIComponent*, Request*, Parameter*[]);
    /**
     * Deletes the indicated resource or the first resource of the component if one is not specified
     * @param URIComponent* The Component determined by the Request
     * Request* request The passed request
     * @return Response*
     */
    Response* (*delete)(Resource*, Request*, Response*);
};

extern struct CommunicationControllerHandles CommunicationControllerHandles;

#endif /* COMMUNICATION_CONTROLLER_HANDLES_H_ */
