#include "handles.h"

/**
 * Sets the response's transaction_id based on the request's transaction_id.
 * @param Response*
 * @param Request*
 */
static void
relate(Response* response, Request* request)
{
    response->transaction_id = request->transaction_id;
}

/**
 * Decides which resource to use from the URIComponent.
 * @param URIComponent*
 * @param Parameter*[]
 * @return Resource* || NULL
 */
Resource*
CommunicationControllerHandles_get_resource(URIComponent* uri, Parameter* parameters[])
{
    Resource* resource = uri->resources;

    if (parameters != NULL)
    {
        Parameter* action = RequestManager.get_action(parameters);
        if (action != NULL)  // action was explicitly passed
        {
            Resource* searched_action = ResourceLocator.search_in(uri, (uint8_t*) action->key);
            resource = searched_action == NULL ? resource : searched_action;
        }
    }

    return resource;
}

/**
 * Calls the respective Handle from the Resource to generate data.
 * This is used for a one time communication, not for a stream.
 * @param URIComponent* uri
 * Request* request
 * Parameter* parameters[]
 * @return Response*
 */
Response*
CommunicationControllerHandles_action(URIComponent* uri, Request* request, Parameter* parameters[])
{
    Response* response = ResponseManager.create();
    relate(response, request);

    if (uri == NULL)
    {
        response->type = NOT_FOUND;
        return response;
    }

    Resource* resource = CommunicationControllerHandles_get_resource(uri, parameters);

    if (resource == (Resource*) NULL)
    {
        response->type = NOT_FOUND;
        return response;
    }

    switch(request->type)
    {
        case GET:
            response = StreamManager.read(resource->stream, request, parameters, response);
            break;
        case POST:
            response = StreamManager.write(resource->stream, request, parameters, response);
            break;
        case DELETE:
        	response = CommunicationControllerHandles.delete(resource, request, response);
        	break;
        default:
            break;
    }

    return response;
}

/**
 * Calls the respective Handle from the Resource to generate data.
 * This is used for a stream, not for a one time communication.
 * @param URIComponent* uri
 * @param Request* request
 * @param Parameter* parameters[]
 * @return Response*
 */
Response*
CommunicationControllerHandles_call(URIComponent* uri, Request* request, Parameter* parameters[])
{
    Response* response = ResponseManager.create();
    relate(response, request);

    if (uri == NULL)
    {
        response->type = NOT_FOUND;
        return response;
    }

    Resource* resource = CommunicationControllerHandles_get_resource(uri, parameters);

    if (resource == (Resource*) NULL)
    {
        response->type = NOT_FOUND;
        return response;
    }

    switch(request->type)
    {
        case GET:
            response = StreamManager.call(resource->stream, request, parameters, response);
            break;
        case POST:
            response = StreamManager.write(resource->stream, request, parameters, response);
            break;
        default:
            break;
    }

    response->type = SEND;

    return response;
}

/**
 * Handles the Request of a Stream.
 * @see ResponseManager.destroy
 * @param URIComponent*
 * @param Request*
 * @param Parameter*[]
 * @return Response*
 */
Response*
CommunicationControllerHandles_stream(URIComponent* uri, Request* request, Parameter* parameters[])
{
    Response* response = ResponseManager.create();
    relate(response, request);

    if (uri == NULL)
    {
        response->type = NOT_FOUND;
        return response;
    }

    uint8_t frequency = RequestManager.get_frequency(parameters);
    Request* request_added = RequestList.add(request, frequency);

    if (request_added == NULL)
    {
        response->type = ERROR_FOUND;
    }

    return response;
}

/**
 * Closes the streaming of the registered Requests
 * @see ResponseManager.destroy
 * @param URIComponent*
 * @param Request*
 * @param Parameter*[]
 * @return Response*
 */
Response*
CommunicationControllerHandles_close(URIComponent* uri, Request* request, Parameter* parameters[])
{
    Response* response = ResponseManager.create();
    response->type = ERROR_FOUND;
    relate(response, request);

    if (uri == NULL)
    {
        response->type = NOT_FOUND;
        return response;
    }

    Request* stream_request = RequestList.remove_by_transaction(request->transaction_id);

    if (stream_request != NULL)
    {
        response->type = ACK;
        RequestManager.destroy(stream_request, DESTROY_URI);
    }

    return response;
}

/**
 * Deletes the resource or the first resource of the component if one is not specified
 * @param URIComponent* uri
 * Request* request
 * Parameter* parameters[]
 * @return Response*
 */
Response*
CommunicationControllerHandles_delete(Resource* resource, Request* request, Response* response)
{
	ResourceManagerState state = ResourceManager.delete(resource);

    if (state != RESOURCE_MANAGER_SUCCESS)
    {
    	response->type = NOT_FOUND;
    	return response;
    }

    response->type = RESOURCE_DELETED;

    return response;
}

/* Initialisation of the CommunicationControllerHandles */
struct CommunicationControllerHandles CommunicationControllerHandles = {
    .action = CommunicationControllerHandles_action,
    .call = CommunicationControllerHandles_call,
    .stream = CommunicationControllerHandles_stream,
    .close = CommunicationControllerHandles_close,
	.delete = CommunicationControllerHandles_delete
};
