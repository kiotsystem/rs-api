#ifndef COMMUNICATION_CONTROLLER_WATCHDOG_H_
#define COMMUNICATION_CONTROLLER_WATCHDOG_H_

#ifndef LOCAL_TESTS

#include <avr/wdt.h>
#include <avr/interrupt.h>

#endif

/**
 * \typedef TimeInterval
 */
typedef enum
TimeInterval {
    T120MS = 3,
    T250MS = 4,
    T500MS = 5,
    T1S = 6,
    T2S = 7
} TimeInterval;

/**
 * \struct CommunicationControllerRecovery
 * This is a wrapper for the Watchdog
 */
struct CommunicationControllerRecovery
{
    /**
     * Initialises the Watchdog.
     * This implementation only allows 120ms and above implementations, in the parameter it means above 2 (max. available in 328p and 2650 MCU's is 7 -> 2 seconds).
     * @see WDTO_120MS
     * @see WDTO_250MS
     * @see WDTO_500MS
     * @see WDTO_1S
     * @see WDTO_2S
     * @param TimeInterval determines the time interval for the Watchdog
     */
    void (*init)(TimeInterval);
    /**
     * Taps the Watchdog. If not tapped with the set frequency the whole program will be reset.
     */
    void (*ok)();
    /**
     * Cancels the Watchdog.
     */
    void (*cancel)();
};

extern struct CommunicationControllerRecovery CtrlRecovery;

#endif /* COMMUNICATION_CONTROLLER_WATCHDOG_H_ */
