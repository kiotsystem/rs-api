#ifndef LOCAL_TESTS
#include <remote-sensor/communication/controller/watchdog.h>

/**
* Initialises the Watchdog.
* This implementation only allows 120ms and above implementations, in the parameter it means above 2 (max. available in 328p and 2650 MCU's is 7 -> 2 seconds).
* @see WDTO_120MS
* @see WDTO_250MS
* @see WDTO_500MS
* @see WDTO_1S
* @see WDTO_2S
* @param TimeInterval determines the time interval for the Watchdog
*/
void
CommunicationControllerRecovery_init(TimeInterval interval)
{
    if (interval < 2)
    {
        interval = WDTO_1S;
    }
    
    cli();
    wdt_enable(interval);
    sei();
}

/**
 * Taps the Watchdog. If not tapped with the set frequency the whole program will be reset.
 */
void
CommunicationControllerRecovery_ok()
{
    cli();
    wdt_reset();
    sei();
}

/**
 * Cancels the Watchdog.
 */
void
CommunicationControllerRecovery_cancel()
{
    cli();
    wdt_disable();
    sei();
}

struct CommunicationControllerRecovery CtrlRecovery = {
    .init = CommunicationControllerRecovery_init,
    .ok = CommunicationControllerRecovery_ok,
    .cancel = CommunicationControllerRecovery_cancel
};

#endif
