/**
 * @file marshalers
 * This header file includes the RequestMarshaler and ResponseMarshalers, include this to include both marshalers.
 * Else include the Marshalers Independently
 * @see ResponseMarshaler
 * @see RequestMarshaler
 */

#ifndef COMMUNCATION_MARSHALER_MANAGER_H_
#define COMMUNCATION_MARSHALER_MANAGER_H_

#include <inttypes.h>
#include "models.h"

#include "marshalers/request.h"
#include "marshalers/response.h"

#endif /* COMMUNCATION_MARSHALER_MANAGER_H_ */
