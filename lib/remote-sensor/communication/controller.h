/** @file Communication
 * Contains everything related to the message layer communication,
 * and interaction to the resource manager from incomming messages.
 */

#ifndef COMMUNICATION_CONTROLLER_H_
#define COMMUNICATION_CONTROLLER_H_

#include <stdlib.h>
#include <inttypes.h>

#ifndef LOCAL_TESTS

#include <CommunicationMiddleware.h>
#include <remote-sensor/communication/controller/watchdog.h>

#else
/* If you need to unit test please define the macro LOCAL_TESTS before the if statement */
/* Simple `#define LOCAL_TESTS` will do the trick */
#include <communication/dummy/CommunicationMiddlewareAPI.h>
#include <communication/dummy/watchdog.h>

#endif

#include <remote-sensor/communication/controller/handles.h>

uint8_t MAX_PARAMS;

/**
 * \struct CommunicationController
 *
 * Communication Controller to handle all message layer communication
 * and interaction with the resources, doing so through the ResourceManager.
 * This API is dependent on the CommunicationMiddleware API!
 */
struct CommunicationController
{
    /**
     * This initialises the communication controller by initialising the ResourceManager,RequestManager, Watchdog timer and Middleware.
     * \brief calls ResourceManager.set_defaults so the same conditions apply.
     *
     * @param RequestAcceptType determines if streams are to be accepted or not
     * @param size_t list size the maximum allowed number of concurrent streams
     * @param uint8_t params list size the maximum allowed number of parameters for one request
     * @param uint8_t buffer size the default buffer size for streams
     * @see ResourceManager.set_defaults
     * @see RequestManager.init
     * @see CtrlRecovery
     */
    void (*init)(RequestAcceptType, size_t, uint8_t, uint8_t);
    /**
     * Handles an incoming request from the network.
     * This call in contrast to `handle_request` gets a buffer of `uint8_t`, Marshals it into a request then calls the `handle_request`.
     * When a valid response is returned by the `handle_request` then this function calls the Response marshaler to demarshal it, which results in a buffer of `uint8_t`.
     * @see CommunicationController
     * @see ResponseMarshaler.demarshal
     * @param uint8_t* serial request data received from the network which will be processed
     * @param size_t* response_size the size of the uint8_t buffer allocated for the response
     * @return uint8_t* A demarshalled response ready to be sent through the network
     */
    uint8_t* (*handle)(uint8_t*, size_t*);
    /**
     * This call extracts the resource being requested in the request, calls the `ResourceManager` for the requesting `URIComponent`, and depending on the `Request`'s parameters, invokes the `Resource`'s respective handle.
     * Important to note that the available handles are only the GET and POST, translating to `read_handle` and `write_handle` from the Handles.
     * When a DELETE method is being issued, this will unregister the given `Resource` from the URIComponent;
     * If a request type is STREAM, then it registers it into the RequestList and returns the proper response, ERROR_FOUND in case it couldn't add it to the List.
     * If a Request with connection type STREAM is being handled this will handle the registration of the Request for streaming data,
     * as well when a connection type CLOSE comes this will delete the streaming Request.
     *
     * @see RequestList
     * @see CommunicationControllerHandles
     * @see ResourceManager
     * @see Resource
     * @see Handles
     * @param Request*
     * @return Response*
     */
    Response* (*handle_request)(Request*);
    /**
     * This call handles the interrupt by iterating over the registered STREAM requests. And sending data through the network if the target frequency is reached.
     * This should operate in parallel with the CommunicationController.listen function
     * @see RequestList
     */
    void (*handle_interrupt)();
    /**
     * This call handles a Request, creates a Response from the resulting data and returns it.
     * This function is meant to be called only when dealing with STREAM connections, as it only handles the call to the Stream and not the registration or ONE_TIME connection type requests.
     * @see CommunicationControllerHandles
     * @see RequestList
     * @see ResourceManager
     * @see Resource
     * @see Handles
     * @param Request* request
     * @return Response*
     */
    Response* (*handle_call)(Request*);
    /**
     * This call enters in an infinite loop in which is listening to incoming requests, processing them and sending a Response.
     * This call is recommended to be executed after, and only after `register`.
     * @see CommunicationController.register_to
     * @param uint8_t[] address
     * @param size_t size of address in bytes
     */
    void (*listen)(uint8_t[], size_t);
    /**
     * Enters in a blocking loop waiting for a frame to arrive, when it arrives it returns the Response.
     * If a request is received instead it is dropped.
     * @param uint8_t attempts before returning NULL
     * @return Response*
     */
    Response* (*receive_response)(uint8_t);
    /**
     * This will send, using the CommunicationMiddlewareAPI, the request.
     * @param Request* to send
     * @param uint8_t[] address
     * @param size_t address size in bytes
     */
    void (*send_request)(Request*, uint8_t[], size_t);
    /**
     * This will send, using the CommunicationMiddlewareAPI, the response.
     * @param Response* to send
     * @param uint8_t[] address to send
     * @param size_t size of the address in bytes
     */
    void (*send_response)(Response*, uint8_t[], size_t);
    /**
     * This will send, using the CommunicationMiddlewareAPI, the data.
     * @param uint8_t* data
     * @param uint8_t data size
     * @param uint8_t[] address
     * @param size_t address size in bytes
     */
    void (*send)(uint8_t*, uint8_t, uint8_t[], size_t);
};

extern struct CommunicationController CommunicationController;

#endif /* COMMUNICATION_H_ */
