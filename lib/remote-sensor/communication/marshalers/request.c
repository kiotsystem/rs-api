#include "request.h"

/**
* Instantiates a request and transforms the linearised data given into a structure.
* This function calls malloc on the returned Request instance and in the URI a calloc is done.
* @see RequestManager.destroy
* @param uint8_t* linear data received from the CommunicationMiddleware
* @return Request* request the resulting request
*/
Request*
RequestMarshaler_marshal(uint8_t* data)
{
    // REGISTRATION is the greatest value
    if (*(data + REQUEST_TYPE_OFFSET) > REGISTRATION)
    {
        return NULL;
    }

    Request* request = malloc(sizeof(Request));

    request->type = *data;  // first byte is the request type
    request->transaction_id = *(data + REQUEST_TRANSACTION_OFFSET); // second byte in the request
    request->connection_type = *(data + REQUEST_CONNECTION_OFFSET);  // third byte in the request
    request->uri_length = *(data + REQUEST_URI_LENGTH_OFFSET);  // fourth byte in the request

    if (request->uri_length == 0x0)
    {
        request->uri = NULL;
    } else {
        uint8_t* uri_buffer = calloc(request->uri_length + 1, sizeof(uint8_t));  // include null character
        request->uri = uri_buffer;
        memcpy(uri_buffer, (data + REQUEST_URI_OFFSET), request->uri_length);
    }

    return request;
}

/**
 * Returns a byte array based on the Request instance passed
 * This function calls malloc on the returned Request data instance
 * @param Request* the request to be demarshalled
 * @param size_t* request_size size of uint8_t data to be allocated
 * @return uint8_t* array based on the Request instance passed
 */
uint8_t*
RequestMarshaler_demarshal(Request* request, size_t* request_size)
{
    if (request == NULL)
    {
        *request_size = 0;
        return NULL;
    }

    size_t size_calculated = REQUEST_URI_OFFSET + request->uri_length;
    *request_size = size_calculated;
    uint8_t* request_data = malloc(sizeof(uint8_t) * size_calculated);

    *(request_data) = request->type;
    *(request_data + REQUEST_TRANSACTION_OFFSET) = request->transaction_id;
    *(request_data + REQUEST_CONNECTION_OFFSET) = request->connection_type;
    *(request_data + REQUEST_URI_LENGTH_OFFSET) = request->uri_length;

    if (request->uri_length > 0)
    {
        uint8_t* data_reference = request_data + REQUEST_URI_OFFSET;
        memcpy(data_reference, request->uri, request->uri_length);
    }

    return request_data;
}

struct
RequestMarshaler RequestMarshaler = {
    .marshal = RequestMarshaler_marshal,
    .demarshal = RequestMarshaler_demarshal
};
