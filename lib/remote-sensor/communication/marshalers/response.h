#ifndef MARSHALERS_RESPONSE_H_
#define MARSHALERS_RESPONSE_H_

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include <remote-sensor/communication/models.h>

/**
 * \typedef RESPONSE_OFFSET
 * Represents the offset of the fields in a byte array received in a network frame as determined by the protocol
 */
typedef enum RESPONSE_OFFSET {
    RESPONSE_TYPE_OFFSET = 0x0,
    RESPONSE_TRANSACTION_OFFSET = 0x1,
    RESPONSE_WORD_SIZE_OFFSET = 0x2,
    RESPONSE_CONTENT_TYPE_OFFSET = 0x3,
    RESPONSE_DATA_LENGTH_OFFSET = 0x4,
    RESPONSE_DATA_OFFSET = 0x5,
} RESPONSE_OFFSET;

/**
 * \struct ResponseMarshaler
 */
struct ResponseMarshaler
{
    /**
    * Instantiates a response and transforms the linearised data given into a structure.
    * This function calls malloc on the returned Response instance and in the URI a calloc is done.
    * @see ResponseManager.destroy
    * @param uint8_t* linear data array to be Marshalled
    * @return Response* the resulting response
    */
	Response* (*marshal)(uint8_t*);
    /**
     * Demarshalls the passed response in a linear buffer, this function calls malloc, therefore free the memory once the
     * data is not being used. The length of the buffer allocated will be set to the passed size_t variable.
     * @param Response* The response to be demarshalled
     * @param size_t* size of the buffer allocated
     * @return uint8_t* response_data a pointer to a linear byte array
     */
    uint8_t* (*demarshal)(Response*, size_t*);
};

extern struct ResponseMarshaler ResponseMarshaler;
#endif /* MARSHALERS_RESPONSE_H_ */
