#include "response.h"

/**
 * Instantiates a response and returns it based on the data received
 * @param uint8_t* linear data array to be Marshalled
 * @return Response* the resulting response
 */
Response*
ResponseMarshaler_marshal(uint8_t* data)
{
    if (*(data + RESPONSE_TYPE_OFFSET) < ACK)
    {
        return NULL;
    }

    Response* response = malloc(sizeof(Response));
    response->type = *(data);
    response->transaction_id = *(data + RESPONSE_TRANSACTION_OFFSET);
    response->word_size = *(data + RESPONSE_WORD_SIZE_OFFSET);
    response->content_type = *(data + RESPONSE_CONTENT_TYPE_OFFSET);
    response->data_length = *(data + RESPONSE_DATA_LENGTH_OFFSET);

    uint8_t* data_reference = data + RESPONSE_DATA_OFFSET;
    uint8_t* payload_copy = malloc(sizeof(uint8_t) * response->data_length);
    memcpy(payload_copy, data_reference, response->data_length);

    response->data = payload_copy;

    return response;
}

/**
 * Demarshalls the passed response in a linear buffer, this function calls malloc, therefore free the memory once the
 * data is not being used. The length of the buffer allocated will be set to the passed size_t variable.
 * @param Response* response
 * @param size_t* response_size size of uint8_t data to be allocated
 * @return uint8_t* response_data a pointer to a linear byte array
 */
uint8_t*
ResponseMarshaler_demarshal(Response* response, size_t* response_size)
{
    if (response == NULL)
    {
        *response_size = 0;
        return NULL;
    }

    *response_size = RESPONSE_DATA_OFFSET + response->data_length;
    uint8_t* response_data = malloc(sizeof(uint8_t) * (*response_size));

    *(response_data) = (uint8_t) response->type;
    *(response_data + RESPONSE_TRANSACTION_OFFSET) = (uint8_t) response->transaction_id;
    *(response_data + RESPONSE_WORD_SIZE_OFFSET) = (uint8_t) response->word_size;
    *(response_data + RESPONSE_CONTENT_TYPE_OFFSET) = (uint8_t) response->content_type;
    *(response_data + RESPONSE_DATA_LENGTH_OFFSET) = (uint8_t) response->data_length;

    uint8_t* data_reference = response_data + RESPONSE_DATA_OFFSET;
    memcpy(data_reference, response->data, response->data_length);

    return response_data;
}

struct
ResponseMarshaler ResponseMarshaler = {
    .marshal = ResponseMarshaler_marshal,
    .demarshal = ResponseMarshaler_demarshal
};
