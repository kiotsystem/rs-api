#ifndef MARSHALERS_REQUEST_H_
#define MARSHALERS_REQUEST_H_

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <remote-sensor/communication/models.h>

/**
 * \enum REQUEST_OFFSET
 * Represents the offset of the fields in a byte array received in a network frame as determined by the protocol
 */
typedef enum REQUEST_OFFSET {
    REQUEST_TYPE_OFFSET = 0x0,
    REQUEST_TRANSACTION_OFFSET = 0x1,
    REQUEST_CONNECTION_OFFSET = 0x2,
    REQUEST_URI_LENGTH_OFFSET = 0x3,
    REQUEST_URI_OFFSET = 0x4
} REQUEST_OFFSET;

/**
 * \struct RequestMarshaler
 * This function will take care of the marshaling and demarshaling operations from the request
 */
struct RequestMarshaler
{
     /**
     * Instantiates a request and transforms the linearised data given into a structure.
     * This function calls malloc on the returned Request instance and in the URI a calloc is done.
     * @see RequestManager.destroy
     * @param uint8_t* linear data array to be marshalled
     * @return Request* the resulting request
     */
	Request* (*marshal)(uint8_t*);
    /**
     * Demarshalls the passed Request,transforming it into linear data, when passing a pointer to the size_t parameter it will assign the value of the length of the
     * malloced data buffer.
     * @param Request* the request to be demarshalled
     * @param size_t* size of uint8_t data to be allocated
     * @return uint8_t* array based on the Request instance passed
     */
    uint8_t* (*demarshal)(Request*, size_t*);
};

extern struct RequestMarshaler RequestMarshaler;
#endif /* MARSHALERS_REQUEST_H_ */
