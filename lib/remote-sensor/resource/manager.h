/** @file ResourceManager
 *
 * manager.h
 */
#ifndef RESOURCE_MANAGER_H_
#define RESOURCE_MANAGER_H_


#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

#include "models.h"
#include "stream/manager.h"
#include "locator.h"
#include "navigator.h"

URIComponent* resource_manager_root;
uint8_t resource_manager_buffer_size;
ResourceManagerState resource_manager_state;

/**
 * \struct ResourceManager
 * This module implements the functions necessary to create a working URIComponent and Resource tree:
 * It contains the functions to create, register both component and resources, as well as to search them in the tree.
 * To create and register a Resource, first we need to create and register a URIComponent, therefore making it uniquely identifiable.
 * The ResourceManager Protocol has to be able to do this is:
 * 1. Create a URIComponent
 * 2. Register the URIComponent in the ResourceManager, with the desired hierarchy, use the search feature to locale a URIComponent in the tree.
 * 3. Create a Resource.
 * 4. Initialize the resource to have all the Handles needed, the name attached to it is used to specifically call this resource (as a URIComponent may have more than one Resource), else by default will call the first occurrence of the Resource.
 * 5. Register the Resource under the desired URIComponent.
 * @see ResourceLocator
 */
struct ResourceManager
{
    /**
     * Sets the defaults for the resource manager.
     * If a URIComponent is given then it uses that one as root, else creates a new URIComponent.
     * Beware of setting the defaults after initialisation, specially the root node!
     * \brief Allocates memory to the root node unless it was previously done.
     *
     * @param uint8_t default_buffer_size
     * @param Resource* to use as root Component
     */
    void (*set_defaults)(uint8_t, URIComponent*);
    /**
     * @return URIComponent* pointing to root
     */
    URIComponent* (*get_root_component)();
    /**
	 * Creates a resource.
     * If the buffer_size is 0x00 then it will assign the defaults.
     * \brief Allocates memory to the resource and to the stream
     *
     * @param char* The resource name. It will be used to uniquely identify the resource.
     * @param ResettableFlag If the stream of the resource is resettable.
     * @param SingleFlag If the stream of the resource has one or two buffers.
     * @param uint8_t Size of the buffer(s) of the stream of the resource. If 0 (`RESOURCE_MANAGER_DEFAULT`) then it will assign the value set when setting the defaults
     * @return Resource* The newly created resource or NULL if there was an error.
     * @see ResourceManager.delete
     * @see StreamManager.create
     */
    Resource* (*create)(char*, ResettableFlag, SingleFlag, uint8_t);
    /**
     * Creates a copy from the passed Resource. The level of the isolation is passed to the StreamManager.
     * Take in consideration the SHARED_BUFFERS when sharing the same buffer among several resources.
     * The returned Resource has the same parent component than the one passed but the sibling resource is set to NULL!
     * \brief When destroying please consider if the buffers are isolated or shared among resources!
     * @see StreamManager.clone
     * @see IsolatedBuffersFlag
     * @param Resource* Pointer to the resource to clone.
     * @param IsolatedBuffersFlag Set if the buffers should be shared or isolated.
     * @return Resource* Pointer to the newly cloned resource.
     */
    Resource* (*clone)(Resource*, IsolatedBuffersFlag);
    /**
     * Creates an URIComponent with the given name.
     * \brief Allocates memory for an URIcomponent
     *
     * @param char* The name of the new URIComponent.
     * @return URIComponent* The new URIComponent.
     * @see ResourceManager.delete_component
     */
    URIComponent* (*create_identifier)(char*);
    /**
     * The ResourceManager will register the passed Resource under the given URIComponent.
     * Every resource is only supposed to be registered to one component. If more are needed use the clone function.
     * @see ResourceManager.clone
     * @param Resource* Resource to register.
     * @param URIComponent* parent Resource from which the resource to register will be registered.
     * @return ResourceManagerState RESOURCE_MANAGER_SUCCESS or RESOURCE_MANAGER_ERROR
     */
    ResourceManagerState (*register_resource)(Resource*, URIComponent*);
    /**
     * Registers the passed component under (if given) the URIComponent.
     * If no parent URIComponent is given then root node becomes the parent node.
     * If the URI already exists at the parent's children level it will return a `RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS` to avoid duplicates, else will return `RESOURCE_MANAGER_SUCCESS`.
     * In the case `RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS` the `URIComponent` is not registered.
     * @param URIComponent* component to register
     * @param URIComponent* parent component (or NULL)
     * @return ResourceManagerState
     */
    ResourceManagerState (*register_component)(volatile URIComponent*, volatile URIComponent*);
    /**
     * Will unregister the resource from the tree but not delete it
     * @param Resource* Resource to be unregistered.
     * @return ResourceManagerState RESOURCE_MANAGER_SUCCESS or RESOURCE_MANAGER_RESOURCE_NOT_FOUND
     */
    ResourceManagerState (*unregister)(Resource*);
    /**
     * Unregisters the passed component from the tree.
     * @param URIComponent* component to register
     * @return ResourceManagerState
     */
    ResourceManagerState (*unregister_component)(URIComponent*);
    /**
     * Will delete the given Resource.
     * @param Resource*
     * @return ResourceManagerState
     */
    ResourceManagerState (*delete)(Resource*);
    /**
     * Will delete the given component.
     * @param URIComponent* Pointer to Resource to delete.
     * @return ResourceManagerState RESOURCE_MANAGER_SUCCESS or RESOURCE_MANAGER_RESOURCE_NOT_FOUND
     */
    ResourceManagerState (*delete_component)(URIComponent*);
    /**
     * Attaches the handles to the Resource, this is a shortcut for the individual attachments.
     * Will return a `RESOURCE_MANAGER_SUCCESS` else a `RESOURCE_MANAGER_ERROR`.
     * \brief If uninitialized in the given resource, will allocate memory for the handles.
     *
     * @param Resource*
     * @param PeripheralCallHandle
     * @param PeripheralReadHandle
     * @param PeripheralWriteHandle
     * @return ResourceManagerState `RESOURCE_MANAGER_SUCCESS` or `RESOURCE_MANAGER_ERROR`
     * @see PeripheralCallHandle
     * @see PeripheralReadHandle
     * @see PeripheralWriteHandle
     */
    ResourceManagerState (*attach_handles)(Resource*, PeripheralCallHandle, PeripheralReadHandle, PeripheralWriteHandle);
    /**
     * Individual attachment of a PeripheralCall Handle.
<<<<<<< HEAD
     * \brief If  uninitialised in the given resource, will allocate memory for the handles.
     * @see PeripheralCallHandle
=======
     * Will return a `RESOURCE_MANAGER_SUCCESS` else a `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`.
     * \brief If  uninitialized in the given resource, will allocate memory for the handles.
     *
>>>>>>> refs/remotes/origin/develop
     * @param PeripheralCallHandle
     * @param Resource* to which it will be attached to
     * @return ResourceManagerState `RESOURCE_MANAGER_SUCCESS`, `RESOURCE_MANAGER_RESOURCE_NOT_FOUND` or `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`
     */
    ResourceManagerState (*attach_call_handle)(PeripheralCallHandle, Resource*);
    /**
     * Individual handle attachment of a PeripheralRead Handle.
     * Will return a `RESOURCE_MANAGER_SUCCESS` else a `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`.
     * \brief If uninitialized in the given resource, will allocate memory for the handles.
     * @param PeripheralReadHandle
     * @param Resource*
     * @return ResourceManagerState `RESOURCE_MANAGER_SUCCESS`, `RESOURCE_MANAGER_RESOURCE_NOT_FOUND` or `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`
     */
    ResourceManagerState (*attach_read_handle)(PeripheralReadHandle, Resource*);
    /**
     * Individual Handle attachment of a PeripheralWrite Handle.
     * Will return a `RESOURCE_MANAGER_SUCCESS` else a `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`.
     * \brief If the uninitialized in the given resource, will allocate memory for the handles.
     * @param PeripheralWriteHandle
     * @param Resource*
     * @return ResourceManagerState `RESOURCE_MANAGER_SUCCESS`, `RESOURCE_MANAGER_RESOURCE_NOT_FOUND` or `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`
     */
    ResourceManagerState (*attach_write_handle)(PeripheralWriteHandle, Resource*);
    /**
     * Searches in the tree to find the URIComponent.
     * @see ResourceLocator
     * @param char* URL path from which to navigate the tree
     * @return URIComponent* or NULL if not found.
     */
    URIComponent* (*search)(char*);
    /**
     * Will return the absolute path string for a given component, if any component name is NULL it will return the result up to that component.
     * \brief It will allocate memory so free it when no longer needed!
     *
     * @param URIComponent* Pointer to URIComponent to look for the absolute path to.
     * @return char* Absolute path string for a given component. If the root name is "" or "/" or "./" or "." it will not be shown.
     */
    char* (*get_path_for_component)(URIComponent*);
    /**
     * Will return the absolute path string for a given resource, if any component name is NULL it will return the result up to that component.
     * \brief It will allocate memory so free it when no longer needed!
     *
     * @param Resource* Pointer to  Resource to look for the absolute path to.
     * @return char* Absolute path string for a given resource and the resource name as action parameter. If there is only one resource, it will return only the path. If the root name is "" or "/" or "./" or "." it will not be shown.
     */
    char* (*get_path_for_resource)(Resource*);
};

extern struct ResourceManager ResourceManager;

/* non-API calls*/
char* string_concat(char* str1, char* str2);
#endif /* RESOURCE_MANAGER_H_*/
