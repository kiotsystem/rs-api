/** @file StreamManager API
 * manager.h
 *
 */
#ifndef STREAM_MANAGER_H_
#define STREAM_MANAGER_H_

#include <stdlib.h>
#include <inttypes.h>

#include "models.h"
#include "structures/circular_buffer.h"

/**
 * \struct StreamManager
 * \brief The Stream is used to get and send data from or to a resource. It is an abstraction of a buffer, so it is possible to choose different types of buffers, for example a linear buffer or a circular buffer. And it is possible to choose to have different buffers for requests and responses or both in one buffer.
 * Technically a Stream is just a Resource, but behaves as a Stream.
 * There's different approaches to the Stream regarding memory:
 * + call => Requests a stream of data from the sensor.
 * + read => Reads data from the sensor once.
 * + write => Writes data to the stream of a sensor.
 *
 */
struct StreamManager
{
    /**
     * Allocates a Stream instance with the passed parameters; By concept, by creating a Stream the Buffers will be isolated. So every stream has its own dedicated buffer. It also possible to share buffers between streams when using StreamManager.clone.
     * \brief Calls malloc to allocates memory for the stream, release using the destroy function in this API.
     * Order sensitive:
     * @param uint8_t size with which it will allocate buffers of this size
     * @param ResettableFlag If the stream can be reseted.
     * @param SingleFlag in case the stream will only allocate a single buffer or Double.
     * @return Stream*
     * @see IsolatedBuffersFlag
     * @see StreamManager.destroy
     * @see StreamManager.clone
     */
    Stream* (*create)(uint8_t, ResettableFlag, SingleFlag);
    /**
     * The clone function is basically dependent on the IsolatedBuffersFlag,
     * if the ISOLATED_BUFFERS flag is passed then it creates a deep copy of the passed stream, else (SHARED_BUFFERS) then it will fully clone it (code will be different)
     * If the given Stream does not have the STREAM_READY state from the HealthCheck, it will return `NULL`
     * If the health check is passed it will allocate memory for a stream
     * @see IsolatedBuffersFlag
     * @see StreamHealth
     * @param Stream* Stream to clone.
     * @param IsolatedBuffersFlag In case the newly created Stream will share the buffers with the cloned stream or it needs to instantiate new buffers for it.
     * @return Stream* The new stream.
     * @see StreamManager.destroy
     */
    Stream* (*clone)(Stream*, IsolatedBuffersFlag);
    /**
     * \brief Allocates memory for the handles
     *
     * @return an instance of Handles with its function pointers set to `NULL`
     * @see StreamManager_destroy_handles
     */
    Handles* (*create_handles)();
    /**
     * Attaches the Handles to the Stream
     * @param Handles* to register to the Stream
     * @param Stream* target Stream
     */
    void (*attach)(Handles*, Stream*);
    /**
     * \brief Will free the memory of the stream, in the case of shared buffers calling this function on any of them will free the common shared buffer.
     * @param Stream* to destroy, this will free its memory
     * @return StreamHealth
     */
    StreamHealth (*destroy)(Stream*);
    /**
     *Resets the buffers attached to the Streams if the resettable flag is set
     * @param Stream* stream from which the buffer pointers will be reset
     */
    void (*reset_buffers)(Stream*);
    /**
     * Calls the stream to generate data.
     * Calling this will trigger the StreamCall Handle of the Stream.
     * @param Stream* which will perform the call to the handle.
     * @param Request*
     * @param Parameter[]
     * @param Response*
     * @return Response*
     */
    Response* (*call)(Stream*, Request*, Parameter*[], Response*);
    /**
     * Calls the stream to get some data, this call will get always data from the buffer, if the buffer is full then will generate data from the Handle and return a pointer to the data.
     * Calling this, will trigger the handle StreamInput Handle, from the return value from the handle will be either appended to the Buffers or simply will use the attached buffers.
     * If there's data returned it will automatically be written in the output buffer, therefore a NULL pointer is expected to define the end of the data.
     * @param Stream* from which will get the data from
     * @param Request*
     * @param Parameter[]
     * @param Response*
     * @return Response*
     */
    Response* (*read)(Stream*, Request*, Parameter*[], Response*);
    /**
     * Calls the stream to create data from the peripheral, buffer-wise it applies the same than from the get handle.
     * Calling this, will trigger the handle StreamOutput Handle.
     * If there's data returned it will automatically be written in the output buffer, therefore a NULL pointer is expected to define the end of the data.
     * @param Stream*
     * @param Request*
     * @param Parameter[]
     * @param Response*
     * @return Response*
     */
    Response* (*write)(Stream*, Request*, Parameter*[], Response*);
    /**
     * Checks the health of the Stream and returns it
     * @see StreamHealth
     * @param Stream* from which to evaluate the health
     * @return The Stream Health
     */
    StreamHealth (*check_health)(Stream*);
    /**
     * Checks health of stream buffers and returns it.
     * @see StreamHealth
     * @param Stream* from which to evaluate the health
     * @return The Stream Health
     */
    StreamHealth (*check_buffer_health)(Stream*);
};
extern struct StreamManager StreamManager;

/* Non-API functions */
/**
 * Creates a buffer for a stream.
 * @param StreamBuffers*
 * @param uint8_t
 * @return Buffer instantiated, `NULL` if error and sets the BufferStatus flag in the Buffers
 * @see StreamManager.destroy
 */
Buffer* StreamManager_create_buffer(StreamBuffers*, uint8_t);
/**
 * @see IsolatedBuffersFlag
 * @param uint8_t size
 * @param SingleFlag single
 * @return Buffers instantiated, if failed returns `NULL`, if it failed to allocate memory while allocating the buffer through CircularBuffers, then the `NULL` pointer is assigned to the respective buffers.
 * @see StreamManager_destroy
 */
StreamBuffers* StreamManager_create_buffers(uint8_t, SingleFlag);
/**
 * Allocates memory for the handles
 * @return instantiates an instance of Handles and returns it
 * @see StreamManager_destroy_handles
 */
Handles* StreamManager_create_handles();
/**
 * This does not call free on each function, just in the Handles instance
 * @param Stream* stream from which to free the handles instance
 * @return StreamHealth
 */
StreamHealth StreamManager_destroy_handles(Stream* stream);
/**
 * Frees the buffers of a stream.
 * @param Stream* stream from which to free the buffers
 * @return StreamHealth
 */
StreamHealth StreamManager_destroy_buffers(Stream* stream);
/**
 * checks health of stream buffers and returns it
 * @see StreamHealth
 * @param Stream* stream from which to check the status from
 * @return StreamHealth
 */
StreamHealth StreamManager_check_buffer_health(Stream* stream);
/**
 * Will make use of the `CircularBuffer` and override the data from the output buffer of the Stream.
 * @see CircularBuffer
 * @param Stream*
 * @param uint8_t* data
 * @param uint8_t data length
 * @return BufferStatus
 */
BufferStatus
StreamManager_override_buffer(Stream*, uint8_t*, uint8_t);
/**
 * Writes the data passed to the Buffer
 * \brief please note that it writes the data until a NULL pointer is reached.
 * @param uint8_t* data
 * @param uint8_t* data_size
 * @param Stream*
 * @return BufferStatus
 */
BufferStatus
StreamManager_write_to_buffer(uint8_t*, uint8_t, Stream*);

#endif /* STREAM_MANAGER_H_ */
