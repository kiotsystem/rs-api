/**
 * buffer.c
 */

#include "buffer.h"


/**
 * Returns a list of the chosen size.
 * \brief Allocates memory for the array with malloc.
 *
 * @param uint8_t size of the buffer to allocate
 * @return uint8_t* pointer to the memory slot
 */
uint8_t*
Buffer_allocate(uint8_t size)
{
    uint8_t *my_list = malloc(sizeof(uint8_t) * size);
    return my_list;
}

/**
 * BufferFactory initialisation
 */
struct
BufferFactory BufferFactory = {
    .create = Buffer_allocate
};


/* Buffer API implementation */


/**
 * Creates a Buffer instance which can hold multiple uint8_t values. This function calls malloc.
 * @see BufferManager.destroy
 * @param uint8_t data size to allocate
 * @return Buffer* A pointer to the newly created buffer.
 */
Buffer*
BufferManager_create(uint8_t data_size)
{
    Buffer* buffer = malloc(sizeof(Buffer));

    if (buffer != NULL)
    {
        buffer->count = 0;
        buffer->size = data_size;
        buffer->data = BufferFactory.create(data_size);

        if (buffer->data != NULL)
        {
            buffer->head = buffer->data;
            buffer->tail = buffer->data;
            buffer->end = buffer->data + buffer->size;
        }
    }

    return buffer;
}

/**
 * Clones the data from the Buffer, this function calls calloc, please free the memory!
 * If the buffer.count is 0 then it returns NULL.
 * @param Buffer*
 * @return uint8_t* cloned data
 */
uint8_t*
BufferManager_clone_data(Buffer* buffer)
{
    if (buffer->count == 0)
    {
        return NULL;
    }

    uint8_t* cloned_data = calloc(buffer->count, sizeof(uint8_t));
    memcpy(cloned_data, buffer->data, buffer->count);

    return cloned_data;
}

/**
 * Destroys the Buffer
 * @param Buffer*
 */
void
BufferManager_destroy(Buffer* buffer)
{
    if (buffer != NULL)
    {
        if (buffer->data != NULL)
        {
            free(buffer->data);
            buffer->data = NULL;
        }
        free(buffer);
        buffer = NULL;
    }
}

/**
 * Loops over the size of the Buffer and sets the content to 0x00
 * @see Buffer
 * @param Buffer*
 */
void
BufferManager_reset_data(Buffer* buffer)
{
    uint8_t* data_reference = buffer->data;

    for (uint8_t i = 0; i < buffer->size; i++)
    {
        data_reference[i] = 0x00;
    }
}

/**
 * Resets the whole Buffer instance to the following criteria
 * + Sets the head to the initial position
 * + Sets the tail to the initial position
 * + Resets the count to 0
 * + Sets the whole data to 0
 * @see BufferApi.reset_data
 * @param Buffer*
 */
void
BufferManager_reset(Buffer* buffer)
{
    buffer->head = buffer->data;
    buffer->tail = buffer->data;
    buffer->count = 0;
    BufferManager_reset_data(buffer);
}

/**
 * This function attempts to insert the data in the Buffer (linearly) returns the data + 0x01 if the buffer is full, else returns the data passed.
 * @see Buffer
 * @param uint8_t data
 * @param Buffer*
 * @return uint8_t data (+ 0x01)
 */
uint8_t
BufferManager_push(uint8_t data, Buffer* buffer)
{
    if (buffer->count >= buffer->size)
    {
        // can't add anymore
        return data + 0x01;
    }

    uint8_t* data_pointer = buffer->data + buffer->count;
    memcpy(data_pointer, &data, sizeof(uint8_t));
    buffer->count = buffer->count + 1;  // increment the count
    buffer->head = buffer->head + 1;  // increment the head

    return data;
}

/**
 * pops the last element, if the function is called more than the amount of elements the Buffer has,
 * then returns always the tail value (last element) 
 * @param Buffer*
 * @return uint8_t element popped
 */
uint8_t
BufferManager_pop(Buffer* buffer)
{
    if (buffer->count == 0)
    {
        // no more elements to pop
        return *(buffer->tail);
    }
    
    uint8_t* tail = buffer->tail;
    buffer->count = buffer->count - 1;  // decrement the count
    buffer->tail = buffer->tail + 1;  // increment the tail

    uint8_t data_popped = *tail;

    return data_popped;
}

/**
 * Checks if the Buffer is full, returns 0 when is not full and 1 when is full
 * @param Buffer*
 * @return uint8_t 0 when not full, 1 when full
 */
uint8_t
BufferManager_get_availability(Buffer* buffer)
{
    if (buffer == NULL)
    {
        return 0;
    }
    
    return buffer->size - buffer->count;
}

/**
 * Returns 0 if it cannot fit the difference between element count and size, 1 if it can fit it.
 * @param size_t size
 * @param Buffer*
 * @return uint8_t 0 or 1
 */
uint8_t
BufferManager_buffer_can_fit(size_t size, Buffer* buffer)
{
    if (buffer == NULL)
    {
        return 0;
    }

    uint8_t difference = buffer->size - buffer->count;

    return difference >= (uint8_t) size;
}

/* Initialize the BufferManager API */
struct
BufferApi BufferManager = {
    .create = BufferManager_create,
    .clone_data = BufferManager_clone_data,
    .destroy = BufferManager_destroy,
    .push = BufferManager_push,
    .pop = BufferManager_pop,
    .get_availability = BufferManager_get_availability,
    .buffer_can_fit = BufferManager_buffer_can_fit,
    .reset = BufferManager_reset,
    .reset_data = BufferManager_reset_data
};
