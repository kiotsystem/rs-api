/** @file circular_buffer.h
 *
 */

#ifndef STREAMERS_STRUCTURES_CIRCULARBUFFER_H_
#define STREAMERS_STRUCTURES_CIRCULARBUFFER_H_

#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include "buffer.h"


/**
 * \enum CircularBufferStep enumeration
 */
typedef enum CircularBufferStep
{
    CIRCULAR_BUFFER_DECREMENT = -1,
    CIRCULAR_BUFFER_NO_INCREMENT = 0,
    CIRCULAR_BUFFER_INCREMENT = 1,
    CIRCULAR_BUFFER_DOUBLE = 2
}
CircularBufferStep;

/**
 * \deprecated {The circular buffer is not to be used, use a regular buffer instead }
 * \struct CircularBuffer
 * Circular Buffer methods, handles a `Buffer` as a circular buffer.
 * The underlying implementation shall care about updating the pointers of the Buffer.
 * @see Buffer
 */
struct CircularBuffer
{
    /**
     * Factory for the Circular Buffer. Returns an instantiation of the buffer with the given size.
     * \brief Allocates memory for the response and for the buffer (array), to free use free function in this API
     *
     * @param uint8_t size of the new buffer
     * @return Buffer instantiated with the given size, if failed then returns a `NULL` pointer
     * @see CircularBuffer.free
     */
    Buffer* (*create)(uint8_t);
    /**
    * Pops the current element from the buffer, this function updates the pointers of the Buffer
    * @param Buffer* from which to pop an element from
    * @see BufferStatus
    * @return uint8_t data read from the Buffer if succeeded to update pointers, else will return a `BUFFER` error (casted to uint8_t)
    */
    uint8_t (*pop)(Buffer*);
   /**
    * Gets/reads from the buffer without moving the pointers
    * @param Buffer* buffer from which to read the data from
    * @return uint8_t data read from the buffer or a `BUFFER_EMPTY`.
    */
    uint8_t (*get)(Buffer*);
   /**
    * Inserts the data to the buffer and returns the data inserted
    * @param Buffer* Pointer to buffer from which to push the data
    * @param uint8_t data slice
    * @return uint8_t The data pushed to the buffer or `BUFFER_FULL` (casted to uint8_t).
    */
    uint8_t (*push)(Buffer*, uint8_t);
   /**
    * Returns the actual amount of elements in the buffer.
    * @param Buffer* buffer from which to get the amounts of elements.
    * @return uint8_t Returns the count of the elements from the Buffer.
    */
    uint8_t (*count)(Buffer*);
    /**
     * Returns a `BUFFER_READY` if the buffer passed is not full but ready to be used, a `BUFFER_EMPTY` if the buffer is empty and not being initialized (size is 0) and a `BUFFER_FULL` when is full; if at any point pointer are not syncronised properly it returns a `BUFFER_ERROR`.
     * @param Buffer*
     * @return BufferStatus
     */
    BufferStatus (*check_health)(Buffer*);
    /**
     * Resets buffer to starting state
     * @param Buffer* to reset
     */
    void (*reset)(Buffer*);
    /**
     * @param Buffer* from which to free the data
     * @return the Buffer but with a `NULL` pointer in `data`
     */
    Buffer* (*clean_up)(Buffer*);
    /**
     * @param Buffer* from which to free the memory
     */
    void (*free)(Buffer*);
};

extern struct CircularBuffer CircularBuffer;

/* Non Api functions */

/**
 * Updates the pointer from the buffer by the amount of steps passed.
 * @param Buffer* buffer to update the pointer from
 * @param BufferPointer buffer_pointer
 * @param CircularBufferStep step to increment the buffer's head pointer
 * @param CircularBufferStep counter_step is the value to update the counter value from.
 * @return BufferStatus that represents the buffer status after the update, returns a `BUFFER_READY` once it's done.
 */
BufferStatus
CircularBuffer_update_pointer(Buffer*, BufferPointer, CircularBufferStep, CircularBufferStep);

#endif /* STRUCTURES_CIRCULARBUFFER_H_ */
