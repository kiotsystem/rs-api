#ifndef STRUCTURES_MODELS_H_
#define STRUCTURES_MODELS_H_

/**
 * \typedef BufferStatus
 * This is a representation of the status of the Buffer
 * + When having a BUFFER_EMPTY the state of the buffer is empty
 * + When the buffer has `BUFFER_BUSY` then the buffer is being used.
 * + When having `BUFFER_ERROR` then the buffer is internally corrupted.
 */
typedef enum BufferStatus
{
    BUFFER_EMPTY = 0,
    BUFFER_FULL = 2,
    BUFFER_ERROR = -1,
    BUFFER_READY = 1,
    BUFFER_BUSY = 3,
    BUFFER_UNINITIALIZED = 4
} BufferStatus;

/**
 * Enumeration for referring to the head or tail.
 * Used for referring to the Buffer's target updating pointer.
 * Used internally only, not meant to be used externally.
 */
typedef enum BufferPointer
{
    HEAD,
    TAIL
} BufferPointer;

/**
 * Structure to represent a linear Buffer. A Buffer is needed for the stream of the data from sensors.
 * The project is buffer structure independent. It offers two implementations,
 * CircularBuffer, a circular FIFO buffer, and BufferManager API, a linear FIFO buffer.
 * The actual logic implementation of the Buffer is left upon the Developer using the project.
 */
typedef struct Buffer
{
    uint8_t *data; /**< Pointer to data contained in the buffer. */
    uint8_t *end; /**< Points to the end of the buffer. */
    uint8_t *head; /**< Pointer to the head of the buffer. */
    uint8_t *tail; /**< Pointer to the end of the buffer. */
    uint8_t count; /**< Counter holding the amount of elements. */
    uint8_t size;  /**< Size of the buffer. This is how many elements the buffer can hold. */
} Buffer;

#endif /* STRUCTURES_MODELS_H_ */
