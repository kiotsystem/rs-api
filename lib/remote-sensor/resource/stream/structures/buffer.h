/*
 * buffer.h
 *
 */

#ifndef STRUCTURES_BUFFER_H_
#define STRUCTURES_BUFFER_H_

#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

#include "models.h"

/**
 * Buffer factory
 */
struct BufferFactory
{
    /**
     * Returns a list of the chosen size to use as a buffer.
     * \brief Allocates memory for the array with malloc.
     *
     * @param uint8_t size of the buffer to allocate
     * @return uint8_t*
     */
    uint8_t* (*create)(uint8_t);
};

/**
 * \struct BufferApi
 * Deals with the Buffer's operations treating an array of data as a linear Buffer.
 * @see Buffer
 * @see StreamManager.create
 */
struct BufferApi
{
    /**
     * Creates a Buffer instance which can hold multiple uint8_t values. This function calls malloc.
     * @param uint8_t data size to allocate
     * @return Buffer* A pointer to the newly created buffer.
     * @see BufferManager.destroy
     */
    Buffer* (*create)(uint8_t);
    /**
     * Clones the data from the Buffer, this function calls calloc, please free the memory!
     * If the buffer.count is 0 then it returns NULL.
     * @param Buffer*
     * @return uint8_t* cloned data
     */
    uint8_t* (*clone_data)(Buffer*);
    /**
     * Destroys the Buffer
     * @param Buffer* to destroy
     */
    void (*destroy)(Buffer*);
    /**
     * Checks if the Buffer is full, returns 0 when is not full and 1 when is full
     * @param Buffer*
     * @return uint8_t 0 when not full, 1 when full
     */
    uint8_t (*get_availability)(Buffer*);
    /**
     * Returns 0 if it cannot fit the difference between element count and size, 1 if it can fit it.
     * @param size_t size
     * @param Buffer*
     * @return uint8_t 0 or 1
     */
    uint8_t (*buffer_can_fit)(size_t, Buffer*);
    /**
     * This function attempts to insert the data in the Buffer (linearly) returns the data + 0x01 if the buffer is full, else returns the data passed.
     * @see Buffer
     * @param uint8_t data
     * @param Buffer*
     * @return uint8_t
     */
    uint8_t (*push)(uint8_t, Buffer*);
    /**
     * pops the last element, if the function is called more than the amount of elements the Buffer has,
     * then returns always the tail value (last element) 
     * @return uint8_t element popped
     */
    uint8_t (*pop)(Buffer*);
    /**
     * Resets the whole Buffer instance to the following criteria
     * + Sets the head to the initial position
     * + Sets the tail to the initial position
     * + Resets the count to 0
     * + Sets the whole data to 0
     * @see BufferApi.reset_data
     */
    void (*reset)(Buffer*);
    /**
     * Loops over the size of the Buffer and sets the content to 0x00
     * @see Buffer
     * @param Buffer*
     */
    void (*reset_data)(Buffer*);
};

extern struct BufferFactory BufferFactory;
extern struct BufferApi BufferManager;

#endif /* STRUCTURES_BUFFER_H_ */
