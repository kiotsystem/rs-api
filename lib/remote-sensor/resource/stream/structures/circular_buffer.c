/** @file circular_buffer.c
 * circular_buffer.c
 *
 */

#include "circular_buffer.h"


/**
 * Factory for the Circular Buffer. Returns an instantiation of the buffer with the given size.
 * \brief Calls malloc to allocates memory for the response, to free use CircularBuffer.free
 * @param uint8_t size of the new buffer
 * @return Buffer* to the buffer created.
 * @see CircularBuffer.free
 */
Buffer*
CircularBuffer_create(uint8_t size)
{
    uint8_t *data = BufferFactory.create(size);

    if (data == NULL)
    {
        return NULL;
    }

    Buffer *buffer = malloc(sizeof(struct Buffer));

    buffer->size = size;
    buffer->data = data;
    buffer->end = buffer->data + size - 1;

    buffer->count = 0;
    buffer->head = data;
    buffer->tail = data;

    return buffer;
};

/**
 * Updates the pointer from the buffer by the amount of steps passed.
 * @param Buffer* buffer to update the pointer from
 * @param BufferPointer buffer_pointer
 * @param CircularBufferStep step to increment the buffer's head pointer
 * @param CircularBufferStep counter_step is the value to update the counter value from.
 * @return BufferStatus that represents the buffer status after the update, returns a `BUFFER_READY` once it's done.
 */
BufferStatus
CircularBuffer_update_pointer(Buffer* buffer, BufferPointer buffer_pointer, CircularBufferStep step, CircularBufferStep counter_step)
{
    uint8_t** pointer = (buffer_pointer == HEAD) ? &(buffer->head) : &(buffer->tail);
    if (step != CIRCULAR_BUFFER_NO_INCREMENT)
    {
        *pointer = (*pointer == buffer->end) ? buffer->data : *pointer + (int8_t) step;
    }
    buffer->count = buffer->count + (int8_t) counter_step;  // updates the count

    return BUFFER_READY;
}

/**
 * Inserts the data to the buffer and returns the data inserted
 * @param Buffer* Pointer to buffer from which to push the data
 * @param uint8_t Data slice.
 * @return uint8_t The data pushed to the buffer or `BUFFER_FULL` (casted to uint8_t).
 */
uint8_t
CircularBuffer_push(Buffer* buffer, uint8_t data)
{
    if (buffer->count >= buffer->size && buffer->head == buffer->tail)
    {
        return (uint8_t) BUFFER_FULL;
    }
    
    memcpy(buffer->head, &data, sizeof(uint8_t));
    uint8_t inserted_data = *(buffer->head);

    BufferStatus status = CircularBuffer_update_pointer(buffer, HEAD, CIRCULAR_BUFFER_INCREMENT, CIRCULAR_BUFFER_INCREMENT);

    if (status == BUFFER_READY)
    {
        return inserted_data;
    }

    return (uint8_t) BUFFER_ERROR;
}

/**
 * Gets/reads from the buffer without moving the pointers
 * @param Buffer* buffer from which to read the data from
 * @return uint8_t data read from the buffer or a `BUFFER_EMPTY`.
 */
uint8_t
CircularBuffer_get(Buffer* buffer)
{
    if (buffer->count == 0)
    {
        return BUFFER_EMPTY;
    }

    uint8_t data_from_buffer = *(buffer->tail);
    return data_from_buffer;
}

/**
 * Pops the current element from the buffer, this function updates the pointers of the Buffer
 * @param Buffer* from which to pop an element from
 * @see BufferStatus
 * @return uint8_t data read from the Buffer if succeeded to update pointers, else will return a `BUFFER` error (casted to uint8_t)
 */
uint8_t
CircularBuffer_pop(Buffer* buffer)
{
    uint8_t data_from_buffer = CircularBuffer_get(buffer);

    if (data_from_buffer != BUFFER_EMPTY)
    {
        CircularBufferStep tail_step = CIRCULAR_BUFFER_INCREMENT;
        CircularBufferStep counter_step = CIRCULAR_BUFFER_DECREMENT;
        BufferStatus status = CircularBuffer_update_pointer(buffer, TAIL, tail_step, counter_step);
        if (status == BUFFER_READY)
        {
            return data_from_buffer;
        }
    }

    return (uint8_t) BUFFER_ERROR;
};

/**
 * Returns the actual amount of elements in the buffer.
 * @param Buffer* buffer from which to get the amounts of elements.
 * @return uint8_t Returns the count of the elements from the Buffer.
 */
uint8_t
CircularBuffer_count(Buffer* buffer)
{
    return buffer->count;
}

/**
 * Returns a `BUFFER_READY` if the buffer passed is not full but ready to be used, a `BUFFER_EMPTY` if the buffer is empty and not being initialized (size is 0) and a `BUFFER_FULL` when is full; if at any point pointer are not synchronized properly it returns a `BUFFER_ERROR`.
 * @param Buffer* from which to return the value from
 * @return BufferStatus
 */
BufferStatus
CircularBuffer_health_check(Buffer* buffer)
{
    if ((buffer->size == (uint8_t) NULL || buffer->size == 0)
            && (buffer->count == (uint8_t) NULL || buffer->count == 0)
            && buffer->data == NULL)
        return BUFFER_UNINITIALIZED;

    if (buffer->count == 0 && buffer->size == 0 && buffer->data != NULL)
        return BUFFER_EMPTY;

    if (buffer->count == 0 && buffer->size > 0 && buffer->data != NULL)
        return BUFFER_EMPTY;

    if (buffer->count == buffer->size && buffer->data != NULL)
        return BUFFER_FULL;

    if (buffer->count > buffer->size
            || (buffer->data == NULL && buffer->size > 0))
        return BUFFER_ERROR;

    return BUFFER_READY;
}

/**
 * Resets buffer to starting state
 * @param Buffer* to reset
 */
void
CircularBuffer_reset(Buffer* buffer)
{
    if (buffer != NULL)
    {
        buffer->head = buffer->data;
        buffer->tail = buffer->data;
        buffer->count = 0;
    }
}

/**
 * Frees the memory from the buffer.
 * Assigns the size from the buffer to `BUFFER_UNINITIALIZED` and the count to `BUFFER_EMPTY`.
 * @param Buffer*
 * @return Buffer*
 */
Buffer*
CircularBuffer_clean_up(Buffer* buffer)
{
    if (buffer != NULL)
    {
        free(buffer->data);
        buffer->data = NULL;

        buffer->size = BUFFER_UNINITIALIZED;
        buffer->count = BUFFER_EMPTY;
    }

    return buffer;
}

/**
 * Cleans up all the Buffer's instances from the memory
 * @param Buffer* to delete
 */
void
CircularBuffer_free(Buffer* buffer)
{
    if (buffer != NULL)
    {
        if (buffer->size != BUFFER_UNINITIALIZED)
        {
            free(buffer->data);
            buffer->data = NULL;
        }

        free(buffer);
        buffer = NULL;
    }
}

/**
 * Making the CircularBuffer Functions constants
 */
struct
CircularBuffer CircularBuffer = {
    .create = CircularBuffer_create,
    .pop = CircularBuffer_pop,
    .get = CircularBuffer_get,
    .push = CircularBuffer_push,
    .count = CircularBuffer_count,
    .check_health = CircularBuffer_health_check,
    .reset = CircularBuffer_reset,
    .clean_up = CircularBuffer_clean_up,
    .free = CircularBuffer_free

};
