/**
 * @file stream.c
 */
#include "manager.h"

/**
 * Creates a buffer for a stream.
 * @param StreamBuffers*
 * @param uint8_t
 * @return Buffer instantiated, `NULL` if error and sets the BufferStatus flag in the Buffers
 * @see StreamManager.destroy
 */
Buffer*
StreamManager_create_buffer(StreamBuffers* buffers, uint8_t size)
{
    Buffer* buffer = CircularBuffer.create(size);
    buffers->status = BUFFER_READY;

    if (buffer == NULL)
    {
        buffers->status = BUFFER_ERROR;
    }

    return buffer;
}

/**
 * Creates one or two buffers for a stream, depends on the SingleFlag.
 * @see IsolatedBuffersFlag
 * @param uint8_t size
 * @param SingleFlag single
 * @return Buffers instantiated, if failed returns `NULL`, if it failed to allocate memory while allocating the buffer through CircularBuffers, then the `NULL` pointer is assigned to the respective buffers.
 * @see StreamManager_destroy
 */

StreamBuffers*
StreamManager_create_buffers(uint8_t size, SingleFlag single_flag)
{
    StreamBuffers *buffers = malloc(sizeof(StreamBuffers));
    
    if (buffers == NULL)
    {
        return NULL;
    }
    
    buffers->output = StreamManager_create_buffer(buffers, size); 

    buffers->input = NULL;
    if (single_flag == DOUBLE_BUFFER)
    {
        buffers->input = StreamManager_create_buffer(buffers, size); 
    }

    buffers->single_buffer = single_flag;
    buffers->isolated = ISOLATED_BUFFERS;

    return buffers;
}

/**
 * Allocates memory for the handles
 * @return instantiates an instance of Handles and returns it
 * @see StreamManager_destroy_handles
 */
Handles*
StreamManager_create_handles()
{
    Handles* handle = malloc(sizeof(Handles));
 
    handle->call_handle = NULL;
    handle->write_handle = NULL;
    handle->read_handle = NULL;
 
    return handle;
}

/* End of helper functions */

/**
 * Allocates a Stream instance with the passed parameters, for optional parameters pass NULL; By concept, by creating a Stream the Buffers will be isolated. So every stream has its own dedicated buffer. It also possible to share buffers between streams when using StreamManager.clone.
 * \brief Calls malloc to allocates memory for the stream, release using the destroy function in this API.
 * @param uint8_t size with which it will allocate buffers of this size
 * @param ResettableFlag flush will be passed to the Stream, refer to it for more information
 * @param SingleFlag single in case the stream will only allocate a single buffer.
 * @return a Stream instance or `NULL` if the allocation failed. Worth to note that allocation status applies here, too.
 * @see IsolatedBuffersFlag
 * @see StreamManager.destroy
 * @see StreamManager.clone
 */
Stream*
StreamManager_create(uint8_t size, ResettableFlag resettable, SingleFlag single)
{
    StreamBuffers* buffers = StreamManager_create_buffers(size, single);
    Stream *stream = malloc(sizeof(Stream));

    if (stream == NULL)
    {
        return NULL;
    }

    // initialize the stream
    stream->buffers = buffers;
    stream->handles = StreamManager_create_handles();
    stream->resettable = resettable;

    return stream;
}

/**
 * checks health of stream buffers and returns it
 * @see StreamHealth
 * @param Stream* stream from which to check the status from
 * @return StreamHealth
 */
StreamHealth
StreamManager_check_buffer_health(Stream* stream)
{
    if (stream->buffers == NULL || stream->buffers->status == BUFFER_ERROR)
    {
        return STREAM_ERROR;
    }
    
    if (stream->buffers->output == NULL)
    {
        return STREAM_ERROR;
    }
    
    if (stream->buffers->single_buffer == DOUBLE_BUFFER && stream->buffers->input == NULL)
    {
        return STREAM_ERROR;
    }

    BufferStatus buffer_state = stream->buffers->status;
    if (buffer_state == BUFFER_FULL || buffer_state == BUFFER_BUSY)
    {
        return STREAM_BUSY;
    }

    return STREAM_READY;
}

/**
 * Checks the overall state of the passed stream.
 * @see StreamHealth
 * @param Stream* Stream which will be checked.
 * @return StreamHealth
 */
StreamHealth
StreamManager_check_health(Stream* stream)
{
    if (stream == NULL || stream->handles == NULL)
    {
        return STREAM_ERROR;
    }

    StreamHealth buffer_feedback = StreamManager_check_buffer_health(stream);

    return buffer_feedback;
}

/**
 * \fn Stream_attach
 *
 * Attaches the Handles struct into the Stream
 * @param Handles* handles
 * @param Stream* stream
 */
void
StreamManager_attach(Handles* handles, Stream* stream)
{
    stream->handles = handles;
}

/**
 * Frees the buffers of a stream.
 * @param Stream* stream from which to free the buffers
 * @return StreamHealth
 */
StreamHealth
StreamManager_destroy_buffers(Stream* stream)
{
    if (stream == NULL)
    {
        return STREAM_NULL;
    }

    if (stream->buffers != NULL)
    {
        CircularBuffer.free(stream->buffers->output);
        if (stream->buffers->input != NULL)
        {
            CircularBuffer.free(stream->buffers->input);
        }
        free(stream->buffers);
        stream->buffers = NULL;

    }
        return STREAM_DESTROYED;
}

/**
 * This does not call free on each function, just in the Handles instance
 * @param Stream* stream from which to free the handles instance
 * @return StreamHealth
 */
StreamHealth
StreamManager_destroy_handles(Stream* stream)
{
    if (stream->handles == NULL)
    {
        return STREAM_NULL;
    }
        free(stream->handles);
        stream->handles = NULL;
        return STREAM_DESTROYED;
}

/**
 * \fn destroy
 * Destroys the passed Stream* and its contents if
 * stream is not NULL
 * @param Stream*
 * @return StreamHealth
 */
StreamHealth
StreamManager_destroy(Stream* stream)
{
    if(stream == NULL)
    {
        return STREAM_NULL;
    }
    // free the buffers
    StreamManager_destroy_buffers(stream);
    // free the handles
    StreamManager_destroy_handles(stream);
    free(stream);
    stream = NULL;
    return STREAM_DESTROYED;
}

/**
 * The clone function is basically dependent on the IsolatedBuffersFlag,
 * if the ISOLATED_BUFFERS flag is passed then it creates a deep copy of the passed stream, else (SHARED_BUFFERS) then it will fully clone it (code will be different)
 * If the given Stream does not have the STREAM_READY state from the HealthCheck, it will return `NULL`
 * If the health check is passed it will allocate memory for a stream
 * @see IsolatedBuffersFlag
 * @see StreamHealth
 * @param Stream* Stream to clone.
 * @param IsolatedBuffersFlag In case the newly created Stream will share the buffers with the cloned stream or it needs to instantiate new buffers for it.
 * @return Stream* The new stream.
 * @see StreamManager_destroy
 */
Stream*
StreamManager_clone(Stream* original, IsolatedBuffersFlag isolation)
{
    if (StreamManager.check_health(original) != STREAM_READY)
    {
        return NULL;
    }

    Stream *stream;

    if (isolation == ISOLATED_BUFFERS)
    {
        uint8_t size = original->buffers->output->size;
        SingleFlag single_flag = original->buffers->single_buffer;
        
        stream = StreamManager.create(size, original->resettable, single_flag);
        stream->handles = original->handles;

        return stream;
    }

    stream = malloc(sizeof(Stream));
    stream->buffers = original->buffers;
    stream->buffers->isolated = SHARED_BUFFERS;
    stream->handles = original->handles;
    stream->resettable = original->resettable;

    return stream;
}

/**
 *Resets the buffers attached to the Streams if the resettable flag is set
 * @param Stream* stream from which the buffer pointers will be reset
 */
void
StreamManager_reset_buffers(Stream* stream)
{
    if (stream->resettable == IS_RESETTABLE)
    {
        if (StreamManager_check_buffer_health(stream) == STREAM_READY)
        {
            CircularBuffer.reset(stream->buffers->output);
            if (stream->buffers->single_buffer == DOUBLE_BUFFER)
            {
                CircularBuffer.reset(stream->buffers->input);
            }
        }
    }
}

/**
 * Calls the Stream Call Handle.
 * This will only be fully executed if the stream's health is STREAM_READY and of course there's a call attached to the stream.
 * This call won't flush the buffers even if the flag is set to true.
 * It is up to the Resource to flush the stream.
<<<<<<< HEAD
 * @see PeripheralCallHandle
 * @param Stream* pointer to a stream from which the callback will be called.
=======
 * @param Stream* pointer to a stream from which it the callback will be called.
>>>>>>> refs/remotes/origin/develop
 * @param Request*
 * @param Parameter[]
 * @param Response*
 * @return Response*
 */
Response*
StreamManager_call_handle(Stream* stream, Request* request, Parameter* parameters[], Response* response)
{
    if (StreamManager.check_health(stream) == STREAM_READY)
    {
        PeripheralCallHandle call_back = stream->handles->call_handle;
        
        if (call_back != NULL)
        {
            if (stream->resettable == IS_RESETTABLE)
            {
                BufferManager.reset(stream->buffers->output);
            }
            // call the callback
            call_back(stream->buffers, request, parameters, response);
            // relate the buffer's data to the response
            response->data = stream->buffers->output->data;
            response->data_length = stream->buffers->output->count;
        }
    }
    return response;
}

/**
 * Calls the stream to get some data, this call will get always data from the buffer, if the buffer is full then will generate data from the Handle and return a pointer to the data.
 * Calling this, will trigger the handle StreamInput Handle, from the return value from the handle will be either appended to the Buffers or simply will use the attached buffers.
 * @param Stream* From which will get the data from.
 * @param Request*
 * @param Parameter[]
 * @param Response*
 * @return Response*
 */
Response*
StreamManager_call_read(Stream* stream, Request* request, Parameter* parameters[], Response* response)
{
    if (StreamManager.check_health(stream) == STREAM_READY)
    {
        PeripheralReadHandle call_back = stream->handles->read_handle;

        if (call_back != NULL)
        {
            if (stream->resettable == IS_RESETTABLE)
            {
                BufferManager.reset(stream->buffers->output);
            }
            call_back(stream->buffers, request, parameters, response);
            // relate the buffer's data to the response
            response->data = stream->buffers->output->data;
            response->data_length = stream->buffers->output->count;
        }
    }

    return response;
}

/**
 * Calls the Stream's Peripheral write handle.
 * It is up to the Resource to flush the stream.
 * @param Stream*
 * @param Request*
 * @param Parameter[]
 * @param Response*
 * @return Response*
 */
Response*
StreamManager_call_write(Stream* stream, Request* request, Parameter* parameters[], Response* response)
{
    if (StreamManager.check_health(stream) == STREAM_READY)
    {
        PeripheralWriteHandle call_back = stream->handles->write_handle;
        
        if (call_back != NULL)
        {
            if (stream->resettable == IS_RESETTABLE)
            {
                BufferManager.reset(stream->buffers->output);
            }
            call_back(stream->buffers, request, parameters, response);
            // relate the buffer's data to the response
            response->data = stream->buffers->output->data;
            response->data_length = stream->buffers->output->count;
        }
    }
    
    return response;
}

/* Initialization */
struct
StreamManager StreamManager = {
    .create = StreamManager_create,
    .clone = StreamManager_clone,
    .create_handles = StreamManager_create_handles,
    .destroy = StreamManager_destroy,
    .call = StreamManager_call_handle,
    .read = StreamManager_call_read,
    .write = StreamManager_call_write,
    .check_health = StreamManager_check_health,
    .reset_buffers = StreamManager_reset_buffers,
    .check_buffer_health = StreamManager_check_buffer_health
};
