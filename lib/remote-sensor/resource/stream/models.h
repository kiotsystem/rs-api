#ifndef STREAM_MODELS_H_
#define STREAM_MODELS_H_

#include "structures/buffer.h"
#include <remote-sensor/communication/models.h>

/**
 * \enum ResettableFlag
 * Signals if a stream can be reset, so it will be emptied.
 * @see StreamManager.reset_buffers
 */
typedef enum
ResettableFlag
{
    NOT_RESETTABLE,
    IS_RESETTABLE
} ResettableFlag;

/**
 * \enum SingleFlag
 * A stream can have just one buffer for input and output or, if needed, one dedicated buffer for input and one dedicated for output, to separate the data. This flag signals if a stream has only one buffer.
 */
typedef enum
SingleFlag
{
    DOUBLE_BUFFER,
    SINGLE_BUFFER

}SingleFlag;

/**
 * \enum IsolatedBuffersFlag
 * A buffer can be shared among multiple streams, or just dedicated to one stream. This flag sets the Buffer as an isolated Buffer per Resource or a shared Buffer among all Resources of the assigned URIComponent.
 * @see Resource
 * @see StreamBuffers
 * @see URIComponent
 */
typedef enum
IsolatedBuffersFlag
{
    SHARED_BUFFERS,
    ISOLATED_BUFFERS
} IsolatedBuffersFlag;

/**
 * \struct StreamBuffers
 * Buffers are basically a wrapper for Buffer, since a Stream can offer
 * bidirectional buffer, is a way of wrapping both into one structure.
 * One of them can be intentionally left NULL.
 * When single_buffer is set to true it will only use one of the buffers.
 * > consider memory limitations
 */
typedef struct StreamBuffers
{
    /**
     * The buffer assigned to store data from the Peripheral and output to the stream. The `output` is the one which will be initialised always and therefore will be the one used by default.
     */
    Buffer* output;
    Buffer* input;  /**< The buffer assigned to store data going to the peripheral, optional buffer. */
    SingleFlag single_buffer;  /**< Flag to determine if the instance got only one buffer on initialisation. */
    IsolatedBuffersFlag isolated; /**< Flag that determines if the pointer to the buffers are shared among resources or not */
    BufferStatus status;  /**< Flag to determine if the buffer is in error state. */
} StreamBuffers;

/**
 * \fn PeripheralCallHandle
 * This function is the default handle is none of the other handles are present, this is also called when using streams, and the node is sending consistent data to the gateway, therefore no return value from the function, everything shall be in the `StreamBuffers`.
 * In the case this handle is not being requested, but rather just streaming data then the original `Request` will be injected.
 * This function is meant to call information from the peripheral, this is up to the implementation and shall be given
 * to the underlying Handle.
 * This method is used to add data into the buffers from the peripheral, and therefore should be sent in the response.
 * The response is injected in the Handle, so the developer can manipulate directly the response to fit its needs.
 * The data from the response will be attached after the function is called so setting the Response's data in the handle will be overwritten.
 *
 * @param StreamBuffers* Are automatically injected into the function.
 * @param Request* Automatically injected, else NULL.
 * @param Parameter[] Parameters from the query.
 * @param Response*
 */
typedef void (*PeripheralCallHandle)(StreamBuffers*, Request*, Parameter*[], Response*);

/**
 * \fn PeripheralReadHandle
 * This handle will be called when a GET request type is received.
 * This is the function which will be called when data comes from the peripheral.
 * The response is injected in the Handle, so the developer can manipulate directly the response to fit its needs.
 * And therefore shall be inserted in the Resource (good practice to push it in the proper buffer).
 * The data from the response will be attached after the function is called so setting the Response's data in the handle will be overwritten.
 *
 * @param StreamBuffers* are automatically injected into the function
 * @param Request* Automatically injected, else NULL.
 * @param Parameter[] Parameters from the query.
 * @param Response*
 */
typedef void (*PeripheralReadHandle)(StreamBuffers*, Request*, Parameter*[], Response*);

/**
 * \fn PeripheralWriteHandle
 * This handle will be called when the Request type is POST.
 * This is the function which will be called when data has to be sent to the peripheral, and may or may not have data to reply, the outcome from this call is the same as in the `PeripheralReadHandle`.
 * The data from the response will be attached after the function is called so setting the Response's data in the handle will be overwritten.
 *
 * @see PeripheralReadHandle
 * @param StreamBuffers* Are automatically injected into the function.
 * @param Request* Automatically injected, else NULL.
 * @param Parameter[] Parameters from the query.
 * @param Response*
 */
typedef void (*PeripheralWriteHandle)(StreamBuffers*, Request*, Parameter*[], Response*);


/**
 * \struct Handles
 * This structures contains the pointer to the handle functions.
 * This will be called when the request demands it to, the resource manager will call this, depending on the request type.
 */
typedef struct Handles
{
    PeripheralCallHandle call_handle;  /**< Pointer to a PeripheralCallHandle function */
    PeripheralWriteHandle write_handle;  /**< Pointer to a PeripheralWriteHandle function */
    PeripheralReadHandle read_handle;  /**< Pointer to a PeripheralReadHandle function */
} Handles;

/**
 * \typedef StreamHealth
 * Describes the health of the Stream.
 */
typedef enum StreamHealth
{
    STREAM_READY,
    STREAM_ERROR,
    STREAM_BUSY,       /**< Return value if the buffer of the stream is full. */
    STREAM_DESTROYED,
    STREAM_NULL        /**< Return value if a stream is NULL. */
}
StreamHealth;

/**
 * \typedef Stream
 * Stream instance
 * \brief `resettable` Flag will reset the buffers once they were read, this is useful if fresh data is required.
 */
typedef struct Stream
{
    /**
     * Buffer wrapper to hold data in.
     * @see StreamBuffers
     */
    StreamBuffers* buffers;
    /**
     * Handles to call per action
     * @see Handles
     */
    Handles* handles;
    char code;  /**< code of the stream */
    ResettableFlag resettable;  /**< will clear the buffer once the buffers were read, defaults to `false` */
} Stream;

#endif /* STREAM_MODELS_H_ */
