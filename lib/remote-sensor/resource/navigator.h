/**
 * @file ResourceNavigator
 */
#ifndef RESOURCE_NAVIGATOR_H_
#define RESOURCE_NAVIGATOR_H_

#include <stdlib.h>
#include <stdbool.h>
#include "models.h"

/**
 * \struct ResourceNavigator
 * This API offers some quick utilities for navigating the Resource Tree.
 * Take in consideration that the Resource Tree structured by the ResourceManager
 * is simply a binary tree!
 * @see ResourceManager
 */
struct ResourceNavigator
{
    /**
     * Returns the count of all elements in the tree based on a URIComponent* starting from the given URIComponent.
     * @param URIComponent*
     * @return uint8_t Count of all elements in the tree based on a URIComponent.
     */
    uint8_t (*count)(URIComponent*);
    /**
     * Checks if the URIComponent has a child, returns bool value.
     * If NULL is passed will always return false.
     * @param URIComponent*
     * @return bool True if the URIComponent has a child.
     */
    bool (*has_child)(URIComponent*);
    /**
     * Returns the last component from the tree (based on the given URIComponent), runs the Morris Traversal algorithm and returns the last URIComponent in the algorithm.
     * @see RegistrationController.register_to
     * @param URIComponent* from which the algorithm will start from.
     * @return URIComponent* The last URIComponent of the Morris Traversal Algorithm.
     */
    URIComponent* (*get_last_component)(URIComponent*);
    /**
     * Returns the last Resource from that given URIComponent, this will not navigate through the URIComponent tree, this will only navigate through the Resources.
     * @see RegistationController.register_to
     * @param URIComponent* from which the navigation will start from.
     * @return Resource The last resource in the URIComponent.
     */
    Resource* (*get_last_resource_from)(URIComponent*);
};

extern struct ResourceNavigator ResourceNavigator;
#endif /* RESOURCE_NAVIGATOR_H_ */
