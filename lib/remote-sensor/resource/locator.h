/**
 * @file ResourceLocator
 */
#ifndef RESOURCE_LOCATOR_H_
#define RESOURCE_LOCATOR_H_

#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

#include "models.h"

/**
 * \struct ResourceLocator
 * Holds the search algorithms to locate Resources in the Resource Tree.
 * The ResourceLocator uses URLs to locate uniquely a Resource.
 */
struct ResourceLocator
{
    /**
     * Locates the resource from within the URIComponent passed.
     * @param URIComponent*
     * @param uint8_t* The resource name string.
     * @return Resource* || NULL, if not found
     */
    Resource* (*search_in)(URIComponent*, uint8_t*);
    /**
     * Searches from the passed component the given resource represented as a URL, return NULL if no match was found.
     * Normally the passed URIComponent is the root component.
     * @param char* The URL to the resource.
     * @param URIComponent* The component to start the search from.
     * @return URIComponent* || NULL, if not found
     */
    URIComponent* (*search_component)(char*, URIComponent*);
    /**
     * Searches for a URIComponent with the given name, searches only in sibling level.
     * @param char* The name of the sibling.
     * @param URIComponent* The component to start the search from.
     * @return URIComponent* || NULL, if not found
     */
    URIComponent* (*search_component_sibling)(char*, URIComponent*);
    /**
     * Will search for a URIComponent from the start URIComponent and will update the end pointer when it finds the last sibling of the given URIComponent, will assign `NULL` to end if no results found.
     * If a name is passed (not NULL) then the search becomes unique, and if a name is found that matches the given name it will return a `RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS`. If everything was successful then `RESOURCE_MANAGER_SUCCESS` is returned.
     * @param URIComponent* Pointer do the URIComponent where to start from.
     * @param char* Name of the URIComponent to compare to, or NULL.
     * @param URIComponent** Pointer to Pointer to URIComponent will be updated to the last sibling.
     * @return ResourceManagerState || NULL, if not found.
     */
    ResourceManagerState (*get_last_sibling_component)(volatile URIComponent*, volatile char*, volatile URIComponent**);
};

extern struct ResourceLocator ResourceLocator;
#endif /* RESOURCE_LOCATOR_H_ */
