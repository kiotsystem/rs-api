#include "manager.h"

/* Global variables */

URIComponent* resource_manager_root = NULL;
uint8_t resource_manager_buffer_size = (uint8_t) RESOURCE_MANAGER_DEFAULT;
ResourceManagerState resource_manager_state = RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
char* DEFAULT_ROOT_NAME = "." ;

/* End Global Variables */

/* Helper functions */

/**
 * Creates an URIComponent with the given name.
 * \brief Allocates memory for an URIcomponent
 * @param char@ The name of the new URIComponent.
 * @return URIComponent* The new URIComponent.
 * @see ResourceManager.delete_component
 * @see URIComponent
 */
URIComponent*
ResourceManager_create_identifier_component(char* name)
{
    URIComponent* component = malloc(sizeof(URIComponent));
    if (component != NULL && name != NULL)
    {
        component->name = name;
        component->child = NULL;
        component->resources = NULL;
        component->sibling = NULL;
        component->parent = NULL;
    }
    return component;
}

/**
 * Makes sure the Resource has a resource, used to pass the health check of the Stream.
 * @param Resource*
 */
void
ResourceManager_assure_handles(Resource* resource)
{
    Handles* handles;
    if (resource->stream->handles == NULL)
    {
        handles = StreamManager.create_handles();
        StreamManager.attach(handles, resource->stream);
    } else {
        handles = resource->stream->handles;
    }
}
/* End Helper functions */


/**
 * Sets the defaults for the resource manager.
 * If a URIComponent is given then it uses that one as root, else creates a new URIComponent.
 * Beware of setting the defaults after initialisation, specially the root node!
 * \brief Allocates memory to the root node unless it was previously done.
 *
 * @param uint8_t default_buffer_size
 * @param Resource* to use as root Component
 */
void
ResourceManager_set_defaults(uint8_t buffer_size, URIComponent* root)
{
    resource_manager_buffer_size = buffer_size;

    if (root == NULL)
    {
        root = ResourceManager.create_identifier(DEFAULT_ROOT_NAME);
    }

    // if root is still null, something went wrong!
    resource_manager_state = (root == NULL) ? RESOURCE_MANAGER_ERROR : RESOURCE_MANAGER_READY;
    root->name = ".";
    resource_manager_root = root;
};

/**
 * Gets the root components
 * @return URIComponent* root component
 */
URIComponent*
ResourceManager_get_root_component()
{
    return resource_manager_root;
};

/**
 * Creates a resource.
 * If the buffer_size is 0x00 then it will assign the defaults.
 * \brief Allocates memory to the resource and to the stream
 *
 * @param char* The resource name. It will be used to uniquely identify the resource.
 * @param ResettableFlag If the stream of the resource is resettable.
 * @param SingleFlag If the stream of the resource has one or two buffers.
 * @param uint8_t Size of the buffer(s) of the stream of the resource. If 0 (`RESOURCE_MANAGER_DEFAULT`) then it will assign the value set when setting the defaults
 * @return Resource* The newly created resource or NULL if there was an error.
 * @see ResourceManager.delete
 * @see StreamManager.create
 */
Resource*
ResourceManager_create_resource(char* name, ResettableFlag resettable, SingleFlag single, uint8_t buffer_size)
{
    Resource* resource = malloc(sizeof(Resource));

    if (buffer_size == RESOURCE_MANAGER_DEFAULT)
    {
        buffer_size = resource_manager_buffer_size;
    }

    if (resource != NULL)
    {
        resource->component = NULL;
        resource->sibling = NULL;
        resource->name = name;
        resource->failed_writes = 0x00;

        // Using NULL for the code to get the automatic code generation
        Stream* stream = StreamManager.create(buffer_size, resettable, single);
        resource->state = (stream == NULL) ? RESOURCE_ERROR : RESOURCE_READY;
        resource->stream = stream;

        resource_manager_state = (resource->state == RESOURCE_READY) ?
                RESOURCE_MANAGER_READY : RESOURCE_MANAGER_RESOURCE_HAS_ERROR;

        return resource;
    }

    resource_manager_state = RESOURCE_MANAGER_RESOURCE_HAS_ERROR;
    return resource;
}

/**
 * Creates a copy from the passed Resource. The level of the isolation is passed to the StreamManager.
 * Take in consideration the SHARED_BUFFERS when sharing the same buffer among several resources.
 * The returned Resource has the same parent component than the one passed but the sibling resource is set to NULL!
 * \brief When destroying please consider if the buffers are isolated or shared among resources!
 * @see StreamManager.clone
 * @see IsolatedBuffersFlag
 * @param Resource* Pointer to the resource to clone.
 * @param IsolatedBuffersFlag Set if the buffers should be shared or isolated.
 * @return Resource* Pointer to the newly cloned resource.
 */
Resource*
ResourceManager_clone_resource(Resource* original, IsolatedBuffersFlag isolation)
{
    Resource* resource = malloc(sizeof(Resource));
    resource->component = original->component;
    resource->sibling = NULL;

    resource->data_size = original->data_size;
    resource->failed_writes = original->failed_writes;
    resource->name = original->name;
    resource->state = original->state;

    Stream* stream = StreamManager.clone(original->stream, isolation);
    resource->stream = stream;

    return resource;
}

/**
 * Individual attachment of a PeripheralCall Handle.
 * \brief If  uninitialised in the given resource, will allocate memory for the handles.
 * @see PeripheralCallHandle
 * @param PeripheralCallHandle
 * @param Resource* to which it will be attached to
 * @return ResourceManagerState `RESOURCE_MANAGER_SUCCESS`, `RESOURCE_MANAGER_RESOURCE_NOT_FOUND` or `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`
 */
ResourceManagerState
ResourceManager_attach_call_handle(PeripheralCallHandle call_handle, Resource* resource)
{
    if (resource == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    ResourceManager_assure_handles(resource);

    if (StreamManager.check_health(resource->stream) == STREAM_READY)
    {
        resource->stream->handles->call_handle = call_handle;
        return RESOURCE_MANAGER_SUCCESS;
    }

    return RESOURCE_MANAGER_RESOURCE_HAS_ERROR;
}

/**
 * Individual handle attachment of a PeripheralRead Handle.
 * \brief If uninitialised in the given resource, will allocate memory for the handles.
 * @see PeripheralReadHandle
 * @param PeripheralReadHandle
 * @param Resource*
 * @return ResourceManagerState `RESOURCE_MANAGER_SUCCESS`, `RESOURCE_MANAGER_RESOURCE_NOT_FOUND` or `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`
 */
ResourceManagerState
ResourceManager_attach_read_handle(PeripheralReadHandle read_handle, Resource* resource)
{
    if (resource == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    ResourceManager_assure_handles(resource);

    if (StreamManager.check_health(resource->stream) == STREAM_READY)
    {
        resource->stream->handles->read_handle = read_handle;
        return RESOURCE_MANAGER_SUCCESS;
    }

    return RESOURCE_MANAGER_RESOURCE_HAS_ERROR;
}

/**
 * Individual Handle attachment of a PeripheralWrite Handle.
 * \brief If the uninitialised in the given resource, will allocate memory for the handles.
 * @see PeripheralWriteHandle
 * @param PeripheralWriteHandle
 * @param Resource*
 * @return ResourceManagerState `RESOURCE_MANAGER_SUCCESS`, `RESOURCE_MANAGER_RESOURCE_NOT_FOUND` or `RESOURCE_MANAGER_RESOURCE_HAS_ERROR`
 */
ResourceManagerState
ResourceManager_attach_write_handle(PeripheralWriteHandle write_handle, Resource* resource)
{
    if (resource == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    ResourceManager_assure_handles(resource);

    if (StreamManager.check_health(resource->stream) == STREAM_READY)
    {
        resource->stream->handles->write_handle = write_handle;
        return RESOURCE_MANAGER_SUCCESS;
    }

    return RESOURCE_MANAGER_RESOURCE_HAS_ERROR;
}

/**
 * Attaches the handles to the Resource, this is a shortcut for the individual attachments.
 * \brief If uninitialised in the given resource, will allocate memory for the handles.
 *
 * @param Resource*
 * @param PeripheralCallHandle
 * @param PeripheralReadHandle
 * @param PeripheralWriteHandle
 * @return ResourceManagerState `RESOURCE_MANAGER_SUCCESS` or `RESOURCE_MANAGER_ERROR`
 * @see PeripheralCallHandle
 * @see PeripheralReadHandle
 * @see PeripheralWriteHandle
 */
ResourceManagerState
ResourceManager_attach_handles(Resource* resource, PeripheralCallHandle call_handle, PeripheralReadHandle read_handle, PeripheralWriteHandle write_handle)
{
    if (resource == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    ResourceManager_assure_handles(resource);

    ResourceManagerState call_state = ResourceManager_attach_call_handle(call_handle, resource);
    ResourceManagerState read_state = ResourceManager_attach_read_handle(read_handle, resource);
    ResourceManagerState write_state = ResourceManager_attach_write_handle(write_handle, resource);

    return (
            call_state == RESOURCE_MANAGER_SUCCESS &&
            read_state == RESOURCE_MANAGER_SUCCESS &&
            write_state == RESOURCE_MANAGER_SUCCESS
    ) ? RESOURCE_MANAGER_SUCCESS: RESOURCE_MANAGER_ERROR;
}

/**
  * The ResourceManager will register the passed Resource under the given URIComponent.
  * Every resource is only supposed to be registered to one component. If more are needed use the clone function.
  * @see ResourceManager.clone
  * @param Resource* Resource to register.
  * @param URIComponent* parent Resource from which the resource to register will be registered.
  * @return ResourceManagerState RESOURCE_MANAGER_SUCCESS or RESOURCE_MANAGER_ERROR
  */
ResourceManagerState
ResourceManager_register(Resource* resource, URIComponent* component)
{
    if (component == NULL)
    {
        return RESOURCE_MANAGER_ERROR;
    }
    if (component->resources == NULL)
    {
        component->resources = resource;
        resource->component = component;
        return RESOURCE_MANAGER_SUCCESS;
    }

    Resource* current = component->resources;
    while (current != NULL && current->sibling != NULL)
    {
        current = current->sibling;
    }

    current->sibling = resource;
    resource->component = component;

    return RESOURCE_MANAGER_SUCCESS;
}

/**
 * Registers as the next sibling of the latest sibling of the parent's child.
 * If no parent is passed, will use root component as the parent.
 * If the URI already exists at the parent's children level it will return a `RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS` to avoid duplicates, else will return `RESOURCE_MANAGER_SUCCESS`.
 * In the case `RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS` the `URIComponent` is not registered.
 * @param URIComponent* component to register
 * @param URIComponent* parent component (or NULL)
 * @return ResourceManagerState
 */
ResourceManagerState
ResourceManager_register_component(volatile URIComponent* component, volatile URIComponent* parent)
{
    volatile URIComponent* end;
    // decide which node to use
    volatile URIComponent* current = (parent == NULL) ? resource_manager_root : parent;

    if (current->child == NULL)
    {
        component->parent = current;
        current->child = component;
        return RESOURCE_MANAGER_SUCCESS;
    }

    current = current->child;
    ResourceManagerState status = ResourceLocator.get_last_sibling_component(current, component->name, &end);

    if (end == NULL || end->name == NULL)
    {
        return RESOURCE_MANAGER_ERROR;
    }

    if (status == RESOURCE_MANAGER_SUCCESS)
    {
        component->sibling = NULL;
        component->parent = (parent == NULL) ? resource_manager_root : parent;
        end->sibling = component;
        end->parent = (parent == NULL) ? resource_manager_root : parent;
    }

    return status;
}

/**
 * Searches in the tree to find the URIComponent.
 * @see ResourceLocator
 * @param char* URL path from which to navigate the tree
 * @return URIComponent* or NULL if not found.
 */
URIComponent*
ResourceManager_search(char* url)
{
    return ResourceLocator.search_component(url, ResourceManager.get_root_component());
}

/**
 * Deletes the resource from the tree and frees the memory
 * @param Resource* resource
 * @return ResourceManagerState
 */
ResourceManagerState
ResourceManager_delete(Resource* resource)
{
    if (resource == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    if (ResourceManager.unregister(resource) != RESOURCE_MANAGER_SUCCESS)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }
    
    StreamManager.destroy(resource->stream);
    free(resource);
    resource = NULL;

    return RESOURCE_MANAGER_SUCCESS;
}

/**
 * Will unregister the resource from the tree but not delete it
 * @param Resource* Resource to be unregistered.
 * @return ResourceManagerState RESOURCE_MANAGER_SUCCESS or RESOURCE_MANAGER_RESOURCE_NOT_FOUND
 */
ResourceManagerState
ResourceManager_unregister_resource(Resource* resource)
{
    if (resource == NULL || resource->component == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    URIComponent* uri = resource->component;
    Resource* head = uri->resources;

    if (head == resource && strcmp(head->name, resource->name) == 0)
    {
        // the first resource is going to be unregistered
        uri->resources = head->sibling;
        head->component = NULL;
        head->sibling = NULL;
        return RESOURCE_MANAGER_SUCCESS;
    }

    // find which of the Resources we need to unregister
    Resource* current = head;
    Resource* anchor = current;
    // loop until we find the resource
    while (current != NULL && strcmp(current->name, resource->name) != 0)
    {
        anchor = current;
        current = current->sibling;
    }

    if (current == NULL || anchor == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    if (current->sibling == NULL)
    {
        // last resource
        anchor->sibling = NULL;
        current->component = NULL;
        return RESOURCE_MANAGER_SUCCESS;
    }

    // resource was somewhere in between the resource chain
    anchor->sibling = current->sibling;
    current->sibling = NULL;
    current->component = NULL;

    return RESOURCE_MANAGER_SUCCESS;
}

/**
 * Unregisters the passed component from the tree.
 * @param URIComponent* component to register
 * @return ResourceManagerState
 */
ResourceManagerState
ResourceManager_unregister_component(URIComponent* component)
{
    if (component == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    URIComponent *parent = component->parent;

    if (parent == NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }

    if (component->name == parent->child->name)
    {
        //if the component is the direct child of the parent
        URIComponent * to_unregister = parent->child;
        parent->child = parent->child->sibling;
        to_unregister->parent = NULL;
        to_unregister->sibling = NULL;

        return RESOURCE_MANAGER_SUCCESS;
    } else {
        URIComponent *current_component = parent->child;

        while((current_component != NULL) && (current_component->sibling->name != component->name))
        {
            current_component=current_component->sibling;
        }

        if (current_component != NULL)
        {
            URIComponent * to_unregister = current_component->sibling;
            current_component->sibling = to_unregister->sibling;
            current_component->parent = NULL;

            return RESOURCE_MANAGER_SUCCESS;
        }

        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }
}
/**
 * Will delete the given component.
 * @param URIComponent* Pointer to Resource to delete.
 * @return ResourceManagerState RESOURCE_MANAGER_SUCCESS or RESOURCE_MANAGER_RESOURCE_NOT_FOUND
 */
ResourceManagerState
ResourceManager_delete_component(URIComponent* component)
{
    if (component->child != NULL)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }
    component->parent->resources = component->resources;
    component->resources = NULL;
    if (ResourceManager_unregister_component(component) != RESOURCE_MANAGER_SUCCESS)
    {
        return RESOURCE_MANAGER_RESOURCE_NOT_FOUND;
    }
    free(component);
    component = NULL;
    return RESOURCE_MANAGER_SUCCESS;
}

/**
 * Will return the absolute path string for a given component, if any component name is NULL it will return the result up to that component.
 * \brief It will allocate memory so free it when no longer needed!
 *
 * @param URIComponent* Pointer to URIComponent to look for the absolute path to.
 * @return char* Absolute path string for a given component. If the root name is "" or "/" or "./" or "." it will not be shown.
 */
char*
ResourceManager_get_path_for_component(URIComponent* component)
{
    if (component == NULL || component->name == NULL)
    {
        return NULL;
    }

    URIComponent* traverser = component;

    char* to_delete;
    char* result = strdup(traverser->name); // this way the tree is untouched

    if (traverser->parent == NULL)
    {
        return result;  // return the only name we have
    }

    traverser = traverser->parent;

    while (traverser->parent  != NULL)
    {
        if (traverser->name == NULL)
        {
            to_delete = result;
            result =  string_concat("/", result);// allocates new string
            free(to_delete);// frees memory from the last result
            return result;
        }

        to_delete = result;
        result =  string_concat("/", result);
        free(to_delete);
        to_delete = result;
        result =  string_concat(traverser->name, result);
        free(to_delete);
        traverser = traverser->parent;
    }

    to_delete = result;
    result =  string_concat("/", result);
    free(to_delete);

    if (traverser->name != NULL)
    {
        if (strcmp(traverser->name, "") != 0 && strcmp(traverser->name, "/") != 0 && strcmp(traverser->name,".") != 0 && strcmp(traverser->name, "./") != 0)
        {
            to_delete = result;
            result = string_concat(traverser->name, result);
            free(to_delete);
        }
    }
    return result;
}

/**
 * Will return the absolute path string for a given resource, if any component name is NULL it will return the result up to that component.
 * \brief It will allocate memory so free it when no longer needed!
 *
 * @param Resource* Pointer to  Resource to look for the absolute path to.
 * @return char* Absolute path string for a given resource and the resource name as action parameter. If there is only one resource, it will return only the path. If the root name is "" or "/" or "./" or "." it will not be shown.
 */
char*
ResourceManager_get_path_for_resource(Resource* resource)
{
    if (resource == NULL || resource->component == NULL)
    {
        return NULL;
    }

    char* component_path = ResourceManager.get_path_for_component(resource->component);
    Resource* resource_sibling = resource->component->resources->sibling;

    if (resource_sibling == NULL)
    {
        return component_path;
    }

    char* result = string_concat(component_path, "/?action=");
    free(component_path);
    component_path = NULL;

    if (resource->name != NULL)
    {
        char* to_delete = result;
        result = string_concat(result, resource->name);
        free(to_delete);
        to_delete = NULL;
    }

    return result;
}

/**
 * Will return an concatenated string for two given strings. It does not need to have a formated buffer unlike strcat.
 * It will allocate memory for the returned string so free it when no longer needed!
 * @param char* str1
 * @param char* str2
 * @return char* concatenated string
 */
char*
string_concat(char* str1, char* str2)
{
    if (str1 == NULL || str2 == NULL)
    {
        return NULL;
    }

    size_t string_length = strlen(str1) + strlen(str2) + 1;  // plus the EOW
    char* result = malloc(sizeof(char) * string_length);

    if (result != NULL)
    {
        result = strcpy(result, str1);
        result = strcat(result, str2);
    }

    return result;
}


/* Initialisation */
struct
ResourceManager ResourceManager = {
    .set_defaults = ResourceManager_set_defaults,
    .get_root_component = ResourceManager_get_root_component,
    .create = ResourceManager_create_resource,
    .clone = ResourceManager_clone_resource,
    .create_identifier = ResourceManager_create_identifier_component,
    .attach_handles = ResourceManager_attach_handles,
    .attach_call_handle = ResourceManager_attach_call_handle,
    .attach_read_handle = ResourceManager_attach_read_handle,
    .attach_write_handle = ResourceManager_attach_write_handle,
    .register_resource = ResourceManager_register,
    .register_component = ResourceManager_register_component,
    .unregister = ResourceManager_unregister_resource,
    .delete = ResourceManager_delete,
    .search = ResourceManager_search,
    .unregister_component = ResourceManager_unregister_component,
    .delete_component = ResourceManager_delete_component,
    .get_path_for_component = ResourceManager_get_path_for_component,
    .get_path_for_resource = ResourceManager_get_path_for_resource
};
