#include "navigator.h"

   /**
     * Checks if the URIComponent has a child, returns bool value.
     * If NULL is passed will always return false.
     * @param URIComponent*
     * @return bool True if the URIComponent has a child.
     */
bool
ResourceNavigator_has_child(URIComponent* component)
{
    if (component != NULL)
    {
        return component->child == (URIComponent*) NULL ? false : true;
    }

    return false;
}

/**
 * Returns the count of all elements in the tree based on a URIComponent*
 * @param URIComponent*
 * @return uint8_t Count of all elements in the tree based on a URIComponent.
 */
uint8_t
ResourceNavigator_count(URIComponent* component)
{
    if (component == NULL)
    {
        return 0;
    } else {
        uint8_t count = 1;
        count += ResourceNavigator_count(component->child);
        count += ResourceNavigator_count(component->sibling);
        return count;
    }    
}

/**
  * Returns the last component from the tree (based on the given URIComponent), runs the Morris Traversal algorithm and returns the last URIComponent in the algorithm.
  * @see RegistrationController.register_to
  * @param URIComponent* from which the algorithm will start from.
  * @return URIComponent* The last URIComponent of the Morris Traversal Algorithm.
  */
URIComponent*
ResourceNavigator_get_last_component(URIComponent* root_component)
{
    URIComponent* traverser = root_component;

    if (traverser == NULL)
    {
        return NULL;
    }

    URIComponent* end = traverser;

    while (traverser != NULL)
    {
        end = traverser;
        traverser = traverser->sibling;
    }

    return end;
}

/**
 * Returns the last Resource from that given URIComponent, this will not navigate through the URIComponent tree, this will only navigate through the Resources.
 * @see RegistationController.register_to
 * @param URIComponent* from which the navigation will start from.
 * @return Resource The last resource in the URIComponent.
 */
Resource*
ResourceNavigator_get_last_resource_from(URIComponent* component)
{
    if (component == NULL || component->resources == NULL)
    {
        return NULL;
    }

    Resource* resource = component->resources;
    Resource* end = resource;

    while (resource != NULL)
    {
        end = resource;
        resource = resource->sibling;
    }

    return end;
}

struct
ResourceNavigator ResourceNavigator = {
    .count = ResourceNavigator_count,
    .has_child = ResourceNavigator_has_child,
    .get_last_component = ResourceNavigator_get_last_component,
    .get_last_resource_from = ResourceNavigator_get_last_resource_from
};
