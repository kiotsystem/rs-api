#ifndef RESOURCE_MODELS_H_
#define RESOURCE_MODELS_H_

#include "stream/models.h"

/**
 * \typedef ResourceManagerState
 * Represents the state of the Resource Manager
 */
typedef enum ResourceManagerState
{
    RESOURCE_MANAGER_DEFAULT = 0,
    RESOURCE_MANAGER_ERROR,
    RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS,
    RESOURCE_MANAGER_RESOURCE_HAS_ERROR,
    RESOURCE_MANAGER_RESOURCE_NOT_FOUND,
    RESOURCE_MANAGER_READY,
    RESOURCE_MANAGER_SUCCESS
} ResourceManagerState;

/**
 * \typedef ResourceState
 * Represents the state of the resource
 */
typedef enum
ResourceState
{
    RESOURCE_READY,
    RESOURCE_ERROR,
    RESOURCE_UNRESPONSIVE,
} ResourceState;

/**
 * \typedef URIComponent
 * An URIComponent is a Node in a tree which is uniquely identifiable by an URL. One or multiple resources are attached to it. An URIComponent has to be registered in the tree's structure using the register_component call.
 * @see Resource
 */
typedef struct URIComponent
{
    char* name;/**<Name which uniquely identifies the component in this level.*/
    struct Resource* resources;/**<Attached Resource*/
    struct URIComponent *parent;/**<Pointer to the component in the upper level of the tree*/
    struct URIComponent *sibling;/**<Pointer to a component in the same level of the tree*/
    struct URIComponent *child;/**<Pointer to the component in the lower level of the tree*/
} URIComponent;

/**
 * \typedef Resource
 * Resource, which will represent the Peripheral (e.g. sensor). The resource has to be accessed through an URIComponent. The resource has to be registered to the URIComponent by using the register_resource.
 * The resource therefore can be uniquely identified at that component's level, allowing resources with the same name as long as they are in different URIComponents.
 * It is referred to by the action parameter in the URL via the name.
 * @see URIComponent
 * @see ResourceState
 * @see Stream
 */
typedef struct Resource
{
    URIComponent *component;  /**< Pointing to the component in the tree. */
    struct Resource* sibling;  /**< Resource sibling. */
    char* name;  /**< Name which uniquely identifies the Resource.  */
    uint8_t data_size;  /**< The parameter `data_size` represents the size of the buffers to allocate, assign to this Resource. */
    uint8_t failed_writes;  /**< This counts the number of failed writes in the Stream. */
    ResourceState state;  /**< State of the resource. */
    Stream* stream;  /**< Pointer to the stream. */
} Resource;

#endif /* RESOURCE_MODELS_H_ */
