#include "locator.h"

/**
 * Search for the sibling of a given component.
 * @param char* name is the name of the sibling
 * @param URIComponent component
 * @return URIComponent || NULL, if not found
 */
URIComponent*
ResourceLocator_search_component_sibling(char* name, URIComponent* component)
{
    if (component == NULL)
    {
        return NULL;
    }
    
    URIComponent* current = component;

    do
    {
        if(current->name != NULL && strcmp(name, current->name) == 0)
        {
            return current;
        }
        current = current->sibling;
    } while(current != NULL);

    return NULL;  // no matches
}

/**
 * search for a URIComponent with a given url
 * @param char* url
 * @param URIComponent* component from which to start looking from
 * @return URIComponent has the value NULL if not found
 */
URIComponent*
ResourceLocator_search_component(char* url, URIComponent* current)
{
    if (url == NULL)
    {
        return (URIComponent*) NULL;
    }
    size_t size = strlen(url);
    char* url_copy = malloc(size + 1);  // create a copy in dynamic memory

    if (url_copy == NULL)
    {
        return NULL;
    }

    strcpy(url_copy, url);
    char* uri_name = url_copy;

    // handle the route for root
    if (strcmp(url, "") == 0 || strcmp(url, "/") == 0 || strcmp(url, ".") == 0 || strcmp(url, "./") == 0)
    {
        return current;
    }

    char* urn = strtok(uri_name, "/");

    // check if the tokenization was successful for root..
    if (urn == NULL)
    {
        return (URIComponent*) NULL;
    }

    //for the case we have url = /root_name/blabla/child_name/
    current = current->child;  // root's first child
    URIComponent* last_match = NULL;
    while (urn != NULL)
    {
        last_match = ResourceLocator.search_component_sibling(urn, current);
        if (last_match != NULL)
        {
            //checks if there is a sibling with that name
            current = last_match->child;
        } else {
            // no match was found for the current uri name
            free(url_copy);  // strdup calls malloc
            return NULL;
        }
        urn = strtok (NULL, "/");
    }
    free(url_copy);  // strdup calls malloc
    return last_match;
}

/**
 * Locates the resource from within the URIComponent passed.
 * @param URIComponent* component
 * @param uint8_t* name
 * @return Resource* || NULL, if not found
 */
Resource*
ResourceLocator_search_in(URIComponent* component, uint8_t* name)
{
    Resource* current = component->resources;

    while (current != NULL)
    {
        if (strcmp(current->name, name) == 0)
        {
            return current;
        }
        current = current->sibling;
    }

    return current;
}

/**
 * Will search from the start URIComponent and will update the end pointer when it finds the last result.
 * If a name is passed (not NULL) then the search becomes unique, and if a name is found that matches the given name it will return a `RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS`. If everything was successful then `RESOURCE_MANAGER_SUCCESS` is returned.
 * @param URIComponent* start
 * @param char* name
 * @param volatile URIComponent** end
 * @return ResourceManagerState
 */
ResourceManagerState
ResourceLocator_get_last_sibling_component(volatile URIComponent* start, volatile char* name, volatile URIComponent** end)
{
    volatile URIComponent* current = start;
    *end = current;

    while (current != NULL && current->sibling != NULL)
    {
        if (name != NULL && current->name != NULL && current->name == name)
        {
            *end = current;
            return RESOURCE_MANAGER_RESOURCE_ALREADY_EXISTS;
        }
        current = current->sibling;  // continue looping
        *end = current;
    }

    return RESOURCE_MANAGER_SUCCESS;
}

/* Initialization */
struct
ResourceLocator ResourceLocator = {
    .search_component = ResourceLocator_search_component,
    .search_in = ResourceLocator_search_in,
    .search_component_sibling = ResourceLocator_search_component_sibling,
    .get_last_sibling_component = ResourceLocator_get_last_sibling_component
};
