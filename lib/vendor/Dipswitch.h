#ifndef DIPSWITCH_H
#define DIPSWITCH_H

#include <stdint.h>

#include "IOPinController.h"

/*
* enums
*/
typedef enum
  {
    DIPSWITCH_NOT_INITIALIZED = 0,
    DIPSWITCH_INITIALIZED = 1,
  } DipSwitchStatus_t;

/*
  Summary: Returns the dipswitch init status
  @return: The current dip switch status, not the value!!!
    possible values: see enum above
*/
DipSwitchStatus_t
DipSwitch_getStatus();

/*
  Summary: Initialize the dipswitch on given port with the given pin numbers
  @param port: the port to work on
  @param pinOne: the pin of the dipswitch to represent the first bit
  @param pinTwo: the pin of the dipswitch to represent the second bit
  @param pinThree: the pin of the dipswitch to represent the third bit
  @return:
      0: init was not successfull
      1: init was successfull
*/
uint8_t
DipSwitch_init(Port port, uint8_t pinOne, uint8_t pinTwo, uint8_t pinThree);

/*
  Summary: Resets the dipswitch to an not initialized state
  @return:
      0: reset was not successfull
      1: reset was successfull
*/
uint8_t
DipSwitch_reset();

/*
  Summary: Returns the dipswitch state as decimal value
  @return: The current dipswitch value as uint8_t
*/
int8_t
DipSwitch_getValue();

/*
  Summary: Returns the dipswitch state as decimal char value
  @return: The current dip switch value as char
*/
char
DipSwitch_getValueAsChar();

#endif //DIPSWITCH_H
