/*
 * IOPinController.h
 *
 *  Created on: 19 May 2017
 *      Author: charlesbakar
 */

#ifndef IOPINCONTROLLER_H
#define IOPINCONTROLLER_H

#include <stdint.h>

/**
 * The Port object used here
 * is dynamically allocated and therefore has to
 * be freed manually with IOPinController_destroyPort().
 * Each pin of a port can be either in Output
 * or Input mode.
 * If you don't want to use a pin, put it into Input mode.
 * After setting the last necessary register, the port is automatically initialized with InputMode for all pins.
 **/

enum
  {
    PORT_NOT_INITIALIZED        =  0,
    PORT_INITIALIZED            =  1,
    PIN_WRITE_SUCCESS           =  5,
    PIN_WRONG_STATE_ERROR       = -6,
    PORT_NOT_INITIALIZED_ERROR  = -7,
    PIN_SET_ERROR               = -8,
  };

struct PortStruct_t;

typedef struct PortStruct_t *Port;

Port
IOPinController_createEmptyPort();

void
IOPinController_destroyPort(Port);

int8_t
IOPinController_getPortState(const Port);

void
IOPinController_setDataDirectionRegister(Port, volatile uint8_t*);

void
IOPinController_setDataRegister(Port, volatile uint8_t*);

void
IOPinController_setInputPinsRegister(Port, volatile uint8_t*);

/*
 * Use this to set up the Register used to control
 * the pullup resistors state of the pins.
 * E.g. for atmega328p you don't have to call it,
 * because the platform has no dedicated register for
 * pullup resistor control.
 */
void
IOPinController_setPullUpRegister(Port, volatile uint8_t*);

void
IOPinController_setPortOutputModeLow(Port);

void
IOPinController_setPortInputMode(Port);

void
IOPinController_setPinOutputLow(Port, uint8_t);

void
IOPinController_setPinInputMode(Port, uint8_t);

int8_t
IOPinController_writePinHigh(Port, uint8_t);

int8_t
IOPinController_writePinLow(Port, uint8_t);

/*
 * Writes a given byte to the port. Pins, that are not
 * in output mode, pins in input mode are not touched.
 */
int8_t
IOPinController_writeBitPatternToOutputPins(Port, uint8_t);

/*
 * Read a byte from a given port. This replaces the values
 * of pins, that are not in input mode, with zeros.
 * In case of a not initialized port 0 is returned.
 */
uint8_t
IOPinController_readByteLowSignalAsOne(const Port);

uint8_t
IOPinController_readByteLowSignalAsZero(const Port);

/*
 * Read a single input pin.
 * PIN_WRONG_STATE error is returned, if the specified pin is
 * not in input mode.
 */
int8_t
IOPinController_readPinLowSignalAsOne(const Port, uint8_t);

int8_t
IOPinController_readPinLowSignalAsZero(const Port, uint8_t);

/**
 * Returns a byte, that represents which pins
 * of the port are set to input mode.
 * E.g.: 0x02 - only the third pin is in input mode
 **/
uint8_t
IOPinController_getInputModePins(const Port);

#endif /* IOPINCONTROLLER_H */
