/*
 * IOPinController_atmega328p.c
 *
 *  Created on: 20 May 2017
 */

#include <stdbool.h>
#include <stdlib.h>

#include "IOPinController.h"

typedef struct PortStruct_t
{
  volatile uint8_t *data_direction_ptr;
  volatile uint8_t *data_ptr;
  volatile uint8_t *input_pins_ptr;
} PortStruct;

enum
  {
    DATA_DIRECTION_INPUT_ALL_PINS  = 0x00,
    PULLUP_DISABLED_ALL_PINS       = 0x00,
    PULLUP_ENABLED_ALL_PINS        = 0xFF,
    DATA_DIRECTION_OUTPUT_ALL_PINS = 0xFF,
    LOW_SIGNAL_ALL_PINS            = 0x00,
    MAXIMUM_PIN_NUMBER             =    7,
    ALL_PINS_HIGH                  = 0xFF,
  };
/*****************************************
 * internal helper function declarations *
 *****************************************/

static bool
portRegisterSetupIsValid(const Port);

static bool
noRegisterPtrIsNull(const Port);

static bool
portRegistersDifferPairwise(const Port);

static bool
pinsDataDirectionIsOut(Port port, uint8_t pin);

static bool
bitInPatternIsOne(uint8_t bit_pattern, uint8_t bit_number);

static uint8_t
shiftOneBitBy(uint8_t amount);

static uint8_t
shiftZeroBitBy(uint8_t amount);

static void
setBitToZero(volatile uint8_t *address, uint8_t bit_number);

static void
setBitToOne(volatile uint8_t *address, uint8_t bit_number);

static bool
setPinFunctionParametersAreValid(const Port port, uint8_t pin);

Port
IOPinController_createEmptyPort()
{
  Port port = (Port) malloc(sizeof(PortStruct));
  port->data_direction_ptr = NULL;
  port->data_ptr = NULL;
  port->input_pins_ptr = NULL;
  return port;
}

void
IOPinController_destroyPort(Port port)
{
  free(port);
}

int8_t
IOPinController_getPortState(Port port)
{
  if (portRegisterSetupIsValid(port))
    return PORT_INITIALIZED;
  return PORT_NOT_INITIALIZED;
}

void
IOPinController_setDataDirectionRegister(Port port, volatile uint8_t *data_direction)
{
  port->data_direction_ptr = data_direction;
  IOPinController_setPortInputMode(port);
}

void
IOPinController_setDataRegister(Port port, volatile uint8_t *data)
{
  port->data_ptr = data;
}

void
IOPinController_setInputPinsRegister(Port port, volatile uint8_t *input_pins)
{
  port->input_pins_ptr = input_pins;
}

void
IOPinController_setPullUpRegister(Port port, volatile uint8_t *pullup_register)
{
  // atmega328p has no dedicated pullup resistor control register
  // When in input mode, the data register is used to control the pullup resistor.
  IOPinController_setDataRegister(port, pullup_register);
}

void
IOPinController_setPortOutputModeLow(Port port)
{
  if (portRegisterSetupIsValid(port))
    {
      *port->data_direction_ptr = DATA_DIRECTION_OUTPUT_ALL_PINS;
      *port->data_ptr = LOW_SIGNAL_ALL_PINS;
    }
}

void
IOPinController_setPortInputMode(Port port)
{
  if (portRegisterSetupIsValid(port))
    {
      *port->data_direction_ptr = DATA_DIRECTION_INPUT_ALL_PINS;
      *port->data_ptr = PULLUP_ENABLED_ALL_PINS;
    }
}

void
IOPinController_setPinOutputLow(Port port, uint8_t pin_number)
{
  if (setPinFunctionParametersAreValid(port, pin_number))
    {
      setBitToZero(port->data_ptr, pin_number);
      setBitToOne(port->data_direction_ptr, pin_number);
    }
}

void
IOPinController_setPinInputMode(Port port, uint8_t pin_number)
{
  if (setPinFunctionParametersAreValid(port, pin_number))
    {
      setBitToZero(port->data_direction_ptr, pin_number);
      setBitToOne(port->data_ptr, pin_number);
    }
}

int8_t
IOPinController_writePinHigh(Port port, uint8_t pin_number)
{
  if (!portRegisterSetupIsValid(port))
    return PORT_NOT_INITIALIZED_ERROR;
  if (pin_number > MAXIMUM_PIN_NUMBER)
    return PIN_SET_ERROR;
  if ( !pinsDataDirectionIsOut(port, pin_number) )
    return PIN_WRONG_STATE_ERROR;

  setBitToOne(port->data_ptr, pin_number);
  return PIN_WRITE_SUCCESS;
}

int8_t
IOPinController_writePinLow(Port port, uint8_t pin_number)
{
  if (!portRegisterSetupIsValid(port))
    return PORT_NOT_INITIALIZED_ERROR;
  if (pin_number > MAXIMUM_PIN_NUMBER)
    return PIN_SET_ERROR;
  if ( !pinsDataDirectionIsOut(port, pin_number) )
    return PIN_WRONG_STATE_ERROR;

  setBitToZero(port->data_ptr, pin_number);
  return PIN_WRITE_SUCCESS;
}

int8_t
IOPinController_writeBitPatternToOutputPins(Port port, uint8_t bit_pattern)
{
  if (portRegisterSetupIsValid(port))
    {
      uint8_t one_bits_masked_by_ddr = *port->data_direction_ptr & bit_pattern;
      uint8_t zero_bits_masked_by_ddr = ~(*port->data_direction_ptr | ~bit_pattern);
      uint8_t resulting_data_value = (*port->data_ptr & zero_bits_masked_by_ddr) | one_bits_masked_by_ddr;
      *port->data_ptr = resulting_data_value;
      return PIN_WRITE_SUCCESS;
    }
  else
    {
      return PORT_NOT_INITIALIZED_ERROR;
    }
}

uint8_t
IOPinController_readByteLowSignalAsZero(const Port port)
{
  if (portRegisterSetupIsValid(port))
    {
      uint8_t read_value = *port->input_pins_ptr;
      read_value &= ~(*port->data_direction_ptr);
      return read_value;
    }
  else
    {
      return 0;
    }
}

uint8_t
IOPinController_readByteLowSignalAsOne(const Port port)
{
  if (portRegisterSetupIsValid(port))
    {
      uint8_t read_value = ~(*port->input_pins_ptr);
      read_value &= ~(*port->data_direction_ptr);
      return read_value;
    }
  else
    {
      return 0;
    }
}

int8_t
IOPinController_readPinLowSignalAsOne(const Port port, uint8_t pin_number)
{
  if (!portRegisterSetupIsValid(port))
    return PORT_NOT_INITIALIZED_ERROR;
  if (pinsDataDirectionIsOut(port, pin_number))
    return PIN_WRONG_STATE_ERROR;

  uint8_t read_byte_value = IOPinController_readByteLowSignalAsOne(port);
  return bitInPatternIsOne(read_byte_value, pin_number);
}

int8_t
IOPinController_readPinLowSignalAsZero(const Port port, uint8_t pin_number)
{
  int8_t pin_value = IOPinController_readPinLowSignalAsOne(port, pin_number);

  if (pin_value >= 0)
    return !pin_value;

  return pin_value;
}

uint8_t
IOPinController_getInputModePins(const Port port)
{

  return *port->data_ptr & ~(*port->data_direction_ptr);
}

/**********************************
 **   internal helper functions  **
 **********************************/

static bool
bitInPatternIsOne(uint8_t bit_pattern, uint8_t bit_number_to_check)
{
  return (bit_pattern >> bit_number_to_check) & 1;
}

static uint8_t
shiftOneBitBy(uint8_t amount)
{
  return (1 << amount);
}

static uint8_t
shiftZeroBitBy(uint8_t amount)
{
  return ~shiftOneBitBy(amount);
}

static void
setBitToOne(volatile uint8_t *address, uint8_t bit_number)
{
  *address |= shiftOneBitBy(bit_number);
}

static void
setBitToZero(volatile uint8_t *address, uint8_t bit_number)
{
  *address &= shiftZeroBitBy(bit_number);
}

static bool
portRegisterSetupIsValid(const Port port)
{
  return (portRegistersDifferPairwise(port) && noRegisterPtrIsNull(port));
}

static bool
portRegistersDifferPairwise(const Port port)
{
  return (port->data_ptr != port->data_direction_ptr
          && port->data_ptr != port->input_pins_ptr
          && port->data_direction_ptr != port->input_pins_ptr);
}

static bool
noRegisterPtrIsNull(const Port port)
{
  return (port->data_direction_ptr != NULL
          && port->data_ptr != NULL
          && port->input_pins_ptr != NULL);
}

static bool
pinsDataDirectionIsOut(const Port port, uint8_t pin)
{
  uint8_t pin_value = 1 & (*port->data_direction_ptr >> pin);
  return (pin_value == 1);
}

static bool
setPinFunctionParametersAreValid(const Port port, uint8_t pin_number)
{
  return (pin_number < MAXIMUM_PIN_NUMBER &&
          portRegisterSetupIsValid(port));
}
