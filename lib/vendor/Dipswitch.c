
#include "Dipswitch.h"
#include <stdlib.h>
#include <string.h>

#include "IOPinController.h"

static int8_t status = DIPSWITCH_NOT_INITIALIZED;
static Port usedPort = NULL;
static int8_t dipSwitchOneMappedToPinNumber = -1;
static int8_t dipSwitchTwoMappedToPinNumber = -1;
static int8_t dipSwitchThreeMappedToPinNumber = -1;

DipSwitchStatus_t
DipSwitch_getStatus()
{
  return status;
}

uint8_t
DipSwitch_init(Port port, uint8_t pinOne, uint8_t pinTwo, uint8_t pinThree)
{
  uint8_t result = 0;

  if (IOPinController_getPortState(port) == PORT_INITIALIZED)
    {
      IOPinController_setPinInputMode(port,pinOne);
      IOPinController_setPinInputMode(port,pinTwo);
      IOPinController_setPinInputMode(port,pinThree);
      usedPort = port;
      dipSwitchOneMappedToPinNumber = pinOne;
      dipSwitchTwoMappedToPinNumber = pinTwo;
      dipSwitchThreeMappedToPinNumber = pinThree;
      status = DIPSWITCH_INITIALIZED;
      result = 1;
    }
  else
    {
      result = DipSwitch_reset();
    }
  return result;
}

uint8_t
DipSwitch_reset()
{
  uint8_t result = 0;
  if (status == DIPSWITCH_INITIALIZED) {
    usedPort = NULL;
    dipSwitchOneMappedToPinNumber = -1;
    dipSwitchTwoMappedToPinNumber = -1;
    dipSwitchThreeMappedToPinNumber = -1;
    status = DIPSWITCH_NOT_INITIALIZED;
    result = 1;
  }
  return result;
}

int8_t
DipSwitch_getValue()
{
  int8_t value = -1;

  if (status == DIPSWITCH_INITIALIZED)
  {
    uint8_t valueAtDipSwitchOne = IOPinController_readPinLowSignalAsOne(usedPort, dipSwitchOneMappedToPinNumber);
    uint8_t valueAtDipSwitchTwo = IOPinController_readPinLowSignalAsOne(usedPort, dipSwitchTwoMappedToPinNumber);
    uint8_t valueAtDipSwitchThree = IOPinController_readPinLowSignalAsOne(usedPort, dipSwitchThreeMappedToPinNumber);

    value = (valueAtDipSwitchThree << 2) | (valueAtDipSwitchTwo << 1) | (valueAtDipSwitchOne);
  }

  return value;
}

char
DipSwitch_getValueAsChar()
{
  char value = '!';

  if (DipSwitch_getValue() >= 0)
  {
    value = DipSwitch_getValue() + '0';
  }

  return value;
}
