#!/bin/bash
cd ~/codes/remote-sensors/
git reset --hard
git fetch --prune
git checkout develop
git pull
doxygen Doxyfile

